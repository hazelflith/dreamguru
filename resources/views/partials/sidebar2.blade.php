@if(session('token'))
<div id="body-pd" class="body-pd trans5">
    @include('partials.navbar')
    <div class="l-navbar show-navbar" id="nav-bar">
        <nav class="nav">
            <div>
                <div class="nav_list">
                        <a href="{{ route('/') }}" class="nav_link {{ Request::is('/') ? 'nav-active':'' }}">
                            <div class="sidebar-icon">
                                <i class="fa-solid fa-house {{ Request::is('/') ? 'navbar-selected':'' }}"></i>
                            </div>
                            <span class="nav_name {{ Request::is('/') ? 'navbar-selected':'' }}">
                                Home
                            </span> 
                        </a>
                        <a href="{{ route('/course') }}" class="nav_link {{ Request::is('course') ? 'nav-active':'' }}">
                            <div class="sidebar-icon">
                                <i class="fa-solid fa-book {{ Request::is('course') ? 'navbar-selected':'' 
                                || Request::is('course/*') ? 'navbar-selected':''  }}"></i>
                             </div>
                            <span class="nav_name {{ Request::is('course') ? 'navbar-selected':'' 
                            || Request::is('course/*') ? 'navbar-selected':''  }}">
                                Browse Courses
                            </span>
                        </a>
                   
                   
                        <a href="{{ route('/profile') }}" class="nav_link {{ Request::is('profile') ? 'nav-active':'' }}">
                            <div class="sidebar-icon">
                                <i class="fa-solid fa-user {{ Request::is('profile') ? 'navbar-selected':'' }}"></i>
                            </div>
                            <span class="d-none d-sm-inline {{ Request::is('profile') ? 'color-gold':'' }}">
                                Profile
                            </span>
                        </a>
                    
                    {{-- FOR TEACHER SWITCH --}}
                    <div v-if="'{{session('data')['role']}}' == 'teacher' && $cookies.get('dreamguru_role') == 'student'" v-cloak>
                        <div class="mb-3 mt-3" id="sub-title2">
                            <span class="d-none d-md-block ms-4">
                                STUDENT MENUS
                            </span>
                        </div>
                        <a href="{{ route('/student/dashboard/courses') }}"
                            class="nav_link {{ Request::is('student/dashboard/courses') ? 'nav-active':'' }}"
                        >
                            <div class="sidebar-icon">
                                <i class="fa-solid fa-book-open {{ Request::is('student/dashboard/courses') ? 'navbar-selected':'' }}"></i>
                            </div>
                            <span class="nav_name {{ Request::is('student/dashboard/courses') ? 'navbar-selected':'' }}">
                                Courses
                            </span>
                        </a>
                        
                        <a href="{{ route('/student/dashboard/quizzes') }}"
                            class="nav_link {{ Request::is('student/dashboard/quizzes') ? 'nav-active':'' }}"
                        >
                            <div class="sidebar-icon">
                                <i class="fa-solid fa-circle-question {{ Request::is('student/dashboard/quizzes') ? 'navbar-selected':'' }}"></i>    
                            </div>
                            <span class="nav_name {{ Request::is('student/dashboard/quizzes') ? 'navbar-selected':'' }}">
                                Quizzes
                            </span>
                        </a>
                        
                        <a href="{{ route('/student/dashboard/billing') }}"
                            class="nav_link {{ Request::is('student/dashboard/billing') ? 'nav-active':'' }}"
                        >
                        <div class="sidebar-icon">
                            <i class="fa-solid fa-dollar-sign {{ Request::is('student/dashboard/billing') ? 'navbar-selected':'' }}"></i>
                        </div>
                            <span class="nav_name {{ Request::is('student/dashboard/billing') ? 'navbar-selected':'' }}">
                                Billing
                            </span>
                        </a>
                    </div>
                </div>
                <div v-if="'{{session('data')['role']}}' == 'student'" v-cloak>
                    <div class="mb-3 mt-3" id="sub-title2">
                        <span class="d-none d-md-block ms-4">
                            STUDENT MENUS
                        </span>
                    </div>
                    <a href="{{ route('/student/dashboard/courses') }}"
                        class="nav_link {{ Request::is('student/dashboard/courses') ? 'nav-active':'' }}"
                    >
                        <div class="sidebar-icon">
                            <i class="fa-solid fa-book-open {{ Request::is('student/dashboard/courses') ? 'navbar-selected':'' }}"></i>
                        </div>
                        <span class="nav_name {{ Request::is('student/dashboard/courses') ? 'navbar-selected':'' }}">
                            Courses
                        </span>
                    </a>
                    
                    <a href="{{ route('/student/dashboard/quizzes') }}"
                        class="nav_link {{ Request::is('student/dashboard/quizzes') ? 'nav-active':'' }}"
                    >
                        <div class="sidebar-icon">
                            <i class="fa-solid fa-circle-question {{ Request::is('student/dashboard/quizzes') ? 'navbar-selected':'' }}"></i>    
                        </div>
                        <span class="nav_name {{ Request::is('student/dashboard/quizzes') ? 'navbar-selected':'' }}">
                            Quizzes
                        </span>
                    </a>
                    
                    <a href="{{ route('/student/dashboard/billing') }}"
                        class="nav_link {{ Request::is('student/dashboard/billing') ? 'nav-active':'' }}"
                    >
                    <div class="sidebar-icon">
                        <i class="fa-solid fa-dollar-sign {{ Request::is('student/dashboard/billing') ? 'navbar-selected':'' }}"></i>
                    </div>
                        <span class="nav_name {{ Request::is('student/dashboard/billing') ? 'navbar-selected':'' }}">
                            Billing
                        </span>
                    </a>
                </div>
                <div v-if="'{{session('data')['role']}}' == 'student'" id="sub-title2" v-cloak></div>
                <div v-if="'{{session('data')['role']}}' == 'teacher' && $cookies.get('dreamguru_role') == 'teacher'" v-cloak>
                    <div class="mb-3 mt-3" id="sub-title2">
                        <span class="d-none d-md-block ms-4">
                            GURU MENUS
                        </span>
                    </div>

                    <div class="nav-list">
                        <a href="{{ route('/teacher/course') }}" class="nav_link {{ Request::is('teacher/course/*') ? 'nav-active':''
                        || Request::is('teacher/course') ? 'nav-active':'' }}">
                            <i class="fa-solid fa-address-book {{ Request::is('teacher/courses/*') ? 'navbar-selected':'' 
                        || Request::is('teacher/course') ? 'navbar-selected':'' }}"></i>
                            <span class="nav_name {{ Request::is('teacher/course/*') ? 'navbar-selected':'' 
                        || Request::is('teacher/course') ? 'navbar-selected':'' }}">
                                My Courses
                            </span>
                        </a>
                        <a href="{{ route('/teacher/quiz') }}" class="nav_link {{ Request::is('teacher/quiz/*') ? 'nav-active':''
                        || Request::is('teacher/quiz') ? 'nav-active':'' }}">
                            <i class="fa-solid fa-circle-question {{ Request::is('teacher/quiz/*') ? 'navbar-selected':''
                            || Request::is('teacher/quiz') ? 'navbar-selected':'' }}"></i>
                            <span class="nav_name {{ Request::is('teacher/quiz/*') ? 'navbar-selected':''
                            || Request::is('teacher/quiz') ? 'navbar-selected':'' }}">
                                My Quizzes
                            </span>
                        </a>
                    </div>
                </div>
                <div v-if="'{{session('data')['role']}}' == 'admin'" v-cloak>
                    <a href="{{ route('/admin') }}" class="nav_link {{ Request::is('admin') ? 'nav-active':''
                    || Request::is('admin') ? 'nav-active':'' }}">
                        <i class="fa-solid fa-screwdriver-wrench {{ Request::is('admin') ? 'navbar-selected':'' 
                    || Request::is('admin') ? 'navbar-selected':'' }}"></i>
                        <span class="nav_name {{ Request::is('admin') ? 'navbar-selected':'' 
                    || Request::is('admin') ? 'navbar-selected':'' }}">
                        Admin Dashboard
                        </span>
                    </a>
                </div>
            </div>
        </nav>
    </div>
    <div class="main-app">
        @yield('content')
        @include('partials.footer')
    </div>
</div>
@else
<div class="">
    @include('partials.navbar_nologin')
    <div class="">
        @yield('content')
        @include('partials.footer')
    </div>
</div>
@endif
