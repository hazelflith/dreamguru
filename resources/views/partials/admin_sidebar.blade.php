@if(session('token'))
<div id="body-pd" class="body-pd trans5">
    @include('partials.navbar')
    <div class="l-navbar show-navbar" id="nav-bar">
        <nav class="nav">
            <div>
                <div class="nav_list">
                    <div class="mb-3 mt-3" id="sub-title2">
                        <span class="d-none d-md-block ms-4">
                            ADMIN DASHBOARD
                        </span>
                    </div>
                    <div v-if="'{{session('data')['role']}}' == 'admin'">
                        <a href="{{ route('/admin') }}" class="nav_link {{ Request::is('admin') ? 'nav-active':'' }}">
                            <div class="sidebar-icon">
                                <i class="fa-solid fa-gauge-high {{ Request::is('admin') ? 'navbar-selected':'' }}"></i>
                            </div>
                            <span class="nav_name {{ Request::is('admin') ? 'navbar-selected':'' }}">
                                Dashboard
                            </span> 
                        </a>
                        <a class="nav_link {{ Request::is('admin/user*') ? 'nav-active':'' || Request::is('admin/user') ? 'nav-active':'' }}"
                            data-bs-toggle="collapse" href="#UserDashboard" role="button" aria-expanded="true" 
                            aria-controls="userDashboard"
                        >
                            <i class="fa-solid fa-screwdriver-wrench {{ Request::is('admin/user*') ? 'navbar-selected':'' 
                                || Request::is('admin/user*') ? 'navbar-selected':'' }}"
                            ></i>
                            <span class="nav_name {{ Request::is('admin/user*') ? 'navbar-selected':'' 
                                || Request::is('admin/user*') ? 'navbar-selected':'' }}"
                            >
                                Management
                                <i class="ms-2 fa-solid fa-caret-down"></i>
                            </span>
                        </a>
                        <div class="{{ !Request::is('admin/user*') ? 'collapse':'' }} {{ Request::is('admin/user*') ? 'show':'' }}" id="UserDashboard">
                            <div class="ms-5">
                                <a href="{{ route('/admin/user/students') }}" class="nav_link m-0">
                                    <span class="nav_name {{ Request::is('admin/user/students') ? 'navbar-selected':'' }}">
                                        Students
                                    </span> 
                                </a>
                                <a href="{{ route('/admin/user/teachers') }}" class="nav_link m-0">
                                    <span class="nav_name {{ Request::is('admin/user/teachers') ? 'navbar-selected':'' }}">
                                        Gurus
                                    </span> 
                                </a>
                                <a href="{{ route('/admin/user/admins') }}" class="nav_link">
                                    <span class="nav_name {{ Request::is('admin/user/admins') ? 'navbar-selected':'' }}">
                                        Admins
                                    </span> 
                                </a>
                            </div>
                        </div>
                        <a href="{{ route('/admin/categories') }}" class="nav_link {{ Request::is('admin/categories') ? 'nav-active':'' }}">
                            <div class="sidebar-icon">
                                <i class="fa-solid fa-list-ul {{ Request::is('admin/categories') ? 'navbar-selected':'' }}"></i>
                            </div>
                            <span class="nav_name {{ Request::is('admin/categories') ? 'navbar-selected':'' }}">
                                Categories
                            </span> 
                        </a>
                        <a href="{{ route('/admin/courses') }}" class="nav_link {{ Request::is('admin/courses') ? 'nav-active':'' }}">
                            <div class="sidebar-icon">
                                <i class="fa-solid fa-address-book {{ Request::is('admin/courses') ? 'navbar-selected':'' }}"></i>
                            </div>
                            <span class="nav_name {{ Request::is('admin/courses') ? 'navbar-selected':'' }}">
                                Courses
                            </span> 
                        </a>
                    </div>
                </div>
            </div>
        </nav>
    </div>
    <div class="main-app">
        @yield('content')
        @include('partials.footer')
    </div>
</div>
@else
<div class="">
    @include('partials.navbar_nologin')
    <div class="">
        @yield('content')
        @include('partials.footer')
    </div>
</div>
@endif
