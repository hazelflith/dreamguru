<footer class="main-padding-h bg-second">
    <div class="py-5">
      <div class="d-flex py-4">
          <div class="">
                <div class="mb-4 mb-lg-0 footer-container"><img src="/img/dreamguru-white.png" alt="logo-footer" width="180" class="mb-3">
                    <p class="font-italic text-muted mb-3">
                        Scbd, One Pacific Place 15th, Jl. Jenderal Sudirman No.Kav. 52-53, RT.5/RW.3, Senayan, Jakarta, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12190
                    </p>
                        <div class="row">
                            <div class="col-xl-5">
                                <a class="text-muted me-3" href="mailto:hi@dreamaxtion.com">
                                    <i class="fa-solid fa-envelope"></i>
                                    hi@dreamaxtion.com
                                </a>   
                            </div>
                            <div class="col-xl-5">
                                <a class="text-muted me-3" 
                                href="https://api.whatsapp.com/send?phone=6285211888638&amp;text=Hi,%20I%27d%20like%20to%20know%20more%20about%20Dreamaxtion.%20%5Bfrom%20Dreamaxtion%20homepage%5D" target="_blank" class="text-white">
                                    <i class="fa-brands fa-whatsapp"></i>
                                    Chat with us
                                </a>
                            </div>
                        </div>
                        {{-- SOCIAL ICONS --}}
                        {{-- <ul class="list-inline mt-4">
                            <li class="list-inline-item">
                                <a href="#" target="_blank" title="twitter">
                                    <i class="fa-brands fa-twitter"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#" target="_blank" title="facebook">
                                    <i class="fa-brands fa-facebook"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#" target="_blank" title="instagram">
                                    <i class="fa-brands fa-instagram"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#" target="_blank" title="pinterest">
                                    <i class="fa-brands fa-linkedin"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#" target="_blank" title="pinterest">
                                    <i class="fa-brands fa-youtube"></i>
                                </a>
                            </li>
                        </ul> --}}
                        <p class="text-muted mb-0 py-4">
                            © 2022 Dreamguru All rights reserved.
                        </p>
                </div>
            </div>
        </div>
    </div>

    <!-- Copyrights -->
</footer>
<!-- End -->