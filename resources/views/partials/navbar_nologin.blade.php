<nav class="navbar navbar-expand-sm bg-second header" id='header'>
        <a href="/" class="px-4 flex-shrink-0" id="logo">
            <img class="" src="/img/dreamguru.png" alt="logo" height="32">
        </a>
        <div class="input-group me-4 ms-2 d-none d-sm-flex hidden" id="margin3">
            <input class="form-control form-dark rounded-end" type="search" placeholder="Search">
            <span class="input-group-append">
                <button class="btn btn-link ms-n5 color-muted" type="button" id="button-addon2">
                    <i class="fa-solid fa-magnifying-glass"></i>
                </button>
            </span>
        </div>
        <div class="">
            <ul class="d-none d-sm-flex navbar-nav align-items-center">
                <li class="nav-item">
                    <div class="d-flex">
                        <a href="/login" class="btn  me-2 fill-gold fw-semi" role="button">
                            Login
                        </a>
                        <a href="/register" class="btn  me-2 outline-gold" role="button">
                            Register
                        </a>
                    </div>
                </li>
                {{-- <li class="nav-item dropdown flex-shrink-0">
                    <a class="nav-link dropdown-toggle " href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown"
                        aria-expanded="false">
                        <img src="/img/english.png" height="28">
                    </a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="#"> 
                            <img src="/img/english.png" height="24"
                                    class="mr-1 mb-1">English</a></li>
                        <li><a class="dropdown-item" href="#"> 
                            <img src="/img/indonesia.png" height="24"
                                    class="mr-1 mb-1">Bahasa Indonesia</a></li>
                    </ul>
                </li> --}}
            </ul>
            <div class="right-toggler bx-x ps-4 d-sm-none">
                <div class="nav-item dropdown flex-shrink-0">
                    <a class="nav-link " href="#" id="navbarRightDropdown" role="button" data-bs-toggle="dropdown"
                        aria-expanded="false">
                        <i id="right-toggler" class="fas fa-bars text-white"></i> 
                    </a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarRightDropdown">
                        <li>
                            <a class="dropdown-item" href="/login"> 
                                Login
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="/register"> 
                                Register
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
</nav>
