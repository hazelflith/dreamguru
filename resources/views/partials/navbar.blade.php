<nav class="bg-second" id='header'>
    <div class="d-flex header align-items-center trans5">
        <div class="header_toggle bx-x ps-4 pe-3" id="header-toggle"><i class="fas fa-bars text-white"></i> </div>
        <a href="/" class="ps-2 pe-4 flex-shrink-0 me-auto" id="logo">
            <img class="" src="/img/dreamguru.png" alt="logo" height="32">
        </a>
        {{-- <div class="d-none d-md-flex align-items-center">
            <img class="me-3" src="/img/dreamtoken.png" width="auto" height="36" alt="token" id="margin2">
            <div class="me-4 ">
                <p class="color-white m-0 fw-semi">DreamToken</p>
                <p class="color-gold m-0">2.300 Points</p>
            </div>
        </div> --}}
        <div class="hide w-100">
            <div class="input-group me-4 ms-1" id="margin3">
                <input class="form-control form-dark rounded-end" type="search" placeholder="Search">
                <span class="input-group-append me-4">
                    <button class="btn btn-link ms-n5 color-muted" type="button" id="button-addon2">
                        <i class="fa-solid fa-magnifying-glass"></i>
                    </button>
                </span>
            </div>
        </div>
        <div class="d-none d-sm-block" v-if="'{{session('data')['role']}}' == 'teacher'" v-cloak>
            <a v-if="$cookies.get('dreamguru_role') == 'teacher'" href="javascript:void(0)" class="btn fill-gold fw-semi me-3" 
                @click="switchRole('student')" v-cloak>
                Guru
            </a>
            <a v-if="$cookies.get('dreamguru_role') == 'student'" href="javascript:void(0)" class="btn fill-gold fw-semi me-3" 
                @click="switchRole('teacher')" v-cloak>
                Student
            </a>
        </div>
        {{-- <li class="nav-item">
            <a href="#" class="">
                <img src="/img/notification.png" height="28">
            </a>
        </li> --}}
        {{-- <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown"
                aria-expanded="false">
                <img src="/img/english.png" height="28">
            </a>
            <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                <li>
                    <a class="dropdown-item" href="#"> <img src="/img/english.png" height="24"
                            class="me-1 mb-1">English</a></li>
                <li>
                    <a class="dropdown-item" href="#"> <img src="/img/indonesia.png" height="24"
                            class="me-1 mb-1">Bahasa</a></li>
            </ul>
        </li> --}}
        <p class="color-gold fw-bold me-3 d-none d-sm-block">
            Hi {{ucwords(strtolower(session('data')['profile']['name']))}}!
        </p>
            <a class="me-2" href="#" id="navbarProfile" role="button" data-bs-toggle="dropdown"
                aria-expanded="false">
                @if(session('data')['profile']['avatar'])
                    <img  class="avatar" src="{{env('MIX_API_URL')}}/storage/{{session('data')['profile']['avatar']}}">
                @else
                    <img src="/img/placeholder.png" height="36">
                @endif
            </a>
            <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarProfile" v-cloak>
                <li>
                    <a class="dropdown-item" href="/profile">
                        Profile
                    </a>
                </li>
                <li> 
                    <form action="/logout" method="POST">
                        @csrf
                        <input class="dropdown-item color-red" role="button" type="submit" value="Logout">
                    </form>
                </li>
                <li class="d-block d-md-none"
                v-if="'{{session('data')['role']}}' == 'teacher' && $cookies.get('dreamguru_role') == 'teacher'"> 
                    <a href="javascript:void(0)" class="color-gold dropdown-item" @click="switchRole('student')">Guru</a>
                </li>
                <li class="d-block d-md-none"
                v-if="'{{session('data')['role']}}' == 'teacher' && $cookies.get('dreamguru_role') == 'student'"> 
                    <a href="javascript:void(0)" class="color-gold dropdown-item" @click="switchRole('teacher')">Student</a>
                </li>
            </ul>
    </div>
</nav>
<script type="application/javascript">
    var bearer = "{{session('token')}}"
</script>

