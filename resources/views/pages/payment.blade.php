@extends('layout.default')

@section('meta')
    @component('components.meta')
    @slot('title')
        Dreamguru | Payment
    @endslot

    @slot('description')
        Dreamaxtion helps companies and people maximize potential through Human Capital management system and psychometric
        assessment for personal and career growth.
    @endslot
    @endcomponent
@endsection

@section('css')
<style>

</style>
@endsection

@section('content')
<div class="main-padding-h main-padding-v">
    <h3 class="color-gold fw-semi">
        Payment Options
    </h3>
    <h6 class="color-muted">
        Choose your payment method.
    </h6>
</div>
<div class="row g-0" v-cloak>
    <div class="col-7" v-if="is_course_ready">
        <div class="main-padding-h pe-4" v-if="!freeCourse">
            <div class="accordion mb-4" id="accordionPanelsStayOpenExample">
                <div class="accordion-item">
                    <h2 class="accordion-header" id="panelsStayOpen-cardOne">
                        <button 
                            class="accordion-button" 
                            type="button" 
                            data-bs-toggle="collapse"
                            data-bs-target="#panelsStayOpen-cardcollapseOne" 
                            aria-expanded="true"
                            aria-controls="panelsStayOpen-cardcollapseOne"
                        >
                            Card Payment
                        </button>
                    </h2>
                    <div id="panelsStayOpen-cardcollapseOne" class="accordion-collapse collapse show"
                        aria-labelledby="panelsStayOpen-cardOne"
                    >
                        <div class="accordion-body">
                            <i class="fa-solid fa-credit-card me-2 color-gold"></i>
                            <a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#CardDetails">
                                Credit Card / Debit Card
                            </a>
                            <!-- Modal -->
                            <div class="modal fade" id="CardDetails" tabindex="-1" role="dialog"
                                aria-labelledby="CardDetails" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">
                                                Card Payment Details
                                            </h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div id="card_validation_status" class="mb-2 alert d-none"></div>
                                            <div class="row mb-2">
                                                <div class="col-12">
                                                    <label for="card_number">
                                                        Card Number
                                                    </label>
                                                    <input class="form-control form-dark" type="number" id="card_number" required>
                                                </div>
                                            </div>
                                            <div class="row mb-2">
                                                <div class="col-6">
                                                    <label for="card_month">
                                                        Expiration
                                                    </label>
                                                    <div class="input-group">
                                                        <input type="text" 
                                                            aria-label="Month"
                                                            class="form-control form-dark" 
                                                            id="card_exp_month"
                                                            placeholder="MM" 
                                                            maxlength="2"
                                                        >
                                                        <input type="text" 
                                                            aria-label="Year"
                                                            class="form-control form-dark" 
                                                            id="card_exp_year"
                                                            placeholder="YYYY" 
                                                            maxlength="4"
                                                        >
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <label for="card_cvv">
                                                        CVV
                                                    </label>
                                                    <input class="form-control form-dark" type="number" id="card_cvv" required>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col">
                                                    <p style="font-size:40px">
                                                        <img src="/img/visa.png" height="70" class="" alt="visa">
                                                        <img src="/img/mastercard.png" height="50" class="me-2" alt="mastercard">
                                                        <img src="/img/jcb.png" height="65" class="me-2" alt="jcb">
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="hr-gold"></div>
                                        <div class="modal-body">
                                            <form action="/midtrans/chargeCard" method="post" id="tokenForm">
                                                @csrf
                                                <div class="row mb-2">
                                                    <div class="col-6">
                                                        <label for="first_name">
                                                            Nama Depan
                                                        </label>
                                                        <input class="form-control form-dark" id="first_name"
                                                            type="text" v-model="customer.first_name" required
                                                        >
                                                    </div>
                                                    <div class="col-6">
                                                        <label for="last_name">
                                                            Nama Belakang
                                                        </label>
                                                        <input class="form-control form-dark" type="text" id="last_name"
                                                            v-model="customer.last_name" required
                                                        >
                                                    </div>
                                                </div>
                                                <div class="row mb-2">
                                                    <div class="col-12">
                                                        <label for="address">
                                                            Alamat
                                                        </label>
                                                        <input class="form-control form-dark" type="text" id="address"
                                                            v-model="customer.address" required
                                                        >
                                                    </div>
                                                </div>
                                                <div class="row mb-2">
                                                    <div class="col-6">
                                                        <label for="city">
                                                            Kota
                                                        </label>
                                                        <input class="form-control form-dark" type="text" id="city"
                                                            v-model="customer.city" required
                                                        >
                                                    </div>
                                                    <div class="col-6">
                                                        <label for="postal_code">
                                                            Kode Pos
                                                        </label>
                                                        <input class="form-control form-dark" type="number"
                                                            id="postal_code" v-model="customer.postal_code" required
                                                        >
                                                    </div>
                                                </div>
                                                <div class="row mb-2">
                                                    <div class="col-12">
                                                        <label for="country_code">
                                                            Negara
                                                        </label>
                                                        <select class="form-select form-dark" v-model="customer.country_code"
                                                            id="country_code" required
                                                        >
                                                            <option value="IDN">Indonesia</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="card_token" id="card_token">
                                                <button type="button" class="d-none" id="cardSubmitButton" @click="charge('credit_card')"></button>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary outline-gold"
                                                data-bs-dismiss="modal"
                                            >
                                                Kembali
                                            </button>
                                            <button type="button" class="btn fill-gold fw-semi"
                                                @click="getToken()"
                                            >
                                                Bayar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion mb-4" id="accordionPanelsStayOpenExample">
                <div class="accordion-item">
                    <h2 class="accordion-header" id="panelsStayOpen-VAOne">
                        <button class="accordion-button" 
                            type="button" 
                            data-bs-toggle="collapse"
                            data-bs-target="#panelsStayOpen-VAcollapseOne" 
                            aria-expanded="true"
                            aria-controls="panelsStayOpen-VAcollapseOne"
                        >
                            Virtual Account
                        </button>
                    </h2>
                    <div id="panelsStayOpen-VAcollapseOne" class="accordion-collapse collapse show"
                        aria-labelledby="panelsStayOpen-VAOne"
                    >
                        <div class="accordion-body d-flex">
                            <div class="icons me-2">
                                <img src="/img/mandiri.png" alt="mandiri">
                            </div>
                            <a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#mandiriModal" class="w-100">
                                Mandiri Virtual Account
                            </a>
                            <!-- Modal -->
                            <div class="modal fade" id="mandiriModal" tabindex="-1" aria-labelledby="mandiriModalLabel"
                                aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="mandiriModalLabel">
                                                Mandiri Virtual Account
                                            </h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"
                                            ></button>
                                        </div>
                                        <div class="modal-body">
                                            <p class="color-gold mb-3">
                                                Cara Pembayaran
                                            </p>
                                            <ul class="nav nav-tabs" id="mandiriTab" role="tablist">
                                                <li class="nav-item" role="presentation">
                                                    <button 
                                                        class="nav-link active" 
                                                        id="atm-mandiri" 
                                                        data-bs-toggle="tab" 
                                                        data-bs-target="#atm_mandiri" 
                                                        type="button" 
                                                        role="tab" 
                                                        aria-controls="atm_mandiri" 
                                                        aria-selected="true"
                                                    >
                                                        ATM Mandiri
                                                    </button>
                                                </li>
                                                <li class="nav-item" role="presentation">
                                                    <button 
                                                        class="nav-link" 
                                                        id="ib-mandiri" 
                                                        data-bs-toggle="tab" 
                                                        data-bs-target="#ib_mandiri" 
                                                        type="button" 
                                                        role="tab" 
                                                        aria-controls="ib_mandiri" 
                                                        aria-selected="false"
                                                    >
                                                        Mandiri Internet Banking
                                                    </button>
                                                </li>
                                            </ul>
                                            <div class="tab-content" id="myTabContent">
                                                <div class="tab-pane fade show active" id="atm_mandiri" role="tabpanel" aria-labelledby="atm-mandiri">
                                                    <div class="row mt-3">
                                                        <div class="col">
                                                            <ol>
                                                                <li>Pada menu utama, pilih Bayar/Beli.</li>
                                                                <li>Pilih Lainnya.</li>
                                                                <li>Pilih Multi Payment.</li>
                                                                <li>Masukkan 70012 (kode perusahaan Midtrans) lalu tekan Benar.</li>
                                                                <li>Masukkan Kode Pembayaran Anda lalu tekan Benar.</li>
                                                                <li>Pada halaman konfirmasi akan muncul detail pembayaran Anda. Jika informasi telah sesuai tekan Ya.</li>
                                                            </ol>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="ib_mandiri" role="tabpanel" aria-labelledby="ib-mandiri">
                                                <div class="row mt-3">
                                                    <div class="col">
                                                            <ol>
                                                                <li>Login ke Internet Banking Mandiri (https://ibank.bankmandiri.co.id/).</li>
                                                                <li>Pada menu utama, pilih Bayar, lalu pilih Multi Payment.</li>
                                                                <li>Pilih akun Anda di Dari Rekening, kemudian di Penyedia Jasa pilih Midtrans.</li>
                                                                <li>Masukkan Kode Pembayaran Anda dan klik Lanjutkan.</li>
                                                                <li>Konfirmasi pembayaran Anda menggunakan Mandiri Token.</li>
                                                            </ol>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary outline-gold me-2"
                                                data-bs-dismiss="modal"
                                            >
                                                Kembali
                                            </button>
                                            <a class="btn fill-gold fw-semi" @click="setPayment('mandiriVA')" data-bs-dismiss="modal">
                                                Pilih Pembayaran
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-body d-flex">
                            <div class="icons me-2">
                                <img src="/img/bni.png" alt="bni">
                            </div>
                            <a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#bniModal" class="w-100">
                                BNI Virtual Account
                            </a>
                            <!-- Modal -->
                            <div class="modal fade" id="bniModal" tabindex="-1" aria-labelledby="bniModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="bniModalLabel">
                                                BNI Virtual Account
                                            </h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"
                                            ></button>
                                        </div>
                                        <div class="modal-body">
                                            <p class="color-gold mb-3">
                                                Cara Pembayaran
                                            </p>
                                            <ul class="nav nav-tabs" id="bniTab" role="tablist">
                                                <li class="nav-item" role="presentation">
                                                    <button 
                                                        class="nav-link active" 
                                                        id="atm-bni" 
                                                        data-bs-toggle="tab" 
                                                        data-bs-target="#atm_bni" 
                                                        type="button" 
                                                        role="tab" 
                                                        aria-controls="atm_bni" 
                                                        aria-selected="true"
                                                    >
                                                        ATM BNI
                                                    </button>
                                                </li>
                                                <li class="nav-item" role="presentation">
                                                    <button 
                                                        class="nav-link" 
                                                        id="ib-bni" 
                                                        data-bs-toggle="tab" 
                                                        data-bs-target="#ib_bni" 
                                                        type="button" 
                                                        role="tab" 
                                                        aria-controls="ib_bni" 
                                                        aria-selected="false"
                                                    >
                                                        BNI Internet Banking
                                                    </button>
                                                </li>
                                                <li class="nav-item" role="presentation">
                                                    <button 
                                                        class="nav-link" 
                                                        id="m-bni" 
                                                        data-bs-toggle="tab" 
                                                        data-bs-target="#m_bni" 
                                                        type="button" 
                                                        role="tab" 
                                                        aria-controls="m_bni" 
                                                        aria-selected="false"
                                                    >
                                                        BNI Mobile Banking
                                                    </button>
                                                </li>
                                            </ul>
                                            <div class="tab-content" id="bniContent">
                                                <div class="tab-pane fade show active" id="atm_bni" role="tabpanel" aria-labelledby="atm-bni">
                                                    <div class="row mt-3">
                                                        <div class="col">
                                                            <ol>
                                                                <li>Pada menu utama, pilih Menu Lainnya.</li>
                                                                <li>Pilih Transfer.</li>
                                                                <li>Pilih Rekening Tabungan.</li>
                                                                <li>Pilih Ke Rekening BNI.</li>
                                                                <li>Masukkan nomor virtual account dan pilih Tekan Jika Benar.</li>
                                                                <li>Masukkan jumlah tagihan yang akan anda bayar secara lengkap. Pembayaran dengan jumlah tidak sesuai akan otomatis ditolak.</li>
                                                                <li>Jumlah yang dibayarkan, nomor rekening dan nama Merchant akan ditampilkan. Jika informasi telah sesuai, tekan Ya.</li>
                                                                <li>Transaksi Anda sudah selesai.</li>
                                                            </ol>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="ib_bni" role="tabpanel" aria-labelledby="ib-bni">
                                                    <div class="row mt-3">
                                                        <div class="col">
                                                            <ol>
                                                                <li>Ketik alamat https://ibank.bni.co.id kemudian klik Masuk.</li>
                                                                <li>Silakan masukkan User ID dan Password.</li>
                                                                <li>Klik menu Transfer kemudian pilih Tambah Rekening Favorit.</li>
                                                                <li>Masukkan nama, nomor rekening, dan email, lalu klik Lanjut.</li>
                                                                <li>Masukkan Kode Otentikasi dari token Anda dan klik Lanjut.</li>
                                                                <li>Kembali ke menu utama dan pilih Transfer lalu Transfer Antar Rekening BNI.</li>
                                                                <li>Pilih rekening yang telah Anda favoritkan sebelumnya di Rekening Tujuan lalu lanjutkan pengisian, dan tekan Lanjut.</li>
                                                                <li>Pastikan detail transaksi Anda benar, lalu masukkan Kode Otentikasi dan tekan Lanjut.</li>
                                                            </ol>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="m_bni" role="tabpanel" aria-labelledby="m-bni">
                                                    <div class="row mt-3">
                                                        <div class="col">
                                                            <ol>
                                                                <li>Buka aplikasi BNI Mobile Banking dan login</li>
                                                                <li>Pilih menu Transfer</li>
                                                                <li>Pilih menu Virtual Account Billing</li>
                                                                <li>Pilih rekening debit yang akan digunakan</li>
                                                                <li>Pilih menu Input Baru dan masukkan 16 digit nomor Virtual Account</li>
                                                                <li>Informasi tagihan akan muncul pada halaman validasi</li>
                                                                <li>Jika informasi telah sesuai, masukkan Password Transaksi dan klik Lanjut</li>
                                                                <li>Transaksi Anda akan diproses</li>
                                                            </ol>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
            
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary outline-gold me-2"
                                                data-bs-dismiss="modal"
                                            >
                                                Kembali
                                            </button>
                                            <a class="btn fill-gold fw-semi" @click="setPayment('bniVA')" data-bs-dismiss="modal">
                                                Pilih Pembayaran
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-body d-flex">
                            <div class="icons me-2">
                                <img src="/img/bca.png" alt="bca">
                            </div>
                            <a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#bcaModal" class="w-100">
                                BCA Virtual Account
                            </a>
                            <!-- Modal -->
                            <div class="modal fade" id="bcaModal" tabindex="-1" aria-labelledby="bcaModalLabel"
                                aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="mandiriModalLabel">
                                                BCA Virtual Account
                                            </h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"
                                            ></button>
                                        </div>
                                        <div class="modal-body">
                                            <p class="color-gold mb-3">
                                                Cara Pembayaran
                                            </p>
                                            <ul class="nav nav-tabs" id="bcaTab" role="tablist">
                                                <li class="nav-item" role="presentation">
                                                    <button 
                                                        class="nav-link active" 
                                                        id="atm-bca" 
                                                        data-bs-toggle="tab" 
                                                        data-bs-target="#atm_bca" 
                                                        type="button" 
                                                        role="tab" 
                                                        aria-controls="atm_bca" 
                                                        aria-selected="true"
                                                    >
                                                        ATM BCA</button>
                                                </li>
                                                <li class="nav-item" role="presentation">
                                                    <button 
                                                        class="nav-link" 
                                                        id="klik-bca" 
                                                        data-bs-toggle="tab" 
                                                        data-bs-target="#klik_bca" 
                                                        type="button" 
                                                        role="tab" 
                                                        aria-controls="klik_bca" 
                                                        aria-selected="false"
                                                    >
                                                        Klik BCA</button>
                                                </li>
                                                <li class="nav-item" role="presentation">
                                                    <button 
                                                        class="nav-link" 
                                                        id="m-bca" 
                                                        data-bs-toggle="tab" 
                                                        data-bs-target="#m_bca" 
                                                        type="button" 
                                                        role="tab" 
                                                        aria-controls="m_bca" 
                                                        aria-selected="false"
                                                    >
                                                        m-BCA</button>
                                                </li>
                                            </ul>
                                            <div class="tab-content" id="bcaContent">
                                                <div class="tab-pane fade show active" id="atm_bca" role="tabpanel" aria-labelledby="atm-bca">
                                                    <div class="row mt-3">
                                                        <div class="col">
                                                            <ol>
                                                                <li>Pada menu utama, pilih Transaksi Lainnya.</li>
                                                                <li>Pilih Transfer.</li>
                                                                <li>Pilih Ke Rek BCA Virtual Account.</li>
                                                                <li>Masukkan Nomor Rekening pembayaran (11 digit) Anda lalu tekan Benar.</li>
                                                                <li>Masukkan jumlah tagihan yang akan anda bayar.</li>
                                                                <li>Pada halaman konfirmasi transfer akan muncul detail pembayaran Anda. Jika informasi telah sesuai tekan Ya.</li>
                                                            </ol>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="klik_bca" role="tabpanel" aria-labelledby="klik-bca">
                                                    <div class="row mt-3">
                                                        <div class="col">
                                                            <ol>
                                                                <li>Pilih menu Transfer Dana.</li>
                                                                <li>Pilih Transfer ke BCA Virtual Account.</li>
                                                                <li>Masukkan nomor BCA Virtual Account, atau pilih Dari Daftar Transfer.</li>
                                                                <li>Ambil BCA Token Anda dan masukkan KEYBCA Response APPLI 1 dan Klik Submit.</li>
                                                                <li>Transaksi Anda selesai.</li>
                                                            </ol>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="m_bca" role="tabpanel" aria-labelledby="m-bca">
                                                    <div class="row mt-3">
                                                        <div class="col">
                                                            <ol>
                                                                <li>Lakukan log in pada aplikasi BCA Mobile.</li>
                                                                <li>Pilih menu m-BCA, kemudian masukkan kode akses m-BCA.</li>
                                                                <li>Pilih m-Transfer > BCA Virtual Account.</li>
                                                                <li>Pilih dari Daftar Transfer, atau masukkan Nomor Virtual Account tujuan.</li>
                                                                <li>Masukkan jumlah yang ingin dibayarkan.</li>
                                                                <li>Masukkan pin m-BCA.</li>
                                                                <li>Pembayaran selesai. Simpan notifikasi yang muncul sebagai bukti pembayaran.</li>
                                                            </ol>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary outline-gold me-2"
                                                data-bs-dismiss="modal"
                                            >
                                                Kembali
                                            </button>
                                            <a class="btn fill-gold fw-semi" @click="setPayment('bcaVA')" data-bs-dismiss="modal">
                                                Pilih Pembayaran
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-body d-flex">
                            <div class="icons me-2">
                                <img src="/img/permata.png" alt="permata">
                            </div>
                            <a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#permataModal" class="w-100">
                                Permata Virtual Account
                            </a>
                            <!-- Modal -->
                            <div class="modal fade" id="permataModal" tabindex="-1" aria-labelledby="permataModalLabel"
                                aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="permataModalLabel">
                                                Permata Virtual Account
                                            </h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"
                                            ></button>
                                        </div>
                                        <div class="modal-body">
                                            <p class="color-gold mb-3">
                                                Cara Pembayaran
                                            </p>
                                            <ul class="nav nav-tabs" id="permataTab" role="tablist">
                                                <li class="nav-item" role="presentation">
                                                    <button 
                                                        class="nav-link active" 
                                                        id="atm-permata" 
                                                        data-bs-toggle="tab" 
                                                        data-bs-target="#atm_permata" 
                                                        type="button" 
                                                        role="tab" 
                                                        aria-controls="atm_permata" 
                                                        aria-selected="true"
                                                    >
                                                        ATM Permata
                                                    </button>
                                                </li>
                                            </ul>
                                            <div class="tab-content" id="permataContent">
                                                <div class="tab-pane fade show active" id="atm_permata" role="tabpanel" aria-labelledby="atm-permata">
                                                    <div class="row mt-3">
                                                        <div class="col">
                                                            <ol>
                                                                <li>Pada menu utama, pilih Transaksi Lainnya.</li>
                                                                <li>Pilih Pembayaran.</li>
                                                                <li>Pilih Pembayaran Lainnya.</li>
                                                                <li>Pilih Virtual Account.</li>
                                                                <li>Masukkan 16 digit No. Virtual Account yang dituju, lalu tekan Benar.</li>
                                                                <li>Pilih rekening pembayaran Anda dan tekan Benar.</li>
                                                                <li>Transaksi Anda sudah selesai.</li>
                                                            </ol>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary outline-gold me-2"
                                                data-bs-dismiss="modal"
                                            >
                                                Kembali
                                            </button>
                                            <a class="btn fill-gold fw-semi" @click="setPayment('permataVA')" data-bs-dismiss="modal">
                                                Pilih Pembayaran
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-body d-flex">
                            <div class="icons me-2">
                                <img src="/img/briva.svg" alt="briva">
                            </div>
                            <a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#briModal" class="w-100">
                                BRIVA
                            </a>
                            <!-- Modal -->
                            <div class="modal fade" id="briModal" tabindex="-1" aria-labelledby="briModalLabel"
                                aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="briModalLabel">
                                                BRI Virtual Account
                                            </h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"
                                            ></button>
                                        </div>
                                        <div class="modal-body">
                                            <p class="color-gold mb-3">
                                                Cara Pembayaran
                                            </p>
                                            <ul class="nav nav-tabs" id="briTab" role="tablist">
                                                <li class="nav-item" role="presentation">
                                                    <button 
                                                        class="nav-link active" 
                                                        id="atm-bri" 
                                                        data-bs-toggle="tab" 
                                                        data-bs-target="#atm_bri" 
                                                        type="button" 
                                                        role="tab" 
                                                        aria-controls="atm_bri" 
                                                        aria-selected="true"
                                                    >
                                                        ATM BRI
                                                    </button>
                                                </li>
                                                <li class="nav-item" role="presentation">
                                                    <button 
                                                        class="nav-link" 
                                                        id="ib-bri" 
                                                        data-bs-toggle="tab" 
                                                        data-bs-target="#ib_bri" 
                                                        type="button" 
                                                        role="tab" 
                                                        aria-controls="ib_bri" 
                                                        aria-selected="false"
                                                    >
                                                        IB BRI
                                                    </button>
                                                </li>
                                                <li class="nav-item" role="presentation">
                                                    <button 
                                                        class="nav-link" 
                                                        id="m-bri" 
                                                        data-bs-toggle="tab" 
                                                        data-bs-target="#m_bri" 
                                                        type="button" 
                                                        role="tab" 
                                                        aria-controls="m_bri" 
                                                        aria-selected="false"
                                                    >
                                                        BRImo
                                                    </button>
                                                </li>
                                            </ul>
                                            <div class="tab-content" id="briContent">
                                                <div class="tab-pane fade show active" id="atm_bri" role="tabpanel" aria-labelledby="atm-bri">
                                                    <div class="row mt-3">
                                                        <div class="col">
                                                            <ol>
                                                                <li>Pilih menu utama, pilih Transaksi Lain.</li>
                                                                <li>Pilih Pembayaran.</li>
                                                                <li>Pilih Lainnya.</li>
                                                                <li>Pilih BRIVA.</li>
                                                                <li>Masukkan Nomor BRIVA pelanggan dan pilih Benar.</li>
                                                                <li>Jumlah pembayaran, nomor BRIVA dan nama merchant akan muncul pada halaman konfirmasi pembayaran. Jika informasi yang dicantumkan benar, pilih Ya.</li>
                                                                <li>Pembayaran telah selesai. Simpan bukti pembayaran Anda.</li>
                                                            </ol>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="ib_bri" role="tabpanel" aria-labelledby="ib-bri">
                                                    <div class="row mt-3">
                                                        <div class="col">
                                                            <ol>
                                                                <li>Masuk pada Internet Banking BRI.</li>
                                                                <li>Pilih menu Pembayaran & Pembelian.</li>
                                                                <li>Pilih sub menu BRIVA.</li>
                                                                <li>Masukkan Nomor BRIVA.</li>
                                                                <li>Jumlah pembayaran, nomor pembayaran, dan nama merchant akan muncul pada halaman konfirmasi pembayaran. Jika informasi yang dicantumkan benar, pilih Kirim.</li>
                                                                <li>Masukkan password dan mToken, pilih Kirim.</li>
                                                                <li>Masukkan password dan mToken, pilih Kirim.</li>
                                                                <li>Pembayaran telah selesai. Untuk mencetak bukti transaksi, pilih Cetak.</li>
                                                            </ol>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="m_bri" role="tabpanel" aria-labelledby="m-bri">
                                                    <div class="row mt-3">
                                                        <div class="col">
                                                            <ol>
                                                                <li>Masuk ke dalam aplikasi BRI Mobile, pilih Mobile Banking BRI.</li>
                                                                <li>Pilih Pembayaran, lalu pilih BRIVA.</li>
                                                                <li>Masukkan nomor BRIVA.</li>
                                                                <li>Jumlah pembayaran, nomor pembayaran, dan nama merchant akan muncul pada halaman konfirmasi pembayaran. Jika informasi yang dicantumkan benar, pilih Continue.</li>
                                                                <li>Masukkan Mobile Banking BRI PIN, pilih Ok.</li>
                                                                <li>Pembayaran telah selesai. Simpan notifikasi sebagai bukti pembayaran.</li>
                                                            </ol>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary outline-gold me-2"
                                                data-bs-dismiss="modal"
                                            >
                                                Kembali
                                            </button>
                                            <a href="javascript:void(0)" class="btn fill-gold fw-semi"
                                                @click="setPayment('briVA')" data-bs-dismiss="modal"
                                            >
                                                Pilih Pembayaran
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion mb-4" id="accordionPanelsStayOpenExample">
                <div class="accordion-item">
                    <h2 class="accordion-header " id="panelsStayOpen-ewalletOne">
                        <button 
                            class="accordion-button collapsed" 
                            type="button" data-bs-toggle="collapse"
                            data-bs-target="#panelsStayOpen-ewalletcollapseOne" 
                            aria-expanded="false"
                            aria-controls="panelsStayOpen-ewalletcollapseOne"
                        >
                            e-Wallet
                        </button>
                    </h2>
                    <div id="panelsStayOpen-ewalletcollapseOne" class="accordion-collapse collapse"
                        aria-labelledby="panelsStayOpen-ewalletOne"
                    >
                        <div class="accordion-body d-flex">
                            <div class="icons me-2">
                                <img src="/img/gopay.png" alt="gopay">
                            </div>
                            <a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#gopayModal" class="w-100">
                                GoPay
                            </a>
                            <!-- Modal -->
                            <div class="modal fade" id="gopayModal" tabindex="-1" aria-labelledby="gopayModalLabel"
                                aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="gopayModalLabel">
                                                GoPay Payment Details
                                            </h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"
                                            ></button>
                                        </div>
                                        <div class="modal-body">
                                            <p class="color-gold mb-3">
                                                Cara Pembayaran
                                            </p>
                                            <ol>
                                                <li>Buka aplikasi Gojek</li>
                                                <li>Klik menu 'Bayar'</li>
                                                <li>Pindai kode QR yang ada pada monitor Anda.</li>
                                                <li>Periksa detail transaksi Anda pada aplikasi, lalu tap tombol Bayar.</li>
                                                <li>Masukkan PIN dan selesaikan transaksi kamu.</li>
                                            </ol>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary outline-gold me-2"
                                                data-bs-dismiss="modal"
                                            >
                                                Kembali
                                            </button>
                                            <a href="javascript:void(0)" @click="setPayment('gopay')" data-bs-dismiss="modal"
                                                class="btn  fill-gold fw-semi"
                                            >
                                                Pilih Pembayaran
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-body d-flex">
                            <div class="icons me-2">
                                <img src="/img/ovo.png" alt="ovo">
                            </div>
                            <a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#ovoModal" class="w-100">
                                OVO
                            </a>
                            <!-- Modal -->
                            <div class="modal fade" id="ovoModal" tabindex="-1" aria-labelledby="ovoModalLabel"
                                aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="ovoModalLabel">
                                                OVO Payment Details
                                            </h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"
                                            ></button>
                                        </div>
                                        <div class="modal-body">
                                            <div v-if="error_show" class="alert alert-danger" role="alert">
                                                <div v-for="value in error_show">
                                                    <p class="color-red">
                                                        @{{ value[0] }}
                                                    </p>
                                                </div>                
                                            </div>
                                            <label>
                                                Masukkan Nomor Handphone
                                            </label>
                                            <div class="input-group mb-3">
                                                <span class="input-group-text" id="basic-addon1">
                                                    +62
                                                </span>
                                                <input 
                                                    type="number" 
                                                    name="phone_number" 
                                                    id="phone_number"  
                                                    v-model="phone_number" 
                                                    class="form-control form-dark" 
                                                    placeholder="Masukkan Nomor Telefon" required
                                                >
                                            </div>
                                            <p class="color-gold mb-3">
                                                Cara Pembayaran
                                            </p>
                                            <ol>
                                                <li>Masukkan Nomor Handphone</li>
                                                <li>Klik tombol Bayar</li>
                                                <li>Pembayaran diproses</li>
                                                <li>Anda akan menerima notifikasi pada aplikasi OVO</li>
                                                <li>Anda dapat memeriksa detail pembayaran</li>
                                                <li>Klik Bayar</li>
                                                <li>Transaksi Berhasil</li>
                                            </ol>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary outline-gold me-2"
                                                data-bs-dismiss="modal"
                                            >
                                                Kembali
                                            </button>
                                            <a class="btn fill-gold fw-semi" @click="setPayment('ovo')" data-bs-dismiss="modal">
                                                Pilih Pembayaran
                                            </a>
                                            <a  data-bs-target="#ovo2Modal" data-bs-toggle="modal" 
                                                data-bs-dismiss="modal" id="hiddenOVOModalButton" class="hide"
                                            >
                                                Hidden
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- Modal --}}
                            <div class="modal fade" id="ovo2Modal" tabindex="-1" aria-labelledby="ovo2ModalLabel" data-bs-backdrop="static" data-bs-keyboard="false"
                                aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="ovo2ModalLabel">
                                                OVO Payment Details
                                            </h5>
                                        </div>
                                        <div class="modal-body text-center">
                                            <img src="/img/ovodone.png">
                                            <p class="my-3">
                                                Cek aplikasi OVO untuk melanjutkan Pembayaran
                                            </p>
                                        </div>
                                        <div class="modal-footer">
                                            <a :href="'/course/' + course.slug" class="btn btn-block fill-gold fw-semi">
                                                Ke Halaman Course
                                            </a>
                                            <a href="/student/dashboard/billing" class="btn btn-block fill-gold fw-semi" >
                                                Ke Halaman Billing
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-body d-flex">
                            <div class="icons me-2">
                                <img src="/img/dana.png" alt="dana">
                            </div>
                            <a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#danaModal" class="w-100">
                                Dana
                            </a>
                            {{-- MODAL --}}
                            <div class="modal fade" id="danaModal" tabindex="-1" aria-labelledby="danaModalLabel"
                                aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="danaModalLabel">
                                                Dana Payment Details
                                            </h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"
                                            ></button>
                                        </div>
                                        <div class="modal-body">
                                            <p class="color-gold mb-3">
                                                Cara Pembayaran
                                            </p>
                                            <ol>
                                                <li>Anda akan diarahkan ke halaman pembayaran DANA</li>
                                                <li>Input Nomor Handphone</li>
                                                <li>Masukkan PIN DANA</li>
                                                <li>Untuk transaksi pertama, anda akan menerima kode OTP yang harus diinputkan</li>
                                                <li>Anda dapat memeriksa detail pembayaran</li>
                                                <li>Klik Bayar</li>
                                                <li>Transaksi Selesai</li>
                                            </ol>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary outline-gold me-2"
                                                data-bs-dismiss="modal"
                                            >
                                                Kembali
                                            </button>
                                            <a href="javascript:void(0)"
                                                class="btn  fill-gold fw-semi" @click="setPayment('dana')" data-bs-dismiss="modal"
                                            >
                                                Pilih Pembayaran
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-body d-flex">
                            <div class="icons me-2">
                                <img src="/img/shopeepay.png" alt="shopeepay">
                            </div>
                            <a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#shopeePayModal" class="w-100">
                                ShopeePay
                            </a>
                            {{-- MODAL --}}
                            <div class="modal fade" id="shopeePayModal" tabindex="-1" aria-labelledby="shopeePayModalLabel"
                                aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="gopayModalLabel">
                                                ShopeePay Payment Details
                                            </h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"
                                            ></button>
                                        </div>
                                        <div class="modal-body">
                                            <p class="color-gold mb-3">
                                                Cara Pembayaran
                                            </p>
                                            <ol>
                                                <li>Anda akan diarahkan ke Aplikasi ShopeePay</li>
                                                <li>ShopeePay akan menampilkan total pembayaran</li>
                                                <li>Masukkan PIN</li>
                                                <li>Klik Lanjutkan</li>
                                                <li>Transaksi Selesai</li>
                                            </ol>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary outline-gold me-2"
                                                data-bs-dismiss="modal"
                                            >
                                                Kembali
                                            </button>
                                            <a href="javascript:void(0)" class="btn  fill-gold fw-semi" @click="setPayment('shopeePay')" data-bs-dismiss="modal">
                                                Pilih Pembayaran
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-body d-flex">
                            <div class="icons me-2">
                                <img src="/img/linkaja.png" alt="linkaja">
                            </div>
                            <a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#linkAjaModal" class="w-100">
                                LinkAja
                            </a>
                            {{-- MODAL --}}
                            <div class="modal fade" id="linkAjaModal" tabindex="-1" aria-labelledby="linkAjaModalLabel"
                                aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="linkAjaModalLabel">
                                                LinkAja Payment Details
                                            </h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"
                                            ></button>
                                        </div>
                                        <div class="modal-body">
                                            <p class="color-gold mb-3">
                                                Cara Pembayaran
                                            </p>
                                            <ol>
                                                <li>Anda akan dialihkan ke halaman pembayaran LinkAja</li>
                                                <li>Masukkan PIN</li>
                                                <li>Anda akan menerima SMS kode OTP</li>
                                                <li>Masukkan Kode OTP</li>
                                                <li>Transaksi Selesai</li>
                                            </ol>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary outline-gold me-2"
                                                data-bs-dismiss="modal"
                                            >
                                                Kembali
                                            </button>
                                            <a href="javascript:void(0)" @click="setPayment('linkAja')" class="btn fill-gold fw-semi" data-bs-dismiss="modal">
                                                Pilih Pembayaran
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="accordion" id="accordionPanelsStayOpenExample">
                <div class="accordion-item">
                    <h2 class="accordion-header" id="panelsStayOpen-retailOne">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#panelsStayOpen-retailcollapseOne" aria-expanded="false"
                            aria-controls="panelsStayOpen-retailcollapseOne">
                            Retail
                        </button>
                    </h2>
                    <div id="panelsStayOpen-retailcollapseOne" class="accordion-collapse collapse"
                        aria-labelledby="panelsStayOpen-retailOne">
                        <div class="accordion-body d-flex">
                            <div class="icons me-2">
                                <img src="/img/alfamart.svg" alt="alfamart">
                            </div>
                            <a href="javascript:void(0)" class="w-100">Alfamart</a>
                        </div>
                        <div class="accordion-body d-flex">
                            <div class="icons me-2">
                                <img src="/img/indomaret.png" alt="indomaret">
                            </div>
                            <a href="javascript:void(0)" class="w-100">Indomaret</a>
                        </div>
                    </div>
                </div>
            </div> --}}
        </div>
        <div class="main-padding-h pe-4" v-else>
            <div class="accordion mb-4" id="accordionPanelsStayOpenExample">
                <div class="accordion-item">
                    <h2 class="accordion-header" id="panelsStayOpen-dreamguruOne">
                        <button 
                            class="accordion-button" 
                            type="button" 
                            data-bs-toggle="collapse"
                            data-bs-target="#panelsStayOpen-dreamgurucollapseOne" 
                            aria-expanded="true"
                            aria-controls="panelsStayOpen-dreamgurucollapseOne"
                        >
                            Other
                        </button>
                    </h2>
                    <div id="panelsStayOpen-dreamgurucollapseOne" class="accordion-collapse collapse show"
                        aria-labelledby="panelsStayOpen-dreamguruOne"
                    >
                        <div class="accordion-body">
                            <i class="fa-solid fa-gift me-2 color-gold"></i>
                            <a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#dreamguruModal">
                                Dreamguru Free Course Claim
                            </a>
                            <!-- Modal -->
                            <div class="modal fade" id="dreamguruModal" tabindex="-1" role="dialog" aria-labelledby="dreamguruModal" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="dreamguruModal">
                                                Dreamguru
                                            </h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"
                                            ></button>
                                        </div>
                                        <div class="modal-body">
                                            Happy Learning!
                                        </div>
                                        <div class="modal-footer">
                                            <a class="btn fill-gold fw-semi" @click="setPayment('dreamguru')" data-bs-dismiss="modal">
                                                Claim Course
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-7" v-else>
        <div class="main-padding-h pe-4">
            <vue-skeleton-loader
                :height="100"
                class="w-100 mb-3"
                color="rgba(52, 52, 52, 1)"
                animation="fade"
            ></vue-skeleton-loader>
            <vue-skeleton-loader
                :height="300"
                class="w-100 mb-3"
                color="rgba(52, 52, 52, 1)"
                animation="fade"
            ></vue-skeleton-loader>
        </div>
    </div>
    <div class="col-5">
        <div class="paymentbg">
            <div class="billing-container">
                <p class="color-gold fw-semi mb-3">
                    Detail Item : 
                </p>
                <div class="d-flex justify-content-between">
                    <strong><p>
                        Judul Course
                    </p></strong>
                    <strong><p>
                        Harga
                    </p></strong>
                </div>
                <div class="d-flex justify-content-between" v-if="is_course_ready">
                    <p>
                        @{{ course.title }}
                    </p>
                    <p v-if="!course.is_on_discount">
                        Rp. @{{ price }}
                    </p>
                    <div v-if="course.is_on_discount">
                        <p class="color-muted strike">
                            Rp. @{{ price }}
                        </p>
                        <p>
                            Rp. @{{ discount_price }}
                        </p>
                    </div>
                </div>
                <div class="d-flex justify-content-between" v-if="!is_course_ready">
                    <vue-skeleton-loader
                        :height="36"
                        :width="200"
                        class="me-2"
                        color="rgba(52, 52, 52, 1)"
                        animation="fade"
                    ></vue-skeleton-loader>
                    <vue-skeleton-loader
                        :height="36"
                        :width="200"
                        color="rgba(52, 52, 52, 1)"
                        animation="fade"
                    ></vue-skeleton-loader>
                    
                </div>
                <hr style="color:#FBD68D !important">
                <div class="d-flex justify-content-between mb-2" v-if="is_course_ready">
                    <strong><p>
                        Total
                    </p></strong>
                    <strong v-if="!course.is_on_discount"><p>
                        Rp. @{{ price }}
                    </p></strong>
                    <strong v-else><p>
                        Rp. @{{discount_price}}
                    </p></strong>
                </div>
                <div>
                    <p class="mb-1"> Payment Type : </p>
                    <div class="d-flex align-items-center" v-if="payment_type == 'dreamguru'">
                        <i class="fa-solid fa-gift me-2 color-gold"></i>
                        <p>
                            Dreamguru Free Course Claim
                        </p>
                    </div>
                    <div class="d-flex align-items-center" v-if="payment_type == 'credit_card'">
                        <i class="fa-solid fa-credit-card me-2 color-gold"></i>
                        <p>
                            Credit Card / Debit Card
                        </p>
                    </div>
                    <div class="d-flex align-items-center" v-if="payment_type == 'mandiriVA'">
                        <div class="icons me-2">
                            <img src="/img/mandiri.png" alt="mandiri">
                        </div>
                        <p>
                            Mandiri Virtual Account
                        </p>
                    </div>
                    <div class="d-flex align-items-center" v-if="payment_type == 'bniVA'">
                        <div class="icons me-2">
                            <img src="/img/bni.png" alt="bni">
                        </div>
                        <p>
                            BNI Virtual Account
                        </p>
                    </div>
                    <div class="d-flex align-items-center" v-if="payment_type == 'bcaVA'">
                        <div class="icons me-2">
                            <img src="/img/bca.png" alt="bca">
                        </div>
                        <p>
                            BCA Virtual Account
                        </p>
                    </div>
                    <div class="d-flex align-items-center" v-if="payment_type == 'permataVA'">
                        <div class="icons me-2">
                            <img src="/img/permata.png" alt="permata">
                        </div>
                        <p>
                            Permata Virtual Account
                        </p>
                    </div>
                    <div class="d-flex align-items-center" v-if="payment_type == 'briVA'">
                        <div class="icons me-2">
                            <img src="/img/briva.svg" alt="briva">
                        </div>
                        <p>
                            BRIVA
                        </p>
                    </div>
                    <div class="d-flex align-items-center" v-if="payment_type == 'ovo'">
                        <div class="icons me-2">
                            <img src="/img/ovo.png" alt="ovo">
                        </div>
                        <p>
                            OVO
                        </p>
                    </div>
                    <div class="d-flex align-items-center" v-if="payment_type == 'gopay'">
                        <div class="icons me-2">
                            <img src="/img/gopay.png" alt="gopay">
                        </div>
                        <p>
                            GoPay
                        </p>
                    </div>
                    <div class="d-flex align-items-center" v-if="payment_type == 'dana'">
                        <div class="icons me-2">
                            <img src="/img/dana.png" alt="dana">
                        </div>
                        <p>
                            Dana
                        </p>
                    </div>
                    <div class="d-flex align-items-center" v-if="payment_type == 'shopeePay'">
                        <div class="icons me-2">
                            <img src="/img/shopeepay.png" alt="shopeepay">
                        </div>
                        <p>
                            ShopeePay
                        </p>
                    </div>
                    <div class="d-flex align-items-center" v-if="payment_type == 'linkAja'">
                        <div class="icons me-2">
                            <img src="/img/linkaja.png" alt="linkaja">
                        </div>
                        <p>
                            LinkAja
                        </p>
                    </div>
                </div>
                <div class="d-flex justify-content-between" v-if="!is_course_ready">
                    <strong><p>
                        Total
                    </p></strong>
                    <vue-skeleton-loader
                        :height="36"
                        :width="200"
                        color="rgba(52, 52, 52, 1)"
                        animation="fade"
                    ></vue-skeleton-loader>
                </div>
            </div>
            <div class="me-4">
                <a v-if="price == 0 || (is_on_discount == 1 && discount_price == 0)" class="btn w-100 fill-gold fw-semi mt-2" href="javascript:void(0)" @click=charge(payment_type)
                    :class="{ buttondisabled: !payment_type }"  >
                    Claim
                </a>
                <a v-else class="btn w-100 fill-gold fw-semi mt-2" href="javascript:void(0)" @click=charge(payment_type)
                    :class="{ buttondisabled: !payment_type }"  >
                    Bayar
                </a>
            </div>
        </div>
    </div>
</div>
@endsection
    @section('js')
    <script>
        'use strict';
        var app = new Vue({
            el: '#app',
            data() {
                return {
                    error_show: null,
                    customer: {
                        first_name: '',
                        last_name: '',
                        address: '',
                        city: '',
                        postal_code: '',
                        country_code: '',
                    },
                    token_id: null,
                    course: null,
                    is_course_ready: false,
                    phone_number: null,
                    price: '',
                    discount_price: '',
                    payment_type: '',
                }
            },
            mounted(){
                axios({
                    method: 'get',
                    url: this.api_url + '/api/v1/courses/{{Request::get('course')}}',
                    headers: {
                        'Authorization' : 'Bearer ' + bearer,
                        'Accept' : 'application/json',
                    }
                }).then(response => {
                    this.course = response.data
                    this.price = Number(this.course.price).toLocaleString('id');
                    this.discount_price = Number(this.course.discount_price).toLocaleString('id');
                    this.is_course_ready = true
                })
            },
            computed: {
                freeCourse: function () {
                    if(this.course.price == 0){
                        return true
                    }
                    else{
                        if(this.course.is_on_discount && this.course.discount_price == 0){
                            return true
                        }
                        if(this.course.is_on_discount && this.course.discount_price != 0){
                            return false
                        }

                        return false
                    }
                }
            },
            methods: {
                setPayment(payment_type){
                    this.payment_type = payment_type
                },
                charge(payment_type){
                    var self = this
                    this.showLoading()
                    axios({
                        method: 'post',
                        url: this.api_url + '/api/v1/charge',
                        headers: {
                            'Authorization' : 'Bearer ' + bearer,
                            'Accept' : 'application/json',
                        },
                        data: {
                            payment_type: payment_type,
                            course_id: this.course.id,
                            phone_number: this.phone_number,
                            token_id: this.token_id,
                            first_name: this.customer.first_name,
                            last_name: this.customer.last_name,
                            address: this.customer.address,
                            city: this.customer.city,
                            postal_code: this.customer.postal_code,
                            country_code: this.customer.country_code,
                        }
                    })
                    .then(response => {
                        var transaction = response.data
                        var self = this
                        this.hideLoading()
                        if(payment_type == 'gopay'){
                            //note : jika diakses dari HP, gopay akan redirect pakai deeplink (actions 1)
                            //kalau dari web, cukup display QR
                            
                            window.location.href = '/student/billing/detail?order=' + transaction.order_id
                        }

                        else if(payment_type == 'ovo'){
                            phoneNumberValidation()
                            document.getElementById('hiddenOVOModalButton').click()
        
                        }
                        else if(payment_type == 'credit_card'){
                            if(typeof transaction.redirect_url !== 'undefined' && transaction.transaction_status == 'pending'){ //berarti membutuhkan 3ds
                                window.location.href = '/payment/redirect?redirect=' + transaction.redirect_url
                            }
                            return redirect(transaction.actions[1].url);
                        }
                        else if(transaction.payment_type == 'bank_transfer' || transaction.payment_type == 'echannel'){
                            window.location.href = '/student/billing/detail?order=' + transaction.order_id
                        }
                        else if(payment_type == 'shopeePay'){
                            //note : jika diakses dari HP, shopeepay akan redirect pakai deeplink (mobile_deeplink_checkout_url)
                            //kalau dari web, cukup display QR (qr_checkout_string)
                            window.location.href = transaction.actions.mobile_deeplink_checkout_url
                        }
                        else if(payment_type == 'dreamguru'){
                            window.location.href = '/student/dashboard/courses'
                        }
                        else{
                            //berlaku untuk dana dan link aja
                            //note : jika diakses dari HP, akan redirect pakai mobile link (mobile_web_checkout_url)
                            //kalau dari desktop, pakai desktop link (desktop_web_checkout_url)
                            window.location.href = transaction.actions.desktop_web_checkout_url
                        }
                    })
                    .catch(function (error) {
                        console.log(error.response)
                        if(error.response.status == 403){
                            window.location.href = '/redirect-to-billing?course=' + self.course.title
                        }
                        else{
                            self.hideLoading()
                            self.error_show = error.response.data.errors
                        }
                    })
                },
                
                getToken(){
                    var self = this
                    var cardData = {
                        "card_number": document.getElementById('card_number').value,
                        "card_exp_month": document.getElementById('card_exp_month').value,
                        "card_exp_year": document.getElementById('card_exp_year').value,
                        "card_cvv": document.getElementById('card_cvv').value,
                    }
                    
                    var alertBox = document.getElementById('card_validation_status')
                    var options = {
                        onSuccess: function(response){
                        alertBox.classList.remove("d-none")
                        alertBox.classList.add("alert-success")
                        alertBox.innerHTML = 'Validasi Sukses, Mengalihkan ke halaman pembayaran...'

                        self.token_id = response.token_id

                        var submitButton = document.getElementById('cardSubmitButton');
                        setTimeout(function(){ submitButton.click()}, 2500);
                        },
                        onFailure: function(response){
                        alertBox.classList.remove("d-none")
                        alertBox.classList.add("alert-danger")
                        alertBox.innerHTML = 'Validasi Gagal, Coba Lagi'

                        }
                    };  
                    MidtransNew3ds.getCardToken(cardData, options);
                }
            }
        })
    </script>
    @endsection