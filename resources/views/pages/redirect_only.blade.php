@extends('layout.default')

@section('meta')
    <title>Redirecting...</title>
@endsection

@section('css')
    <style> 
        
    </style>
@endsection

@section('content')
@endsection

@section('js')
    <script>
        'use strict';
        var app = new Vue({
            el: '#app',

            mounted: function() {
                axios({
                    method: 'get',
                    url: this.api_url + '/api/v1/section-quizzes/{{$quiz_id}}',
                    headers: {
                        'Authorization' : 'Bearer ' + bearer,
                        'Accept' : 'application/json',
                    },

                })
                .then(response => {
                    var master_quiz_id = response.data.quiz_id
                    window.location.href = '/teacher/quiz/' + master_quiz_id + '/edit'
                })
                .catch(function (error) {
                    if (error.response) {
                    }
                })
            }
        })
    </script>

@endsection