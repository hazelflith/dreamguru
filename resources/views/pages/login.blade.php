@extends('layout.default')

@section('meta')
    @component('components.meta')
        @slot('title')
             Dreamguru | Login
        @endslot

        @slot('description')
            Dreamaxtion helps companies and people maximize potential through Human Capital management system and psychometric assessment for personal and career growth.
        @endslot
    @endcomponent
@endsection

@section('css')
    <style> 
        
    </style>
@endsection

@section('content')
<div class="container">
    <div class="row main-padding-v justify-content-center">
        <div class="col-md-6 col-xl-4 col-10">
            <div class="py-3 text-center" id="logo">
                <img src="/img/dreamguru.png" alt="logo" width="100%">
            </div>
            <h1 class="py-2 color-gold fw-semi text-center mb-4">
                Login
            </h1>
            @if(Session::has("errors"))
            <div class="alert alert-danger" role="alert">
                @foreach(Session::get('errors') as $message)
                    <p class="color-red">
                        {{ucfirst($message[0])}}
                    </p>
              @endforeach
            </div>
            @endif
            @if(Session::has("message"))
            <div class="alert alert-danger" role="alert">
                <p class="color-red">
                    {{ucfirst(Session::get('message'))}}
                </p>
            </div>
            @endif
            <form action="" method="post">
            @csrf
                <!-- Email input -->
                <div class="form-outline mb-4">
                    <label class="form-label" for="form2Example1">
                        Email address / Username
                    </label>
                    <input type="text" name="username" id="form2Example1" class="form-control" />
                </div>
              
                <!-- Password input -->
                <div class="form-outline mb-4">
                    <label class="form-label" for="form2Example2">
                      Password
                    </label>
                    <div class="input-group">
                        <input type="password" id="password" name="password" class="form-control" />
                        <span class="input-group-text-blend input-group-text" id="basic-addon1">
                            <i class="fa-solid fa-eye color-white" id="togglePassword" style="cursor: pointer; width: 24px"></i>
                        </span>
                    </div>
                </div>
              
                <!-- 2 column grid layout for inline styling -->
                <div class="row mb-4">
                  <div class="col">
                    <!-- Simple link -->
                    <a href="/forgot-password" class="color-gold">
                      Forgot password?
                    </a>
                  </div>
                </div>
              
                <!-- Submit button -->
                <div class="row px-3">
                    <button type="submit" class="btn  fill-gold fw-semi btn-block mb-4 ">
                      Log in
                    </button>
                </div>
            </form>
                <!-- Register buttons -->
                <div class="text-center">
                    <p>Not a member? 
                        <a href="/register" class="color-gold">
                            Register
                        </a>
                    </p>
                  {{-- <p>
                    or sign in with:
                  </p>
                  <button type="button" class="btn btn-link color-gold btn-floating mx-1">
                    <i class="fab fa-facebook-f"></i>
                  </button>
              
                  <button type="button" class="btn btn-link color-gold btn-floating mx-1">
                    <i class="fab fa-google"></i>
                  </button>
              
                  <button type="button" class="btn btn-link color-gold btn-floating mx-1">
                    <i class="fab fa-twitter"></i>
                  </button>
              
                  <button type="button" class="btn btn-link color-gold btn-floating mx-1">
                    <i class="fab fa-github"></i>
                  </button> --}}
                </div>
            </div>
        </div>
    </div>
    
@endsection

@section('js')
    <script>
        'use strict';
        var app = new Vue({
            el: '#app',
        })
    </script>
    <script>
        const togglePassword = document.querySelector("#togglePassword");
        const password = document.querySelector("#password");

        togglePassword.addEventListener("click", function () {
            
            // toggle the type attribute
            const type = password.getAttribute("type") === "password" ? "text" : "password";
            password.setAttribute("type", type);

            // toggle the eye icon
            this.classList.toggle('fa-eye-slash');
        });
    </script>
@endsection