@extends('layout.default')

@section('meta')
    @component('components.meta')
        @slot('title')
             Dreamguru | Student Dashboard
        @endslot

        @slot('description')
            Dreamaxtion helps companies and people maximize potential through Human Capital management system and psychometric assessment for personal and career growth.
        @endslot
    @endcomponent
@endsection

@section('css')
    <style> 
        
    </style>
@endsection

@section('content')
<div class="main-padding-h main-padding-v" v-cloak>
    <h3 class="color-gold fw-semi">
        Student Dashboard
    </h3> 
    <h6 class="color-muted mb-5">
        BROWSE YOUR COURSES
    </h6>
    <div v-if="page_ready">
        <div v-if="in_progress_courses.length > 0 || in_progress_quizzes.length > 0">
            <h5 class="color-gold fw-semi mt-3">
                Continue Your Journey
            </h5> 
            <p class="color-muted mb-3">
                Here is what’s been leftoff
            </p>
            <div v-if="in_progress_quizzes.length > 0" class="mb-3">
                <div v-if="in_progress_quizzes.length > 0">
                    <div class="d-flex align-items-center">
                        <h6 class="color-gold me-3 mb-0">
                            QUIZZES
                        </h6>
                        <div class="hr"></div>
                    </div>
                    <div class="slider pt-4 pb-5" v-if="in_progress_quizzes_ready">
                        <div class="mt-2 me-2" style="width: 525px" v-for="quiz in in_progress_quizzes">
                            <cards-long :quiz="quiz"></cards-long>
                        </div>
                    </div>
                </div>
            </div>
            <div v-if="in_progress_courses.length > 0">
                <div class="d-flex align-items-center">
                    <h6 class="color-gold me-3 mb-0">
                        COURSES
                    </h6>
                    <div class="hr"></div>
                </div>
                <div class="row pt-4 pb-5 g-3"
                    v-if="in_progress_courses_ready"
                >
                    <div class="col-lg-6 col-xl-3 col-xxl-3" v-for="course in in_progress_courses">
                        <cards-cont :course="course.course"></cards-cont>
                    </div>
                </div>
            </div>
        </div>
        <div v-if="completed_courses.length > 0">
            <h5 class="color-gold fw-semi mt-3">
                History
            </h5> 
            <p class="color-muted mb-3">
                Here is what’s you completed
            </p>
            
            <div v-if="completed_courses.length > 0">
                <div class="d-flex align-items-center">
                    <h6 class="color-gold me-3 mb-0">
                        COURSES
                    </h6>
                    <div class="hr"></div>
                </div>
                <div class="row pt-4 pb-5 g-3"
                    v-if="completed_courses_ready"
                >
                    <div class="col-lg-6 col-xl-3 col-xxl-3" v-for="course in completed_courses">
                        <cards :course="course.course" :dashboard="true"></cards>
                    </div>
                </div>
            </div>
        </div>
        <div v-if="completed_courses.length == 0 && in_progress_courses == 0">
            <h3 class="fw-semi color-gold">
                Uh-oh there is no course or quiz you've been taken
            </h3>
            <p class="mb-3 color-muted">
                Here are some recommendation for you
            </p>
            <div class="row pb-5 gx-3 gy-3">
                <div class="col-12 col-lg-6 col-xl-3" v-if="course.status == 'completed'" v-for="course in courses">
                    <cards class="" :course="course" :dashboard="true"></cards>
                </div>
            </div>
        </div>
    </div>
    <div v-if="!page_ready">
        <vue-skeleton-loader
            :height="300"
            class="w-100 mb-4"
            color="rgba(52, 52, 52, 1)"
            animation="fade"
        ></vue-skeleton-loader>
        <vue-skeleton-loader
            :height="300"
            class="w-100 mb-4"
            color="rgba(52, 52, 52, 1)"
            animation="fade"
        ></vue-skeleton-loader>
    </div>
</div>
@endsection

@section('js')
    <script>
        'use strict';
        var app = new Vue({
            el: '#app',
            data(){
                return {
                    in_progress_courses : null,
                    in_progress_courses_ready : false,
                    completed_courses : null,
                    completed_courses_ready : false,
                    in_progress_quizzes : null,
                    in_progress_quizzes_ready : false,
                    courses: null,
                    page_ready : false,
                }
            },   
            mounted: function(){
                axios.get(this.api_url + '/api/v1/courses')
                .then(response => {
                    this.courses = response.data
                }),
                axios({
                    method: 'get',
                    url: this.api_url + '/api/v1/students/{{session('data')['id']}}/courses',
                    headers: {
                        'Authorization' : 'Bearer ' + bearer,
                        'Accept' : 'application/json',
                    }
                })
                .then(response => {
                    this.in_progress_courses = response.data
                    this.in_progress_courses_ready = true
                    if(this.in_progress_courses_ready  && this.in_progress_quizzes_ready && this.completed_courses_ready){
                        this.page_ready = true
                    }

                })
                .catch(function (error) {
                    if (error.response) {
                    }
                }),
                axios({
                    method: 'get',
                    url: this.api_url + '/api/v1/students/{{session('data')['id']}}/quizzes',
                    headers: {
                        'Authorization' : 'Bearer ' + bearer,
                        'Accept' : 'application/json',
                    }
                })
                .then(response => {
                    this.in_progress_quizzes = response.data
                    this.in_progress_quizzes_ready = true
                    if(this.in_progress_courses_ready  && this.in_progress_quizzes_ready && this.completed_courses_ready){
                        this.page_ready = true
                    }
                    
                })
                .catch(function (error) {
                    if (error.response) {
                        //
                    }
                }),
                axios({
                    method: 'get',
                    url: this.api_url + '/api/v1/students/{{session('data')['id']}}/courses/completed',
                    headers: {
                        'Authorization' : 'Bearer ' + bearer,
                        'Accept' : 'application/json',
                    }
                })
                .then(response => {
                    this.completed_courses = response.data
                    this.completed_courses_ready = true
                    if(this.in_progress_courses_ready  && this.in_progress_quizzes_ready && this.completed_courses_ready){
                        this.page_ready = true
                    }
                })
                .catch(function (error) {
                    if (error.response) {
                        //
                    }   
                })

                

            },
            methods: {
                paginationCallback(page) {
                    console.log(`Page ${page} was selected. Do something about it`);
                }
            },  
        })
    </script>
@endsection