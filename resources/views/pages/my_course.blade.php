@extends('layout.default')

@section('meta')
    @component('components.meta')
        @slot('title')
             Dreamguru | My Courses
        @endslot

        @slot('description')
            Dreamaxtion helps companies and people maximize potential through Human Capital management system and psychometric assessment for personal and career growth.
        @endslot
    @endcomponent
@endsection

@section('css')
    <style> 
        
    </style>
@endsection

@section('content')
<div class="main-padding-h main-padding-v">
    <h3 class="color-gold fw-semi">
        My Courses
    </h3> 
    <h6 class="color-muted mb-3">
        BROWSE & EDIT YOUR COURSES
    </h6>

    <div class="dropdown">
          {{-- <button class="btn btn-secondary dropdown-toggle fill-gray me-2" type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
            Sort By <i class="fa-solid fa-arrow-down-wide-short ms-1"></i>
          </button>
          <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownMenuButton2">
            <li><a class="dropdown-item active" href="#">Action</a></li>
            <li><a class="dropdown-item" href="#">Another action</a></li>
            <li><a class="dropdown-item" href="#">Something else here</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="#">Separated link</a></li>
          </ul> --}}
          <a href="/teacher/course/add" class="btn outline-gold fw-semi">
            Add Course
        </a>
    </div>
    <div class="d-flex align-items-center mt-3">
        <h6 class="color-gold me-3 mb-0">
            COMPLETED
        </h6>
        <div class="hr"></div>
    </div>
        <div class="row pt-4 pb-5 g-3">
            <div class="col-lg-6" v-for="course in courses_completed">
                <cards-edit :session_id="session_id" :course="course"></cards-edit>
            </div>
                <div class="col-lg-6" v-if="!is_data_ready">
                   <cards-edit-skeleton></cards-edit-skeleton>
                </div>
                <div class="col-lg-6" v-if="!is_data_ready">
                    <cards-edit-skeleton></cards-edit-skeleton>
                </div>
        </div>
    <div class="d-flex align-items-center mt-3">
        <h6 class="color-gold me-3 mb-0">
            DRAFT
        </h6>
        <div class="hr"></div>
    </div>
    <div class="row pt-4 pb-5 g-3">
        <div class="col-lg-6" v-for="course in courses_draft">
            <cards-edit :session_id="session_id" :course="course"></cards-edit>
        </div>
        <div class="col-lg-6" v-if="!is_data_ready">
            <cards-edit-skeleton></cards-edit-skeleton>
        </div>
        <div class="col-lg-6" v-if="!is_data_ready">
            <cards-edit-skeleton></cards-edit-skeleton>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script>
        'use strict';
        var app = new Vue({
            el: '#app',

        data() {
            return {
                courses_completed: null,
                courses_draft: null,
                is_data_ready: false,
                session_id: "{{session('data')['id']}}" ,
            }
        },
        mounted: function() {
            axios({
                method: 'get',
                url: this.api_url + '/api/v1/teachers/{{session('data')['id']}}/courses',
                headers: {
                    'Authorization': 'Bearer ' + bearer,
                    'Accept': 'application/json',
                }
            })
            .then(response => {
                this.courses_completed = response.data
                for(var i = 0; i < this.courses_completed.length; i++){
                    this.courses_completed[i].price = Number(this.courses_completed[i].price).toLocaleString('id');
                    this.courses_completed[i].discount_price = Number(this.courses_completed[i].discount_price).toLocaleString('id');
                }
                
            }),
            
            axios({
                method: 'get',
                url: this.api_url + '/api/v1/teachers/{{session('data')['id']}}/courses/draft',
                headers: {
                    'Authorization': 'Bearer ' + bearer,
                    'Accept': 'application/json',
                }
            })
            .then(response => {
                this.courses_draft = response.data
                for(var i = 0; i < this.courses_draft.length; i++){
                    this.courses_draft[i].price = Number(this.courses_draft[i].price).toLocaleString('id');
                    this.courses_draft[i].discount_price = Number(this.courses_draft[i].discount_price).toLocaleString('id');
                }
                this.is_data_ready = true
            })
        },
    });
    </script>
    
@endsection