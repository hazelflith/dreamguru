@extends('layout.default')

@section('meta')
    @component('components.meta')
        @slot('title')
             Dreamguru | Courses
        @endslot

        @slot('description')
            Dreamaxtion helps companies and people maximize potential through Human Capital management system and psychometric assessment for personal and career growth.
        @endslot
    @endcomponent
@endsection

@section('css')
    <style> 
        
    </style>
@endsection

@section('content')
<div class="main-padding-h main-padding-v">
    <h3 class="color-gold fw-semi">
        Course List
    </h3> 
    <h6 class="color-muted mb-3">
        BROWSE COURSES
    </h6>

    {{-- <div class="dropdown">
        <button class="btn btn-secondary dropdown-toggle fill-gray me-2" type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
          Filter 1
        </button>
        <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownMenuButton2">
          <li><a class="dropdown-item active" href="#">Action</a></li>
          <li><a class="dropdown-item" href="#">Another action</a></li>
          <li><a class="dropdown-item" href="#">Something else here</a></li>
          <li><hr class="dropdown-divider"></li>
          <li><a class="dropdown-item" href="#">Separated link</a></li>
        </ul>

        <button class="btn btn-secondary dropdown-toggle fill-gray me-2" type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
            Filter 2
          </button>
          <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownMenuButton2">
            <li><a class="dropdown-item active" href="#">Action</a></li>
            <li><a class="dropdown-item" href="#">Another action</a></li>
            <li><a class="dropdown-item" href="#">Something else here</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="#">Separated link</a></li>
          </ul>

          <button class="btn btn-secondary dropdown-toggle fill-gray me-2" type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
            Filter 3
          </button>
          <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownMenuButton2">
            <li><a class="dropdown-item active" href="#">Action</a></li>
            <li><a class="dropdown-item" href="#">Another action</a></li>
            <li><a class="dropdown-item" href="#">Something else here</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="#">Separated link</a></li>
          </ul>

          <button class="btn btn-secondary dropdown-toggle fill-gray me-2" type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
            Sort By <i class="fa-solid fa-arrow-down-wide-short ms-1"></i>
          </button>
          <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownMenuButton2">
            <li><a class="dropdown-item active" href="#">Action</a></li>
            <li><a class="dropdown-item" href="#">Another action</a></li>
            <li><a class="dropdown-item" href="#">Something else here</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="#">Separated link</a></li>
          </ul>
    </div> --}}
    <div class="row g-3" v-if="!is_course_ready">
        <div class="col-lg-6 col-xl-3 col-xxl-3">
            <cards-skeleton></cards-skeleton>
        </div>
        <div class="col-lg-6 col-xl-3 col-xxl-3">
            <cards-skeleton></cards-skeleton>
        </div>
        <div class="col-lg-6 col-xl-3 col-xxl-3">
            <cards-skeleton></cards-skeleton>
        </div>
        <div class="col-lg-6 col-xl-3 col-xxl-3">
            <cards-skeleton></cards-skeleton>
        </div>
    </div>
    <div v-for="(category , index) in categories" v-cloak>
        <div v-if="category.available_courses.length > 0" class="d-flex align-items-center pt-4">
            <h6 class="color-gold me-3 mb-0" style="white-space: nowrap">
                @{{ category.name }}
            </h6>
            {{-- <a class="btn  btn-rounded btn-sm outline-gold me-3" style="white-space: nowrap" role="button">View More</a> --}}
            <div class="hr"></div>
        </div>
        <div class="row pt-4 pb-5 g-3" v-if="category.available_courses.length > 0">
            <div class="col-lg-6 col-xl-3 col-xxl-3" v-if="course.status == 'completed' && course.category_id == category.id" v-for="course in courses">
                <cards :course="course" :session_id="session_id"></cards>
            </div>
        </div>
    </div>
    {{-- <div class="d-flex align-items-center">
        <h6 class="color-gold me-3 mb-0">CODING</h6>
        <a class="btn  btn-rounded btn-sm outline-gold me-3" style="white-space: nowrap" role="button">View More</a>
        <div class="hr"></div>
    </div>
    <div class="row pt-4 pb-5">
        <div class="col-12 col-lg-6 col-xl-3" v-for="course in courses">
            <cards v-if="course.status == 'completed'" class="mt-3" :course="course"></cards>
        </div>
        
    </div> --}}
</div>
@endsection

@section('js')
    <script>
        'use strict';
        var app = new Vue({
            el: '#app',

        data() {
            return {
                course_hightlight: null,
                courses: null,
                session_id: "{{session('data')['id']}}" ,
                is_course_ready: false,
                categories: null,
            }
        },
        mounted: function() {
            
            axios.get(this.api_url + '/api/v1/courses')
            .then(response => {
                this.course_highlight = response.data[0]
                this.courses = response.data
                
            })
            axios({
                    method: 'get',
                    url: this.api_url + '/api/v1/categories',
                    headers: {
                        'Authorization' : 'Bearer ' + bearer,
                        'Accept' : 'application/json',
                    },
                })
            .then(response => {
                this.categories = response.data
                this.is_course_ready = true
            })
            .catch(function (error) {
                if (error.response) {
                    //
                }
            })
        },
    });     
    </script>
@endsection