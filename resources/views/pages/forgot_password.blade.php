@extends('layout.default')

@section('meta')
    @component('components.meta')
        @slot('title')
             Dreamguru | Forgot Password
        @endslot

        @slot('description')
            Dreamaxtion helps companies and people maximize potential through Human Capital management system and psychometric assessment for personal and career growth.
        @endslot
    @endcomponent
@endsection

@section('css')

@endsection

@section('content')
<div class="container" v-cloak>
    <div class="row main-padding-v justify-content-center">
        <div class="col-md-6 col-xl-4 col-10">
            <div class="py-3 text-center" id="logo">
                <img src="/img/dreamguru.png" alt="logo" width="100%">
            </div>
            <h1 class="py-2 color-gold fw-semi text-center mb-4">
                Forgot Password
            </h1>
            <div v-if="error_show" class="alert alert-danger" role="alert">
                <div v-for="value in error_show">
                    <p class="color-red">
                        @{{ value[0] }}
                    </p>
                </div>                
            </div>
            <div v-if="confirmation_show" class="alert alert-success" role="alert">
                <div>
                    <p class="color-green">
                        @{{ confirmation_show }}
                    </p>
                </div>                
            </div>
                <!-- Email input -->
                <div class="form-outline mb-4">
                    <label class="form-label" for="form2Example1">
                        Email Address
                    </label>
                    <input type="email" v-model="email" class="form-control" />
                </div>
              
                <!-- 2 column grid layout for inline styling -->
                
                <!-- Submit button -->
                <div class="row px-3">
                    <a class="btn fill-gold fw-semi btn-block mb-4" @click="sendEmail()" :class="{ buttondisabled: process }">
                      Reset Password 
                      <span class="ms-2 color-black" v-if="process" id="timer"></span>
                    </a>
                </div>
                <p v-if="text_help"> @{{ text_help }}</p>
                <div class=" d-flex justify-content-center mt-4">
                    <a class="color-gold" href="/login">
                      Back to login
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        'use strict';
        var app = new Vue({
            el: '#app',

            data() {
                return {
                    text_help:'',
                    confirmation_show:'',
                    email:'',
                    error_show: null,
                    ready: false,
                    process: false,
                }
            },
            methods: {
                sendEmail(){
                    this.text_help = "Processing.. Please Wait."
                    this.confirmation_show = null
                    this.error_show= null
                    this.process = true
                    var self = this
                    axios({
                        method: 'post',
                        url: this.api_url + '/api/v1/auth/reset-password',
                        headers:{
                            'Accept' : 'application/json',
                        },
                        data: {
                            email: this.email
                        }
                    })
                    .then(response => {
                        this.confirmation_show = "Email confirmation has been sent!"
                        this.text_help = null
                        var maxTicks = 120
                        var tickCount = 0
                        var distance = maxTicks - tickCount
                        var minutes = Math.floor(distance / 60)
                        var seconds = Math.floor(distance - (minutes * 60))
                        var tick = function(){
                            if (tickCount >= maxTicks){
                                // Stops the interval.
                                clearInterval(self.myInterval);
                                document.getElementById("timer").innerHTML = "00:00"
                                self.process = false
                                return
                            }
                        
                            /* The particular code you want to excute on each tick */
                            if(seconds == -1){
                                minutes -= 1
                                seconds = 59
                            }
                            if(minutes < 10 && seconds < 10){
                                document.getElementById("timer").innerHTML = "0" + minutes + ":" + "0" + seconds;   
                            }
                            else if(minutes < 10){
                                document.getElementById("timer").innerHTML = "0" + minutes + ":" + seconds;   
                            }
                            else if(seconds < 10){
                                document.getElementById("timer").innerHTML = minutes + ":" + "0" + seconds;   
                            }
                            else{
                                document.getElementById("timer").innerHTML = minutes + ":" + seconds;                         
                            }
                            tickCount++;
                            seconds -= 1
                            
                        };
                        
                        // Start calling tick function every 1 second.
                        this.myInterval = setInterval(tick, 1000);
                    })
                    .catch(function (error) {
                        if (error.response) {
                            self.text_help = null
                            self.error_show= error.response.data.errors
                            window.location.href = '#'
                            self.process = false
                        }
                    })
                },
            }
        })
    </script>
@endsection