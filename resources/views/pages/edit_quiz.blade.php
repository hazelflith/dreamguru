@extends('layout.default')

@section('meta')
    @component('components.meta')
        @slot('title')
             Edit Quiz
        @endslot

        @slot('description')
            Dreamaxtion helps companies and people maximize potential through Human Capital management system and psychometric assessment for personal and career growth.
        @endslot
    @endcomponent
@endsection

@section('css')
    <style> 
        
    </style>
@endsection

@section('content')
<div class="main-padding-h main-padding-v" v-cloak>
    <h3 class="color-gold fw-semi">
        Edit Quiz
    </h3>
    <h6 class="color-muted" v-if="is_quiz_ready">
        @{{req.title}}> EDIT
    </h6>
    <vue-skeleton-loader
        :height="30"
        class="w-100"
        v-if="!is_quiz_ready"
        color="rgba(52, 52, 52, 1)"
        animation="fade"
    ></vue-skeleton-loader>
</div> 
<div class="sticky-container" v-if="is_quiz_ready" v-cloak>
    <p class="pt-3 ps-3">
        Shortcuts :
    </p>
    <div class="d-flex p-3">
        <div v-if="is_quiz_ready" class="me-3">
            <a class="btn fill-gold fw-semi"
                @click="updateQuiz('completed')"
            >
                SAVE CHANGES
            </a>
        </div>
        <a href="javascript:void(0)" class="btn outline-gold me-3" @click="updateQuiz('draft')">
            Save Draft
        </a>
        <button class="btn color-red" data-bs-toggle="modal" data-bs-target="#deleteQuizModal">
            <i class="fa-solid fa-trash-can"></i>
        </button>
    </div>
</div>
<div class="row g-0" v-cloak>
    <div class="col-8">
        <div class="main-padding-h">
            <div v-if="error_show" class="alert alert-danger" role="alert">
                <div v-for="value in error_show">
                    <p class="color-red">
                        @{{value[0]}}
                    </p>
                </div>                
            </div>
            <div v-if="error_show2" class="alert alert-danger" role="alert">
                <div>
                    <p class="color-red"> 
                        @{{ error_show2 }}
                    </p>
                </div>                
            </div>
            <div class="d-flex align-items-center mb-3">
                <h4 class="color-gold fw-semi me-auto" v-if="is_quiz_ready">
                    @{{ req.title }}
                </h3>
                <vue-skeleton-loader
                    :height="36"
                    :width="200"
                    v-if="!is_quiz_ready"
                    color="rgba(52, 52, 52, 1)"
                    animation="fade"
                ></vue-skeleton-loader>
                <h6 class="color-muted text-capitalize" v-if="is_quiz_ready">
                    Status: @{{ req.status }}
                </h6>
                <vue-skeleton-loader
                    :height="36"
                    :width="200"
                    class="ms-auto"
                    v-if="!is_quiz_ready"
                    color="rgba(52, 52, 52, 1)"
                    animation="fade"
                ></vue-skeleton-loader>
            </div>
            <div class="d-flex align-items-center">
                <h6 class="color-gold me-3 mb-0" style="white-space: nowrap">
                    QUESTIONS
                </h6>
                <div class="hr"></div>
            </div>
            <div class="mb-3"></div>
            <div class="question-wrap" v-if="is_quiz_ready">
                <div v-for="(question,index) in quiz.questions" :key="question.id">
                    <div class="d-flex">
                        <h5 class="mb-1 color-gold">
                            Question @{{ index + 1 }}
                        </h5>
                        <h5 class="color-gold ms-auto me-3">
                            Point: @{{ question.point }}
                        </h5>
                        <a href="javascript:void(0)" data-bs-toggle="modal" :data-bs-target="'#deleteQuestionModal' + index" class="color-red">
                            <i class="fa-solid fa-trash-can"></i>
                        </a>
                        {{-- MODAL--}}
                        <div class="modal fade" :id="'deleteQuestionModal'+ index" tabindex="-1" aria-labelledby="deleteQuestionModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="deleteQuestionModalLabel">
                                            Delete Question Confirmation
                                        </h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        <p class="mb-3">
                                            Are you sure you want to delete this question?
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary outline-gold me-2" data-bs-dismiss="modal">
                                            Cancel
                                        </button>
                                        <a class="btn fill-gold fw-semi" @click="deleteQuestion(question.id)">
                                            Delete Question
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p class="mb-1">
                        @{{ question.question }}
                    </p>
                    <p v-if="question.question_type == 'single_answer'" class="mb-1">
                        ANSWERS (Single Answer) :
                    </p>
                    <p v-else class="mb-1">
                        ANSWERS (Multiple Answer) :
                    </p>
                    <ul>
                        <li v-for="answer in question.answers" :key="answer.id">
                            @{{ answer.answer }} 
                            <i v-if="answer.is_true" class="fa-solid fa-circle-check ms-2"></i>
                        </li>
                    </ul>
                    <div class="hr mb-3"></div>
                </div>
                <div v-if="req.questions.length == 0">
                    <p>No questions added, Start adding question below</p>
                </div>
            </div>
            <vue-skeleton-loader
                :height="300"
                class="w-100"
                v-if="!is_quiz_ready"
                color="rgba(52, 52, 52, 1)"
                animation="fade"
            ></vue-skeleton-loader>
            <div class="mb-3"></div>
            <div class="d-flex align-items-center">
                <h6 class="color-gold me-3 mb-0" style="white-space: nowrap">
                    NEW QUESTIONS
                </h6>
                <div class="hr"></div>
            </div>
            <div class="mb-3"></div>
            <div class="question-wrap">
                <h5 class="mb-3 color-gold">
                    Question
                </h5>
                <textarea class="form-control form-dark" v-model="question.question">
                    Hello World!
                </textarea>
                <div id="wysiwygHelp" class="form-text mb-2">
                    Describe the question
                </div>
                <div class="mb-2"></div>
                <label for="QuizCourse" class="form-label">
                    Question Type
                </label>
                <select class="form-select form-dark" v-model="question.question_type" id="QuizCourse" aria-label="QuizCourse">
                    <option value="single_answer" selected>Single Answer</option>
                    <option value="multiple_answer">Multiple Answer</option>
                </select>
                <div id="QuizCourseHelp" class="form-text mb-2">
                    Select the type of question
                </div>
                <div class="mb-2"></div>
                <label for="Answers" class="form-label">
                    Answers
                </label>
                <div class="control-group">
                    <div class="controls" v-for="(answer, index) in question.answers">
                        <div class="mb-3">
                            <input type="text" class="form-control form-dark mb-1" v-model="answer.answer">
                            <div class="d-flex justify-content-between">
                                <div v-if="question.question_type == 'multiple_answer'">
                                    <input :id="'isTrueCheckbox' + index" type="checkbox" v-model="answer.is_true">
                                    <label :for="'isTrueCheckbox' + index">
                                        Is True
                                    </label>
                                </div>
                                <div v-else>
                                    <input :id="'isTrueRadio'+ index" @change="setChoice()"
                                        name="isTrueRadio" :value="index" type="radio" v-model="choice"
                                    >
                                    <label :for="'isTrueRadio' + index">
                                        Is True
                                    </label>
                                </div>
                                <a href="javascript:void(0)" class="color-red fw-semi" v-if="question.answers.length > 2" @click="deleteAnswer(index)">
                                    Delete Answer
                                </a>
                            </div>
                        </div>
                    </div>
                   <a v-if="question.answers.length < 6" href="javascript:void(0)" class="fw-semi btn outline-white" @click="addAnswer()">
                       Add another answer
                    </a>
                 </div>
                <div id="tagHelp" class="form-text mb-2">
                    Add choice of answers here
                </div>
                <div class="mb-2"></div>
                <label for="QuestionPoint" class="form-label">
                    Completion Point
                </label>
                <input type="number" class="form-control form-dark" id="QuestionPoint" placeholder="Question Point" v-model="question.point">
            </div>
            <a class="btn btn-secondary d-inline-block fill-gold fw-semi mb-5 mt-4" role="button" @click="addQuestion()">
                Add Question
            </a>
        </div>
    </div>
    <div class="col-4">
        <div class="form-container me-5 mb-4">
            <div class="text-center"> 
                <a class="btn d-inline-block fill-gold fw-semi my-4" href="javascript:void(0)" @click="updateQuiz('completed')">
                    SAVE CHANGES
                </a>
            </div>
            <div class="hr-dark"></div>
            <div class="py-3 px-3">
                <a href="javascript:void(0)" @click="updateQuiz('draft')">
                    Save Draft
                </a>
            </div>
            <div class="hr-dark"></div>
            <div class="py-3 px-3">
                <a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#deleteQuizModal" class="color-red fw-semi">
                    Delete Master Quiz
                </a>
            </div>
            {{-- MODAL--}}
            <div class="modal fade" id="deleteQuizModal" tabindex="-1" aria-labelledby="deleteQuizModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="deleteQuizModalLabel">
                                Delete Master Quiz Confirmation
                            </h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <p class="mb-3">
                                Once you delete a master quiz, you cannot view or edit it's content.
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary outline-gold me-2" data-bs-dismiss="modal">
                                Cancel
                            </button>
                            <a class="btn fill-gold fw-semi" @click="updateQuiz('archived')">
                                Delete Master Quiz
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="d-flex align-items-center mb-4">
            <h6 class="color-gold me-3 mb-0" style="white-space: nowrap">
                OPTIONS
            </h6>
            <div class="hr me-5"></div>
        </div>
        <div class="form-container me-5 mb-4">
            <div class="py-3 px-3">
                <label for="QuizMasterTime" class="form-label">
                    Time Limit:
                </label>
                <div class="d-flex w-50 mb-2">
                    <input placeholder="Minute" type="number" class="form-control form-dark me-2" 
                        v-model="req.time_limit_MM" id="EstimatedTimeMM" required
                    >
                    <input placeholder="Second" type="number" class="form-control form-dark" 
                        v-model="req.time_limit_SS" id="EstimatedTimeSS" required
                    >
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
    <script>
        'use strict';
        var app = new Vue({
            el: '#app',

            data() {
                return {
                    error_show: null,
                    error_show2: null,
                    choice:'',
                    is_quiz_ready: false,
                    req: {
                        title:'',
                        time_limit:'',
                        time_limit_MM: '',
                        time_limit_SS: '',
                        status:'',
                    },
                    question:{
                        question:'',
                        question_type:'',
                        point:'',
                        answers:[
                            {
                                answer: '',
                                is_true: false,
                            },
                            {
                                answer: '',
                                is_true: false,
                            },
                        ],
                        id:''
                    },
                    questions: null,

                }
            },
            mounted: function() {
                axios({
                    method: 'get',
                    url: this.api_url + '/api/v1/quizzes/{{$quiz_id}}',
                    headers: {
                        'Authorization' : 'Bearer ' + bearer,
                        'Accept' : 'application/json',
                    },

                })
                .then(response => {
                
                    this.quiz = response.data
                    this.req = response.data
                    this.is_quiz_ready = true
                    this.req.time_limit_MM = Math.floor(this.req.time_limit /60)
                    this.req.time_limit_SS = Math.floor(this.req.time_limit - (this.req.time_limit_MM * 60))

                })
                .catch(function (error) {
                    if (error.response) {
                    }
                }),

                setTimeout(() => {
                        tinymce.init({
                            selector: '#wysiwyg',
                            menubar : 'edit format insert',
                            height : "480",
                            skin: 'oxide-dark',
                            content_css: 'dark',
                            plugins: 'paste lists nonbreaking',
                            paste_as_text: true,
                            toolbar: 'numlist bullist',
                            nonbreaking_force_tab: true
                        });
                        // The DOM element you wish to replace with Tagify
                        var input = document.querySelector('input[name=tags]');
                        
                        // initialize Tagify on the above input node reference
                        new Tagify(input)
                }, 1000);
            },
            methods: {
                updateQuiz(status){
                    var self = this
                    this.showLoading()
                    this.req.time_limit_MM = this.req.time_limit_MM * 60
                    this.req.time_limit = parseInt(this.req.time_limit_SS) + parseInt(this.req.time_limit_MM)
                    if(this.req.questions.length > 0){
                        axios({
                            method: 'put',
                            url: this.api_url + '/api/v1/quizzes/'+ {{ $quiz_id }},
                            headers: {
                                'Authorization' : 'Bearer ' + bearer,
                                'Accept' : 'application/json',
                            },
                            data: {
                                status: status
                            }
                        })
                        .then(response => {
                            if(status == 'draft'){
                                window.location.href = "{{url()->current()}}"
                                self.hideLoading()
                            }
                            else{
                                self.hideLoading()
                                window.location.href = "/teacher/quiz"
                            }
                        })
                    }
                    else{
                        this.error_show2 = "You have to set at least 1 question to publish the quiz"
                        window.location.href = '#'
                        self.hideLoading()
                    }
                },
                editMasterQuiz(){
                    this.showLoading()
                    var self = this
                    axios({
                        method: 'put',
                        url: this.api_url + '/api/v1/quizzes/{{$quiz_id}}',
                        headers: {
                            'Authorization' : 'Bearer ' + bearer,
                            'Accept' : 'application/json',
                        },
                        data: {
                            title: this.req.title,
                            time_limit: this.time_limit,
                        }

                    })
                    .then(response => {
                        window.location.href = "{{url()->current()}}"
                    })
                    .catch(function (error) {
                        if (error.response) {
                            this.hideLoading()
                        }
                    }),
                    axios({
                        method: 'put',
                        url: this.api_url + '/api/v1/quizzes/{{$quiz_id}}/questions',
                        headers: {
                            'Authorization' : 'Bearer ' + bearer,
                            'Accept' : 'application/json',
                        },
                        data: {
                            title: this.req.title,
                            time_limit: this.time_limit,
                        }

                    })
                    .then(response => {
                        this.hideLoading()
                        window.location.href = "{{url()->current()}}"

                    })
                    .catch(function (error) {
                        if (error.response) {
                            self.hideLoading();
                        }
                    })
                },
                addQuestion(){
                    this.showLoading()
                    var self = this
                    axios({
                        method: 'post',
                        url: this.api_url + '/api/v1/quizzes/{{$quiz_id}}/questions',
                        headers: {
                            'Authorization' : 'Bearer ' + bearer,
                            'Accept' : 'application/json',
                        },
                        data: {
                            question: this.question.question,
                            question_type: this.question.question_type,
                            point: this.question.point,
                            answers: this.question.answers
                        }

                    })
                    .then(response => {
                        this.hideLoading()
                        window.location.href = "{{url()->current()}}"
                    })
                    .catch(function (error) {
                        if (error.response) {
                            self.error_show = error.response.data.errors
                            window.location.href = '#'
                            self.hideLoading()
                        }
                    })
                },
                deleteQuestion(question_id){
                    this.showLoading()
                    var self = this
                    axios({
                        method: 'delete',
                        url: this.api_url + '/api/v1/quizzes/{{$quiz_id}}/questions/' + question_id,
                        headers: {
                            'Authorization' : 'Bearer ' + bearer,
                            'Accept' : 'application/json',
                        },
                    })
                    .then(response => {
                        this.hideLoading()
                        window.location.href = "{{url()->current()}}"
                    })
                    .catch(function (error) {
                        if (error.response) {
                            self.hideLoading()
                        }
                    })
                },
                addAnswer() {
                    this.question.answers.push(
                        {
                            answer: '',
                            is_true: false,
                        }
                    )
                },
                deleteAnswer(index) {
                    this.question.answers.splice(index, 1)
                },
                setChoice(){
                    _.forEach(this.question.answers, (answer, index)=>{
                        if(this.choice == index) {
                            answer.is_true = true
                        }else{
                            answer.is_true = false
                        }
                    })
                }
            },
        });  
    </script>
@endsection