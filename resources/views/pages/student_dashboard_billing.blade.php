@extends('layout.default')

@section('meta')
    @component('components.meta')
        @slot('title')
             Dreamguru | Student Dashboard
        @endslot

        @slot('description')
            Dreamaxtion helps companies and people maximize potential through Human Capital management system and psychometric assessment for personal and career growth.
        @endslot
    @endcomponent
@endsection

@section('css')
    <style> 
        
    </style>
@endsection

@section('content')
<div class="main-padding-h main-padding-v" v-cloak>
    <div v-if="page_ready">
        <div class="row pb-5 pt-4">
            @if(Session::has("errors"))
                <div class="alert alert-danger" role="alert">
                    @foreach(Session::get('errors') as $message)
                        <p class="color-red">
                            {{ucfirst($message)}}
                        </p>
                  @endforeach
                </div>
            @endif
            <div v-if="records.length == 0 && history.length == 0">
                <h3 class="fw-semi color-gold">
                    Uh-oh there is no payment you've been taken.
                </h3>
            </div>
            <div v-else>
                <div v-if="!records.length == 0" class="mb-4">
                    <div class="d-flex align-items-center">
                        <h6 class="color-gold me-3 mb-0" style="white-space: nowrap">
                            ACTIVE PAYMENT
                        </h6>
                        <div class="hr"></div>
                    </div>
                    <div class="bg-second history-container mt-4 table-responsive-md">
                        <table class="table mt-2">
                            <tr>
                                <th class="color-white">No</th>
                                <th class="color-white">Detail</th>
                                <th class="color-white">Status</th>
                                <th class="color-white">Date</th>
                            </tr>
                            <template v-for="(record, index) of displayedRecords">
                                <tr>
                                    <td>
                                        <p class="me-4">
                                            @{{ record.item_index + 1 }}.
                                        </p>
                                    </td>
                                    <td>
                                        <div>
                                            <a class="fw-semi color-gold" :href="'/student/billing/detail?order=' + record.order_id">
                                                @{{ record.course.title }}
                                            </a>
                                            <p v-if="record.final_amount == 0">
                                                Invoice : @{{ record.order_id }} - FREE
                                            </p>
                                            <p v-else>
                                                Invoice : @{{ record.order_id }} - Rp. @{{ record.final_amount }}
                                            </p>
                                        </div>
                                    </td>
                                    <td>
                                        <p class="ms-auto me-3 text-capitalize">
                                            @{{ record.status }}
                                        </p>
                                    </td>
                                    <td>
                                        <p class="me-3">
                                            @{{ record.updated_at }}
                                        </p>
                                    </td>
                                </tr>
                            </template>
                        </table>
                        <pagination class="mt-4" :records="records.length" :per-page="perPage" v-model="page" @paginate="paginationCallback"> 
                        
                        </pagination>
            
                    </div>
                </div>
                <div v-if="!history.length == 0">
                    <div class="d-flex align-items-center">
                        <h6 class="color-gold me-3 mb-0" style="white-space: nowrap">
                            PAYMENT HISTORY
                        </h6>
                        <div class="hr"></div>
                    </div>
                    <div class="bg-second history-container mt-4 table-responsive-md">
                        <table class="table mt-2">
                            <tr>
                                <th class="color-white">No</th>
                                <th class="color-white">Detail</th>
                                <th class="color-white">Status</th>
                                <th class="color-white">Date</th>
                            </tr>
                            <template v-for="(record, index) of displayedHistory">
                                <tr>
                                    <td>
                                        <p class="me-4">
                                            @{{ record.item_index + 1 }}.
                                        </p>
                                    </td>
                                    <td>
                                        <div>
                                            <a class="fw-semi color-gold" :href="'/student/billing/detail?order=' + record.order_id">
                                                @{{ record.course.title }}
                                            </a>
                                            <p v-if="record.final_amount == 0">
                                                Invoice : @{{ record.order_id }} - FREE
                                            </p>
                                            <p v-else>
                                                Invoice : @{{ record.order_id }} - Rp. @{{ record.final_amount }}
                                            </p>
                                        </div>
                                    </td>
                                    <td>
                                        <p v-if="record.status == 'completed'" class="ms-auto me-3 text-capitalize color-gold">
                                            @{{ record.status }}
                                        </p>
                                        <p v-else class="ms-auto me-3 text-capitalize">
                                            @{{ record.status }}
                                        </p>
                                    </td>
                                    <td>
                                        <p class="me-3">
                                            @{{ record.updated_at }}
                                        </p>
                                    </td>
                                </tr>
                            </template>
                        </table>
                        <pagination class="mt-4" :records="history.length" :per-page="perPage" v-model="page" @paginate="paginationCallback"> 
                        
                        </pagination>
            
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div v-if="!page_ready">
    <vue-skeleton-loader
        :height="300"
        class="w-100 mb-4"
        color="rgba(52, 52, 52, 1)"
        animation="fade"
    ></vue-skeleton-loader>
    <vue-skeleton-loader
        :height="300"
        class="w-100 mb-4"
        color="rgba(52, 52, 52, 1)"
        animation="fade"
    ></vue-skeleton-loader>
    </div>
</div>
@endsection

@section('js')
    <script>
        'use strict';
        var app = new Vue({
            el: '#app',
            data(){
                return {
                    page: 1,
                    perPage: 5,
                    records:[],
                    history:[],
                    page_ready : false,
                }
            },
            created() {
                // here we simulate an api call that returns the complete list
                axios({
                    method: 'get',
                    url: this.api_url + '/api/v1/users/self/transactions',
                    headers: {
                        'Authorization' : 'Bearer ' + bearer,
                        'Accept' : 'application/json',
                    },
                })
                .then(response => {
                    this.records = response.data
                    var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour : 'numeric', minute : 'numeric'}
                    for(var i = 0; i < this.records.length; i++){  
                        this.records[i].updated_at = new Date(this.records[i].updated_at).toLocaleString('id-ID', options); 
                        this.records[i].final_amount = Number(this.records[i].final_amount).toLocaleString('id'); 
                        this.records[i].item_index = i
                    }
                })
                axios({
                    method: 'get',
                    url: this.api_url + '/api/v1/users/self/transactions/history',
                    headers: {
                        'Authorization' : 'Bearer ' + bearer,
                        'Accept' : 'application/json',
                    },
                })
                .then(response => {
                    this.history = response.data
                    var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour : 'numeric', minute : 'numeric'}
                    for(var i = 0; i < this.history.length; i++){  
                        this.history[i].updated_at = new Date(this.history[i].updated_at).toLocaleString('id-ID', options);  
                        this.history[i].final_amount = Number(this.history[i].final_amount).toLocaleString('id');
                        this.history[i].item_index = i
                    }
                    this.page_ready = true
                })
            },
            computed: {
                displayedRecords() {
                    const startIndex = this.perPage * (this.page - 1);
                    const endIndex = startIndex + this.perPage;
                    return this.records.slice(startIndex, endIndex);
                },
                displayedHistory() {
                    const startIndex = this.perPage * (this.page - 1);
                    const endIndex = startIndex + this.perPage;
                    return this.history.slice(startIndex, endIndex);
                }
            },
            
            mounted: function(){
                // 
            },
            methods: {
                paginationCallback(page) {
                    console.log(`Page ${page} was selected. Do something about it`);
                }
            },  
        })
    </script>
@endsection