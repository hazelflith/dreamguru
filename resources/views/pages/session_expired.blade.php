@extends('layout.default')

@section('meta')
    @component('components.redirect')
        <title>Redirecting...</title>
    @endcomponent
@endsection

@section('css')
    <style> 
        
    </style>
@endsection

@section('content')
@endsection

@section('js')
    <script>
        'use strict';
        var app = new Vue({
            el: '#app',
        })
    </script>

@endsection