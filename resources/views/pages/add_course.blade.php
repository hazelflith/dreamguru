@extends('layout.default')

@section('meta')
    @component('components.meta')
        @slot('title')
             Add Course
        @endslot

        @slot('description')
            Dreamaxtion helps companies and people maximize potential through Human Capital management system and psychometric assessment for personal and career growth.
        @endslot
    @endcomponent
@endsection

@section('css')
    <style> 
        
    </style>
@endsection

@section('content')
<div class="main-padding-h main-padding-v">
    <h3 class="color-gold fw-semi">
        Add Course
    </h3>
    <h6 class="color-muted">
        MY COURSE > ADD
    </h6>
</div>
<div class="row g-0">
    <div class="col-8">
        <div class="main-padding-h">
            <div v-if="error_show" class="alert alert-danger" role="alert">
                <div v-for="value in error_show">
                    <p class="color-red">
                        @{{ value[0] }}
                    </p>
                </div>                
            </div>
            <div class="d-flex align-items-center">
                <h6 class="color-gold me-3 mb-0" style="white-space: nowrap">
                    BASIC INFORMATION
                </h6>
                <div class="hr"></div>
            </div>
            <div class="mb-3"></div>
            <label for="CourseTitle" class="form-label" >
                Course Title
            </label>
            <input type="text" class="form-control" v-model="req.title" name="CourseTitle" v-validate="'required'">
            <div id="titleHelp" class="form-text mb-2">
                Pick a Short and Clear Title
            </div>
            <label for="CourseLevel" class="form-label">
                Set Course Level
            </label>
            <select type="text" class="form-control mb-2" v-model="req.level" id="CourseLevel" v-validate="'required'">
                <option value="Beginner">Beginner</option>
                <option value="Intermediate">Intermediate</option>
                <option value="Advanced">Advanced</option>
            </select>
            <label for="EstimatedTime" class="form-label">
                Set Estimated Time (Hour : Minute)
            </label>
            <div class="d-flex w-50 mb-2">
                <input 
                    placeholder="Hour" 
                    type="number" 
                    class="form-control me-2" 
                    v-model="req.estimated_time_HH" 
                    id="EstimatedTimeHH" 
                    v-validate="'required'"
                >
                <input 
                    placeholder="Minute" 
                    type="number" 
                    class="form-control" 
                    v-model="req.estimated_time_MM" 
                    id="EstimatedTimeMM" 
                    v-validate="'required'"
                >
            </div>
            <label for="wysiwyg" class="form-label">
                Description
            </label>
            <textarea id="wysiwyg" v-model="req.desc" v-validate="'required'">
                Lorem Ipsum
            </textarea>
            <div id="wysiwygHelp" class="form-text mb-2">
                Shortly describe this course
            </div>
            <div class="mb-3"></div>
        </div>
    </div>
        <div class="col-4">
        <div class="form-container me-5 mb-4">
            <div class="text-center"> 
                <a class="btn  d-inline-block fill-gold fw-semi my-4"
                    type="submit" role="button" @click="createCourse()"
                >
                    SAVE COURSE
                </a>
            </div>
        </div>
        <div class="d-flex align-items-center mb-4">
            <h6 class="color-gold me-3 mb-0" style="white-space: nowrap">
                OPTIONS
            </h6>
            <div class="hr me-5"></div>
        </div>
        <div class="form-container me-5 mb-4">
            <div class="py-3 px-3">
                <label for="CourseCategory" class="form-label">
                    CATEGORY
                </label>
                <select class="form-select form-dark" id="CourseCategory" aria-label="CourseCategory" v-model="req.category_id" v-validate="'required'">
                    <option v-for="category in categories" :value="category.id">@{{category.name}}</option>
                </select>
                <div id="categoryHelp" class="form-text mb-2">
                    Select a Category
                </div>
            </div>
            <div class="pb-3 px-3">
                <label for="CourseLang" class="form-label">
                    LANGUAGE
                </label>
                <select v-model="req.language" class="form-select form-dark" id="CourseLang" aria-label="CourseLang" v-validate="'required'">
                    <option value="english">English</option>
                    <option value="Indonesia">Bahasa Indonesia</option>
                    <option value="korean">Korean</option>
                </select>
                <div id="langHelp" class="form-text mb-2">
                    Select course language
                </div>
            </div>
            <div class="pb-3 px-3">
                <label for="CoursePrice" class="form-label">
                    PRICE
                </label>
                <div class="input-group" id="CoursePrice" v-validate="'required'">
                    <span class="input-group-text price">
                        Rp.
                    </span>
                    <money v-model="req.price" class="form-control form-dark" 
                    aria-label="Amount (to the nearest rupiah)"></money>
                </div>
                <div id="priceHelp" class="form-text mb-2">
                    Type 0 to set the course price free
                </div>      
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
    <script>
        'use strict';
        var app = new Vue({
            el: '#app',
            data() {
                return {
                    error_show: null,
                    categories: null,
                    course: null,
                    req: {
                        title: '',
                        price:'',
                        desc:'',
                        level:'',
                        estimated_time_HH: 0,
                        estimated_time_MM: 0,
                        language:'Indonesia',
                        category_id:'',
                    }
                }
            },
            mounted: function(){
                axios({
                    method: 'get',
                    url: this.api_url + '/api/v1/categories',
                    headers: {
                        'Authorization' : 'Bearer ' + bearer,
                        'Accept' : 'application/json',
                    },
                })
                .then(response => {
                    this.categories = response.data

                })
                .catch(function (error) {
                    if (error.response) {
                        //
                    }
                })

                setTimeout(() => {
                    tinymce.init({
                        selector: '#wysiwyg',
                        menubar : 'edit format insert',
                        height : "480",
                        skin: 'oxide-dark',
                        content_css: 'dark',
                        plugins: 'paste lists nonbreaking',
                        paste_as_text: true,
                        toolbar: 'numlist bullist',
                        nonbreaking_force_tab: true
                    });
                    // The DOM element you wish to replace with Tagify
                    var input = document.querySelector('input[name=tags]');
                    
                    // initialize Tagify on the above input node reference
                    new Tagify(input)
                }, 1000);
            },
            methods: {
                createCourse(){
                    this.showLoading()
                    var self = this
                    axios({
                        method: 'post',
                        url: this.api_url + '/api/v1/courses',
                        headers: {
                            'Authorization' : 'Bearer ' + bearer,
                            'Accept' : 'application/json',
                        },
                        data: {
                            price: this.req.price,
                            desc: tinyMCE.activeEditor.getContent(),
                            level: this.req.level,
                            estimated_time: (this.req.estimated_time_HH * 3600) + (this.req.estimated_time_MM * 60),
                            language: this.req.language,
                            title: this.req.title,
                            category_id: this.req.category_id,
                            teacher_id: "{{session('data')['id']}}"
                        }

                    })
                    .then(response => {
                        this.hideLoading()
                        window.location.href = "/teacher/course/" + response.data.id + "/edit" 

                    })
                    .catch(function (error) {
                        if (error.response) {
                            self.error_show = error.response.data.errors;
                            window.location.href = '#'
                            self.hideLoading()
                        }
                    })
                }
            },
        })
    </script>
@endsection