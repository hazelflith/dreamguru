@extends('layout.default')

@section('meta')
    @component('components.meta')
        @slot('title')
             Dreamguru | Course
        @endslot

        @slot('description')
            Dreamaxtion helps companies and people maximize potential through Human Capital management system and psychometric assessment for personal and career growth.
        @endslot
    @endcomponent
@endsection

@section('css')
    <style> 
        
    </style>
@endsection

@section('content')
<div class="main-padding-h main-padding-v">
    <h3 class="color-gold fw-semi">
        Course Dashboard
    </h3>
    <h6 class="color-muted" v-if="is_course_ready">
        @{{course.title}} > DASHBOARD
    </h6>
    <vue-skeleton-loader
        :height="30"
        class="w-100"
        v-if="!is_course_ready"
        color="rgba(52, 52, 52, 1)"
        animation="fade"
    ></vue-skeleton-loader>
    <div class="d-flex align-items-center pt-4 mb-3">
        <h6 class="color-gold me-3 mb-0" style="white-space: nowrap">
            QUIZ LISTS
        </h6>
        <div class="hr"></div>
    </div>
    <vue-skeleton-loader
        :height="200"
        v-if="!is_course_ready"
        class="w-100 mb-2"
        color="rgba(52, 52, 52, 1)"
        animation="fade"
    ></vue-skeleton-loader>
    <div v-if="is_course_ready">
        <div class="table-responsive-md" v-if="quizzes.length > 0">
            <table class="table mt-2">
                <tr>
                    <th class="color-white">No</th>
                    <th class="color-white">Quiz</th>
                    {{-- <th class="color-white">Average Score</th> --}}
                    <th class="color-white">Actions</th>
                </tr>
                <template v-for="section in course_quizzes">
                    <tr v-for="(content,index) in section.section_content_orders">
                        <td>
                            <p class="me-4">
                                @{{index + 1}}.
                            </p>
                        </td>
                        <td>
                            <p class="me-4">
                                @{{content.title}}
                            </p>
                        </td>
                        {{-- <td>
                            <p class="me-4">@{{content.average_score}}</p>
                        </td> --}}
                        <td>
                            <a :href="'/teacher/course/'+ section.course_id + '/' + content.content_id + '/view'">
                                <i class="fa-solid fa-eye color-gold"></i></a>
                        </td>
                    </tr>
                </template>
            </table>
        </div>
        <p v-else>No Quiz in this Courses</p>
    </div>

    <div class="d-flex align-items-center pt-4 mb-3">
        <h6 class="color-gold me-3 mb-0" style="white-space: nowrap">
            STUDENT PROGRESS
        </h6>
        <div class="hr"></div>
    </div>
    <vue-skeleton-loader
        :height="200"
        v-if="!is_course_ready"
        class="w-100 mb-2"
        color="rgba(52, 52, 52, 1)"
        animation="fade"
    ></vue-skeleton-loader>
    <div class="d-flex flex-wrap" v-if="is_course_ready">
        <h6>
            Total Student Enrolled: @{{course.total_students}}
        </h6>
        <h6 class="ms-md-auto me-3">
            On Progress: @{{course.students_on_progress}}
        </h6>
        <h6 class="color-gold">
            Completed : @{{course.students_completed}}
        </h6>
    </div>
    <div class="table-responsive-md" v-if="is_course_ready">
        <table class="table mt-2">
            <tr>
                <th class="color-white">No.</th>
                <th class="color-white">Name</th>
                <th class="color-white">Start Date</th>
                <th class="color-white">Complete Date</th>
            </tr>
            <tr v-for="(student,index) in course.students">
                <td>
                    <p class="me-4">
                        @{{index + 1}}.
                    </p>
                </td>
                <td>
                    <div class="d-flex align-items-center">
                        <img 
                            v-if="student.profile.avatar" 
                            class="me-2 avatar" 
                            :src="api_url + '/storage/' + student.profile.avatar" 
                            alt="avatar" 
                            height="30"
                        >
                        <img v-else class="me-2 avatar" src="/img/placeholder.png" alt="avatar">
                        <p class="me-4">
                            @{{student.profile.name}}
                        </p>
                    </div>
                </td>
                <td>
                    <p class="me-4">
                        @{{student.pivot.created_at}}
                    </p>
                </td>
                <td>
                    <p v-if="student.pivot.status == 'completed'" class="me-4 color-gold">
                        @{{student.pivot.updated_at}}
                    </p>
                    <p v-else class="me-4">
                        On progress
                    </p>
                </td>
                    {{-- <div class="progress big rounded my-2 p-0 me-2" style="height: 12px;">
                        <div class="progress-bar" role="progressbar" style="width: 90%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <span><span class="color-gold">90%</span> Complete</span> --}}
            </tr>
        </table>
    </div>
</div>
@endsection

@section('js')
    <script>
       'use strict';
        var app = new Vue({
            el: '#app',

        data() {
            return {
                course: null,
                is_course_ready: false,
                course_quizzes: [],
                quizzes: []
            }
        },
        mounted: function() {
            axios({
                method: 'get',
                url: this.api_url + '/api/v1/teachers/{{session('data')['id']}}/courses/{{$course_id}}',
                headers: {
                    'Authorization': 'Bearer ' + bearer,
                    'Accept': 'application/json',
                }
            })
            .then(response => {
                this.course = response.data
                
                var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour : 'numeric', minute : 'numeric'}
                for(var i = 0; i < this.course.students.length; i++){
                    this.course.students[i].pivot.created_at = new Date(this.course.students[i].pivot.created_at).toLocaleString('id-ID', options);  
                    this.course.students[i].pivot.updated_at = new Date(this.course.students[i].pivot.updated_at).toLocaleString('id-ID', options);  
                }
                for(var i = 0; i < this.course.course_sections.length; i++){
                    this.course_quizzes[i] = this.course.course_sections[i]
                    for(var j = 0; j < this.course.course_sections[i].section_content_orders.length; j++){
                        this.course_quizzes[i].section_content_orders = _.filter(this.course.course_sections[i].section_content_orders, ['endpoint', 'section_quizzes']);
                        this.quizzes = _.filter(this.course.course_sections[i].section_content_orders, ['endpoint', 'section_quizzes']);
                    }
                }
                this.is_course_ready = true
            })
        }
        }) 
    </script>
@endsection