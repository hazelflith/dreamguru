@extends('layout.default')

@section('meta')
    @component('components.meta')
        @slot('title')
             Edit Courses
        @endslot

        @slot('description')
            Dreamaxtion helps companies and people maximize potential through Human Capital management system and psychometric assessment for personal and career growth.
        @endslot
    @endcomponent
@endsection

@section('css')
    <style> 
        
    </style>
@endsection

@section('content')
<div class="main-padding-h main-padding-v" v-cloak>
    <h3 class="color-gold fw-semi">
        Edit Course
    </h3>
    <h6 v-if="is_course_ready" class="color-muted text-uppercase">
        @{{ course.title }} > EDIT
    </h6>
    <vue-skeleton-loader
        :height="30"
        class="w-100"
        v-if="!is_course_ready"
        color="rgba(52, 52, 52, 1)"
        animation="fade"
    ></vue-skeleton-loader>
</div>
<div class="sticky-container" v-if="is_course_ready" v-cloak>
    <p class="pt-3 ps-3">
        Shortcuts :
    </p>
    <div class="d-flex p-3">
            <div v-if="is_course_ready" class="me-3">
                <div v-if="course.status == 'completed'">
                        <a class="btn fill-gold fw-semi" @click="updateCourse('completed',true)">
                            SAVE CHANGES
                        </a>
                </div>
                <div v-else>
                        <a class="btn fill-gold fw-semi" data-bs-toggle="modal" data-bs-target="#saveModal">
                            PUBLISH COURSE
                        </a>
                </div>
            </div>
            <div v-if="is_course_ready">
                <div v-if="course.status != 'completed'">
                    <a class="btn outline-gold" 
                        @click="updateCourse('draft')"
                    >
                        Save Draft
                    </a>
                </div>
                <div v-if="course.status == 'completed'">
                    <a class="btn outline-gold" data-bs-toggle="modal" data-bs-target="#unpublishModal">
                        Unpublish Course
                    </a>
                </div>
            </div>
            <button class="btn color-red"  data-bs-toggle="modal" data-bs-target="#trashModal">
                <i class="fa-solid fa-trash-can"></i>
            </button>
            <div v-if="is_course_ready">
                <a class="btn color-gold" :href="'/course/'+ course.slug + '/preview'">
                    <i class="fa-solid fa-eye"></i>
                </a>
            </div>
    </div>
</div>
<div class="main-padding-edit">
    <div class="row g-0 g-lg-5" v-cloak>
        <div class="col-lg-8">
            <div class="">
                <div v-if="error_show" class="alert alert-danger" role="alert">
                    <div v-for="value in error_show">
                        <p class="color-red">
                            @{{ value[0] }}
                        </p>
                    </div>                
                </div>
                <div v-if="error_show2" class="alert alert-danger" role="alert">
                    <div>
                        <p class="color-red">
                            @{{ error_show2 }}
                        </p>
                    </div>                
                </div>
                <div v-if="error_discount" class="alert alert-danger" role="alert">
                    <div v-if="error_discount">
                        <p class="color-red">
                            @{{ error_discount }}
                        </p>
                    </div>
                </div>
                <div class="d-flex align-items-center">
                    <h6 class="color-gold me-3 mb-0" style="white-space: nowrap">
                        BASIC INFORMATION
                    </h6>
                    <h6 class="ms-auto color-muted text-capitalize" v-if="is_course_ready">
                        Status: @{{ course.status }}
                    </h6>
                    <vue-skeleton-loader
                        :width="200"
                        :height="30"
                        v-if="!is_course_ready"
                        class="ms-auto"
                        color="rgba(52, 52, 52, 1)"
                        animation="fade"
                    ></vue-skeleton-loader>
                </div>
                <div class="mb-2"></div>
                <div class="hr mb-2"></div>
                <label for="CourseTitle" class="form-label">
                    Course Title
                </label>
                <div v-if="is_course_ready">
                    <input v-if="course.status != 'completed'" type="text" class="form-control" v-model="req.title" id="CourseTitle" required>
                    <input v-else type="text" class="form-control" v-model="req.title" id="CourseTitle" disabled>
                </div>
                <vue-skeleton-loader
                    :height="25"
                    class="w-100"
                    v-if="!is_course_ready"
                    color="rgba(52, 52, 52, 1)"
                    animation="fade"
                ></vue-skeleton-loader>
                <div id="titleHelp" class="form-text mb-2">
                    Pick a Short and Clear Title
                </div>
    
                <label for="hero-background" class="form-label">
                    Set Featured Image
                </label>
                <img id="hero-background" :src="api_url + '/storage/' + hero_background" 
                    alt="background" height="300" class="w-100" v-if="hero_background"
                >
                <vue-skeleton-loader
                    :height="300"
                    v-if="!is_course_ready"
                    class="w-100"
                    color="rgba(52, 52, 52, 1)"
                    animation="fade"
                ></vue-skeleton-loader>
                <input type="file" class="form-control mt-3 mb-3" accept="image/png, image/jpeg" @change="uploadHero" ref="hero_background">
                <div id="imageHelp" class="form-text mb-2">
                    Images Max Size 1MB & Use 4:1 Ratio Images
                </div>
                <label for="hero-background" class="form-label">
                    Set Thumbnail
                </label>
                <div class="d-flex align-items-center mb-2">
                    <img id="hero-background" :src="api_url + '/storage/' + thumbnail" 
                        alt="thumbnail" height="60" width="60" v-if="thumbnail" class="me-3 thumbnail"
                    >
                    <vue-skeleton-loader
                        :width="60"
                        :height="60"
                        v-if="!is_course_ready"
                        color="rgba(52, 52, 52, 1)"
                        animation="fade"
                        class="me-3"
                    ></vue-skeleton-loader>
                    <div>
                        <input type="file" class="form-control" accept="image/png, image/jpeg" @change="uploadThumbnail" ref="thumbnail">
                        <div id="thumbnailHelp" class="form-text mb-2">
                            Images Max Size 1MB & Use 1:1 Ratio Images
                        </div>
                    </div>
                </div>
    
                <label for="CourseLevel" class="form-label">
                    Set Course Level
                </label>
                <div v-if="is_course_ready">
                    <select v-if="course.status != 'completed'" type="text" class="form-control" v-model="req.level" id="CourseLevel" :required="course.status != 'completed'" :disabled="course.status == 'completed'">
                        <option value="Beginner">Beginner</option>
                        <option value="Intermediate">Intermediate</option>
                        <option value="Advanced">Advanced</option>
                    </select>
                </div>
                <label for="EstimatedTime" class="form-label">
                    Set Estimated Time (Hour : Minute)
                </label>
                <div class="d-flex w-50 mb-2" v-if="is_course_ready">
                    <input placeholder="Hour" type="number" class="form-control me-2" 
                    v-model="req.estimated_time_HH" id="EstimatedTimeHH" required>
                    <input placeholder="Minute" type="number" class="form-control" 
                    v-model="req.estimated_time_MM" id="EstimatedTimeMM" required>
                </div>
                <div class="d-flex"  v-if="!is_course_ready">
                    <vue-skeleton-loader
                        :height="36"
                        :width="200"
                        class="me-2"
                        color="rgba(52, 52, 52, 1)"
                        animation="fade"
                    ></vue-skeleton-loader>
                    <vue-skeleton-loader
                        :height="36"
                        :width="200"
                        class="me-2"
                        color="rgba(52, 52, 52, 1)"
                        animation="fade"
                    ></vue-skeleton-loader>
                </div>
                <label for="wysiwyg" class="form-label">
                    Description
                </label>
                <textarea id="wysiwyg" v-model="req.desc" required v-if="is_course_ready">
                    @{{ course.desc }}
                </textarea>
                <vue-skeleton-loader
                    :height="480"
                    v-if="!is_course_ready"
                    class="w-100"
                    color="rgba(52, 52, 52, 1)"
                    animation="fade"
                ></vue-skeleton-loader>
                <div id="wysiwygHelp" class="form-text mb-2">
                    Shortly describe this course
                </div>
                <div class="mb-3"></div>
                <vue-skeleton-loader
                    :height="480"
                    v-if="!is_course_ready"
                    class="w-100 mb-5"
                    color="rgba(52, 52, 52, 1)"
                    animation="fade"
                ></vue-skeleton-loader>
                <div v-if="is_course_ready">
                    <div class="d-flex align-items-center">
                        <h6 class="color-gold me-3 mb-0" style="white-space: nowrap">
                            SECTIONS
                        </h6>
                        <a v-if="!reorder" class="btn btn-rounded btn-sm outline-gold me-3 fw-semi" style="white-space: nowrap" role="button" @click="reorderSwitch(true)">Reorder Section</a>
                        <a v-else class="btn btn-rounded btn-sm outline-gold me-3 fw-semi" style="white-space: nowrap" role="button" @click="reorderSwitch(false)">Back</a>
                        <div class="hr"></div>
                    </div>
                    <p class="mb-3" v-if="course.status == 'completed'">
                        You must unpublish the course to edit it's section
                    </p>
                    <div v-if="course.status != 'completed'">
                        
                        <div class="mb-3"></div>
                        <draggable class="list-group" v-if="reorder" v-model="req.course_sections" group="sectionhead" 
                            v-bind="dragOptionsHead" @start="drag = true" @end="drag = false">
                            <transition-group type="transition" :name="!drag ? 'flip-list' : null">
                            <div v-if="req.course_sections" class="accordion mb-4" id="accordionPanelsStayOpenExample" 
                                v-for="(section, index) in req.course_sections" :key="section.id"
                                >
                                <div class="d-flex draggable">
                                    <i class="fa-solid fa-grip-lines mx-auto my-2"></i>
                                </div>
                                <div class="accordion-item" v-if="section.status == 'completed'">
                                    <h2 class="accordion-header" :id="'panelsStayOpen-'+index">
                                        <button class="accordion-button d-flex" type="button" data-bs-toggle="collapse"
                                            :data-bs-target="'#panelsStayOpen-collapse'+index" aria-expanded="true"
                                            :aria-controls="'panelsStayOpen-collapse'+index"
                                        >
                                            <a href="javascript:void(0)" class="color-red"  data-bs-toggle="modal" 
                                                :data-bs-target="'#deleteSectionModal' + section.id">
                                                <i class="fa-solid fa-trash-can"></i>
                                            </a>
                                            <a href="javascript:void(0)" class="ms-2 color-gold me-3" data-bs-toggle="modal" 
                                                :data-bs-target="'#sectionEditModal' + section.id" @click="assignSectionTitle(section.title)"
                                            >
                                                <i class="fa-solid fa-pen-to-square"></i>
                                            </a>
                                            @{{ section.title }} 
                                        </button>
                                    </h2>
                                    {{-- MODAL--}}
                                    <div class="modal fade" :id="'deleteSectionModal' + section.id" tabindex="-1" aria-labelledby="deleteSectionModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="deleteSectionModalLabel">
                                                        Delete Section Confirmation
                                                    </h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    <p class="mb-3">
                                                        Once you delete a section, you cannot view or edit it's content.
                                                    </p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary outline-gold me-2" data-bs-dismiss="modal">
                                                        Cancel
                                                    </button>
                                                    <a class="btn fill-gold fw-semi" @click="deleteSection(section.id)">
                                                        Delete Section
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Modal -->
                                    <div class="modal fade" :id="'sectionEditModal' + section.id" tabindex="-1" aria-labelledby="sectionEditModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="sectionEditModalLabel">
                                                        Edit Section
                                                    </h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    <div v-if="error_show3" class="alert alert-danger" role="alert">
                                                        <div>
                                                            <p class="color-red">
                                                                @{{ error_show3 }}
                                                            </p>
                                                        </div>                
                                                    </div>
                                                    <label class="my-2" for="titleQuiz">
                                                        Section Title:
                                                    </label>
                                                    <input type="text" class="form-control form-dark" v-model="section_edit_title" id="titleQuiz">
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary outline-gold me-2" data-bs-dismiss="modal">
                                                        Cancel
                                                    </button>
                                                    <a href="javascript:void(0)" class="btn fill-gold fw-semi" @click="editSection(section.id)">
                                                        Save
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <div :id="'panelsStayOpen-collapse'+index" class="accordion-collapse collapse show"
                                    :aria-labelledby="'panelsStayOpen-'+index"
                                >
                                    <draggable class="list-group" v-model="section.section_content_orders" group="people" v-bind="dragOptions"
                                    @start="drag = true" @end="drag = false">
                                        <transition-group type="transition" :name="!drag ? 'flip-list' : null">
                                        <div v-for="(content, index) in section.section_content_orders" :key="content.id" class="accordion-body">
                                            <div class="d-flex align-items-center">
                                                <i class="fa-solid fa-grip-lines me-3 opacity-50"></i>
                                                <i v-if="content.endpoint == 'parts'" class="fa-solid fa-book me-2 color-gold"></i>
                                                <i v-if="content.endpoint == 'section_quizzes'" class="fa-solid fa-circle-question me-2 color-gold"></i>
                                                <p>
                                                    @{{ content.title }}
                                                </p>
                                                <div class="dropdown show ms-auto d-block d-sm-none" v-if="content.endpoint == 'section_quizzes'">
                                                    <a :href="'#dropdownMoreMenu'+ course.id" style="font-size: 24px" role="button" :id="'dropdownMore'+ course.id" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <div class="mx-3">
                                                            <i class="fa-solid fa-ellipsis-vertical color-gold"></i>
                                                        </div>
                                                    </a>
                                                    <div class="dropdown-menu" :id="'dropdownMoreMenu'+ course.id" :aria-labelledby="'dropdownMore'+ course.id">
                                                        <a class="dropdown-item" href="javascript:void(0)" 
                                                            @click="getSectionQuiz(content.content_id,section.id)" >
                                                            Edit Description
                                                        </a>
                                                        <a class="dropdown-item" 
                                                            :href="'/teacher/quiz/'+ content.content_id + '/edit/redirect'">
                                                            Edit Questions
                                                        </a>
                                                        <a class="dropdown-item color-red" href="javascript:void(0)" 
                                                            data-bs-toggle="modal" :data-bs-target="'#deleteSectionQuizModal' + index">
                                                            Archive
                                                        </a>
                                                    </div>
                                                </div>
                                                <a v-if="content.endpoint == 'section_quizzes'" href="javascript:void(0)" class="ms-auto btn outline-gold me-2 d-none d-md-block"
                                                    @click="getSectionQuiz(content.content_id,section.id)"
                                                >
                                                    Edit Description
                                                </a>
                                                <!-- Modal -->
                                                <div v-if="section_quiz" class="modal fade" :id="'quizEditModal' + section.id" tabindex="-1" aria-labelledby="quizEditModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="quizModalLabel">
                                                                    Edit Quiz
                                                                </h5>
                                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <label class="my-2" for="titleQuiz" >
                                                                    Quiz Title:
                                                                </label>
                                                                <input type="text" class="form-control form-dark" v-model="section_quiz.title" id="titleQuiz">
                
                                                                <label class="my-2" for="attemptQuiz" >
                                                                    Max Attempt (0 for Unlimited):
                                                                </label>
                                                                <input type="number" class="form-control form-dark" v-model="section_quiz.max_attempt" 
                                                                    placeholder="3" id="attemptQuiz"
                                                                >
                                                                <label class="my-2" for="attemptQuiz" >
                                                                    Point Requirement
                                                                </label>
                                                                <input type="number" class="form-control form-dark" v-model="section_quiz.point_requirement" 
                                                                    placeholder="Enter the minimum point to pass" id="attemptQuiz"
                                                                >
                                                                <label class="my-2" for="QuizDesc">
                                                                    Description:
                                                                </label>
                                                                <textarea class="form-control form-dark" v-model="section_quiz.desc" 
                                                                    placeholder="Write your quiz description here" id="QuizDesc"
                                                                ></textarea>
                
                                                                <input class="my-2 mt-4" type="checkbox" id="isUnlock" v-model="section_quiz.is_unlock">
                                                                <label for="isUnlock" class="form-label">
                                                                    Is Unlocked?
                                                                </label>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary outline-gold me-2" data-bs-dismiss="modal">
                                                                    Cancel
                                                                </button>
                                                                <a href="javascript:void(0)" class="btn  fill-gold fw-semi" @click="updateQuiz(section_quiz.id)">
                                                                    Save
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <a v-if="content.endpoint == 'section_quizzes'" :href="'/teacher/quiz/'+ content.content_id + '/edit/redirect'" class="btn outline-gold d-none d-md-block">
                                                    Edit Questions
                                                </a>
                                                <a v-if="content.endpoint == 'parts'" :href="'/course/'+ course.id + '/parts/' + content.content_id + '/edit'" class="ms-auto color-gold">
                                                    <i class="fa-solid fa-pen-to-square"></i>
                                                </a>
                                                <a v-if="content.endpoint == 'section_quizzes'" href="javascript:void(0)" data-bs-toggle="modal" :data-bs-target="'#deleteSectionQuizModal' + index" class="ms-3 color-red d-none d-md-block">
                                                    <i class="fa-solid fa-trash-can"></i>
                                                </a>
                                                 {{-- MODAL--}}
                                                <div class="modal fade" :id="'deleteSectionQuizModal' + index" tabindex="-1" aria-labelledby="deleteSectionQuizModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="deleteSectionQuizModalLabel">
                                                                    Delete Quiz Confirmation
                                                                </h5>
                                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p class="mb-3">
                                                                    Once you delete a quiz, you cannot view or edit it's content.
                                                                </p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary outline-gold me-2" data-bs-dismiss="modal">
                                                                    Cancel
                                                                </button>
                                                                <a class="btn fill-gold fw-semi" @click="deleteSectionQuiz(content.content_id)">
                                                                    Delete Quiz
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <a 
                                                    v-if="content.endpoint == 'parts'" 
                                                    href="javascript:void(0)" 
                                                    data-bs-toggle="modal" 
                                                    :data-bs-target="'#deletePartModal' + index" 
                                                    class="ms-3 color-red"
                                                >
                                                    <i class="fa-solid fa-trash-can"></i>
                                                </a>
                                                {{-- MODAL--}}
                                                <div class="modal fade" :id="'deletePartModal' + index" tabindex="-1" aria-labelledby="deletePartModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="deletePartModalLabel">
                                                                    Delete Part Confirmation
                                                                </h5>
                                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p class="mb-3">
                                                                    Once you delete a part, you cannot view or edit it's content.
                                                                </p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary outline-gold me-2" data-bs-dismiss="modal">
                                                                    Cancel
                                                                </button>
                                                                <a class="btn fill-gold fw-semi" @click="deletePart(content.content_id)">
                                                                    Delete Part
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </transition-group>
                                    </draggable>
                                </div>
                            </div>
                            </div>
                            </transition-group>
                        </draggable>
                        <div v-else >
                            <div v-if="req.course_sections" class="accordion mb-4" id="accordionPanelsStayOpenExample" 
                                v-for="(section, index) in req.course_sections" :key="section.id"
                                >
                                <div class="accordion-item" v-if="section.status == 'completed'">
                                    <h2 class="accordion-header" :id="'panelsStayOpen-'+index">
                                        <button class="accordion-button d-flex" type="button" data-bs-toggle="collapse"
                                            :data-bs-target="'#panelsStayOpen-collapse'+index" aria-expanded="true"
                                            :aria-controls="'panelsStayOpen-collapse'+index"
                                        >
                                            <a href="javascript:void(0)" class="color-red"  data-bs-toggle="modal" 
                                                :data-bs-target="'#deleteSectionModal' + section.id">
                                                <i class="fa-solid fa-trash-can"></i>
                                            </a>
                                            <a href="javascript:void(0)" class="ms-2 color-gold me-3" data-bs-toggle="modal" 
                                                :data-bs-target="'#sectionEditModal' + section.id" @click="assignSectionTitle(section.title)"
                                            >
                                                <i class="fa-solid fa-pen-to-square"></i>
                                            </a>
                                            @{{ section.title }} 
                                        </button>
                                    </h2>
                                    {{-- MODAL--}}
                                    <div class="modal fade" :id="'deleteSectionModal' + section.id" tabindex="-1" aria-labelledby="deleteSectionModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="deleteSectionModalLabel">
                                                        Delete Section Confirmation
                                                    </h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    <p class="mb-3">
                                                        Once you delete a section, you cannot view or edit it's content.
                                                    </p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary outline-gold me-2" data-bs-dismiss="modal">
                                                        Cancel
                                                    </button>
                                                    <a class="btn fill-gold fw-semi" @click="deleteSection(section.id)">
                                                        Delete Section
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Modal -->
                                    <div class="modal fade" :id="'sectionEditModal' + section.id" tabindex="-1" aria-labelledby="sectionEditModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="sectionEditModalLabel">
                                                        Edit Section
                                                    </h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    <div v-if="error_show3" class="alert alert-danger" role="alert">
                                                        <div>
                                                            <p class="color-red">
                                                                @{{ error_show3 }}
                                                            </p>
                                                        </div>                
                                                    </div>
                                                    <label class="my-2" for="titleQuiz">
                                                        Section Title:
                                                    </label>
                                                    <input type="text" class="form-control form-dark" v-model="section_edit_title" id="titleQuiz">
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary outline-gold me-2" data-bs-dismiss="modal">
                                                        Cancel
                                                    </button>
                                                    <a href="javascript:void(0)" class="btn fill-gold fw-semi" @click="editSection(section.id)">
                                                        Save
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div :id="'panelsStayOpen-collapse'+index" class="accordion-collapse collapse show"
                                        :aria-labelledby="'panelsStayOpen-'+index"
                                    >
                                        <div>
                                            <div v-for="(content, index) in section.section_content_orders" :key="content.id" class="accordion-body">
                                                <div class="d-flex align-items-center">
                                                    <i v-if="content.endpoint == 'parts'" class="fa-solid fa-book me-2 color-gold"></i>
                                                    <i v-if="content.endpoint == 'section_quizzes'" class="fa-solid fa-circle-question me-2 color-gold"></i>
                                                    <p>
                                                        @{{ content.title }}
                                                    </p>
                                                    <div class="dropdown show ms-auto d-block d-sm-none" v-if="content.endpoint == 'section_quizzes'">
                                                        <a :href="'#dropdownMoreMenu'+ course.id" style="font-size: 24px" role="button" :id="'dropdownMore'+ course.id" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <div class="mx-3">
                                                                <i class="fa-solid fa-ellipsis-vertical color-gold"></i>
                                                            </div>
                                                        </a>
                                                        <div class="dropdown-menu" :id="'dropdownMoreMenu'+ course.id" :aria-labelledby="'dropdownMore'+ course.id">
                                                            <a class="dropdown-item" href="javascript:void(0)" 
                                                                @click="getSectionQuiz(content.content_id,section.id)" >
                                                                Edit Description
                                                            </a>
                                                            <a class="dropdown-item" 
                                                                :href="'/teacher/quiz/'+ content.content_id + '/edit/redirect'">
                                                                Edit Questions
                                                            </a>
                                                            <a class="dropdown-item color-red" href="javascript:void(0)" 
                                                                data-bs-toggle="modal" :data-bs-target="'#deleteSectionQuizModal' + index">
                                                                Archive
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <a v-if="content.endpoint == 'section_quizzes'" href="javascript:void(0)" class="ms-auto btn outline-gold me-2 d-none d-md-block"
                                                        @click="getSectionQuiz(content.content_id,section.id)"
                                                    >
                                                        Edit Description
                                                    </a>
                                                    <!-- Modal -->
                                                    <div v-if="section_quiz" class="modal fade" :id="'quizEditModal' + section.id" tabindex="-1" aria-labelledby="quizEditModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="quizModalLabel">
                                                                        Edit Quiz
                                                                    </h5>
                                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <label class="my-2" for="titleQuiz" >
                                                                        Quiz Title:
                                                                    </label>
                                                                    <input type="text" class="form-control form-dark" v-model="section_quiz.title" id="titleQuiz">
                    
                                                                    <label class="my-2" for="attemptQuiz" >
                                                                        Max Attempt (0 for Unlimited):
                                                                    </label>
                                                                    <input type="number" class="form-control form-dark" v-model="section_quiz.max_attempt" 
                                                                        placeholder="3" id="attemptQuiz"
                                                                    >
                                                                    <label class="my-2" for="attemptQuiz" >
                                                                        Point Requirement
                                                                    </label>
                                                                    <input type="number" class="form-control form-dark" v-model="section_quiz.point_requirement" 
                                                                        placeholder="Enter the minimum point to pass" id="attemptQuiz"
                                                                    >
                                                                    <label class="my-2" for="QuizDesc">
                                                                        Description:
                                                                    </label>
                                                                    <textarea class="form-control form-dark" v-model="section_quiz.desc" 
                                                                        placeholder="Write your quiz description here" id="QuizDesc"
                                                                    ></textarea>
                    
                                                                    <input class="my-2 mt-4" type="checkbox" id="isUnlock" v-model="section_quiz.is_unlock">
                                                                    <label for="isUnlock" class="form-label">
                                                                        Is Unlocked?
                                                                    </label>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary outline-gold me-2" data-bs-dismiss="modal">
                                                                        Cancel
                                                                    </button>
                                                                    <a href="javascript:void(0)" class="btn  fill-gold fw-semi" @click="updateQuiz(section_quiz.id)">
                                                                        Save
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a v-if="content.endpoint == 'section_quizzes'" :href="'/teacher/quiz/'+ content.content_id + '/edit/redirect'" class="btn outline-gold d-none d-md-block">
                                                        Edit Questions
                                                    </a>
                                                    <a v-if="content.endpoint == 'parts'" :href="'/course/'+ course.id + '/parts/' + content.content_id + '/edit'" class="ms-auto color-gold">
                                                        <i class="fa-solid fa-pen-to-square"></i>
                                                    </a>
                                                    <a v-if="content.endpoint == 'section_quizzes'" href="javascript:void(0)" data-bs-toggle="modal" :data-bs-target="'#deleteSectionQuizModal' + index" class="ms-3 color-red d-none d-md-block">
                                                        <i class="fa-solid fa-trash-can"></i>
                                                    </a>
                                                    {{-- MODAL--}}
                                                    <div class="modal fade" :id="'deleteSectionQuizModal' + index" tabindex="-1" aria-labelledby="deleteSectionQuizModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="deleteSectionQuizModalLabel">
                                                                        Delete Quiz Confirmation
                                                                    </h5>
                                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <p class="mb-3">
                                                                        Once you delete a quiz, you cannot view or edit it's content.
                                                                    </p>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary outline-gold me-2" data-bs-dismiss="modal">
                                                                        Cancel
                                                                    </button>
                                                                    <a class="btn fill-gold fw-semi" @click="deleteSectionQuiz(content.content_id)">
                                                                        Delete Quiz
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a 
                                                        v-if="content.endpoint == 'parts'" 
                                                        href="javascript:void(0)" 
                                                        data-bs-toggle="modal" 
                                                        :data-bs-target="'#deletePartModal' + index" 
                                                        class="ms-3 color-red"
                                                    >
                                                        <i class="fa-solid fa-trash-can"></i>
                                                    </a>
                                                    {{-- MODAL--}}
                                                    <div class="modal fade" :id="'deletePartModal' + index" tabindex="-1" aria-labelledby="deletePartModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="deletePartModalLabel">
                                                                        Delete Part Confirmation
                                                                    </h5>
                                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <p class="mb-3">
                                                                        Once you delete a part, you cannot view or edit it's content.
                                                                    </p>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary outline-gold me-2" data-bs-dismiss="modal">
                                                                        Cancel
                                                                    </button>
                                                                    <a class="btn fill-gold fw-semi" @click="deletePart(content.content_id)">
                                                                        Delete Part
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-body">
                                            <i class="fa-solid fa-plus me-2 color-gold"></i>
                                            <a :href="'/course/parts/' + section.id + '/add'">
                                                Add Parts
                                            </a>
                                        </div>
                                        <div class="accordion-body">
                                            <i class="fa-solid fa-plus me-2 color-gold"></i>
                                            <a data-bs-toggle="modal" :data-bs-target="'#quizModal' + section.id" href="javascript:void(0)">
                                                Add Quiz
                                            </a>
                                            <!-- Modal -->
                                            <div class="modal fade" :id="'quizModal' + section.id" tabindex="-1" aria-labelledby="quizModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                        <h5 class="modal-title" id="quizModalLabel">
                                                            Add Quiz
                                                        </h5>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <label class="my-2" for="QuizList">
                                                                Select Quiz Template:
                                                            </label>
                                                            <select type="text" class="form-control form-dark" id="QuizList" v-model="addquiz.quiz_id" v-if="is_course_ready">
                                                                <option v-if="quiz.status =='completed'" v-for="quiz in quizzes" :value="quiz.id">@{{ quiz.title }}</option>
                                                            </select>
                
                                                            <label class="my-2" for="titleQuiz" >
                                                                Quiz Title:
                                                            </label>
                                                            <input type="text" class="form-control form-dark" v-model="addquiz.title" id="titleQuiz">
                    
                                                            <label class="my-2" for="attemptQuiz" >
                                                                Max Attempt:
                                                            </label>
                                                            <input type="number" class="form-control form-dark" v-model="addquiz.max_attempt" 
                                                                placeholder="Enter how many retake available" id="attemptQuiz"
                                                            >
                                                            <label class="my-2" for="attemptQuiz" >
                                                                Point Requirement
                                                            </label>
                                                            <input type="number" class="form-control form-dark" v-model="addquiz.point_requirement" 
                                                                placeholder="Enter the minimum point to pass" id="attemptQuiz"
                                                            >
                                                            <label class="my-2" for="QuizDesc">
                                                                Description:
                                                            </label>
                                                            <textarea class="form-control form-dark" v-model="addquiz.desc" 
                                                                placeholder="Write your quiz description here" id="QuizDesc"
                                                            ></textarea>
                                                            <input class="my-2 mt-4" type="checkbox" id="isUnlock" v-model="addquiz.is_unlock">
                                                            <label for="isUnlock" class="form-label">
                                                                Is Unlocked?
                                                            </label>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary outline-gold me-2" data-bs-dismiss="modal">
                                                                Cancel
                                                            </button>
                                                            <a href="javascript:void(0)" class="btn fill-gold fw-semi" @click="addQuiz(section.id)">
                                                                Save
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a class="btn btn-secondary d-inline-block fill-gray fw-semi mb-5" role="button"
                            data-bs-toggle="modal" data-bs-target="#sectionModal" v-if="!reorder"
                        >
                            Add Section
                        </a>
                        <!-- Modal -->
                        <div class="modal fade" id="sectionModal" tabindex="-1" aria-labelledby="sectionModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="sectionModalLabel">
                                            Add Section
                                        </h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        <div v-if="error_show3" class="alert alert-danger" role="alert">
                                            <div>
                                                <p class="color-red">
                                                    @{{ error_show3 }}
                                                </p>
                                            </div>                
                                        </div>
                                        <label for="CourseTitle" class="form-label">
                                            Section Title
                                        </label>
                                        <input type="text" v-model="section_title" class="form-control form-dark" placeholder="Insert section title here" id="SectionTitle">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary outline-gold me-2" data-bs-dismiss="modal">
                                            Cancel
                                        </button>
                                        <a class="btn  fill-gold fw-semi" @click="createSection('completed')">
                                            Save
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-container me-lg-4 mb-4">
                <div v-if="is_course_ready">
                    <div v-if="course.status == 'completed'">
                        <div class="text-center"> 
                            <a class="btn d-inline-block fill-gold fw-semi my-4" @click="updateCourse('completed',true)">
                                SAVE CHANGES
                            </a>
                        </div>
                    </div>
                    <div v-else>
                        <div class="text-center"> 
                            <a class="btn  d-inline-block fill-gold fw-semi my-4" data-bs-toggle="modal" data-bs-target="#saveModal">
                                PUBLISH COURSE
                            </a>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="saveModal" tabindex="-1" aria-labelledby="saveModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="saveModalLabel">
                                            Publish Course Confirmation
                                        </h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        <div v-if="error_show3" class="alert alert-danger" role="alert">
                                            <div>
                                                <p class="color-red">
                                                    @{{ error_show3 }}
                                                </p>
                                            </div>                
                                        </div>
                                        <p class="mb-3">
                                            Once you publish a course, you cannot change or edit it's content.
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary outline-gold me-2" data-bs-dismiss="modal">
                                            Cancel
                                        </button>
                                        <a class="btn fill-gold fw-semi" @click="updateCourse('completed')">
                                            Publish
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="hr-dark"></div>
                <div v-if="is_course_ready">
                    <div v-if="course.status != 'completed'">
                        <div class="py-3 px-3">
                            <button class="btn btn-text color-white" 
                                @click="updateCourse('draft')"
                            >
                                Save Draft
                            </button>
                        </div>
                        <div class="hr-dark"></div>
                    </div>
                    <div v-if="course.status == 'completed'">
                        <div class="py-3 px-3">
                            <button class="btn btn-text color-white" data-bs-toggle="modal" data-bs-target="#unpublishModal">
                                Unpublish Course
                            </button>
                            {{-- MODAL --}}
                            <div class="modal fade" id="unpublishModal" tabindex="-1" aria-labelledby="unpublishModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="unpublishModalLabel">
                                                Unpublish Course Confirmation
                                            </h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <p class="mb-3">
                                                Are you sure you want to unpublish this course?.
                                            </p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary outline-gold me-2" data-bs-dismiss="modal">
                                                Cancel
                                            </button>
                                            <a class="btn fill-gold fw-semi" @click="updateCourse('draft')">
                                                Unpublish
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="hr-dark"></div>
                    </div>
                </div>
                <div class="py-3 px-3">
                    <button class="btn btn-text color-red fw-semi"  data-bs-toggle="modal" data-bs-target="#trashModal">
                        Move to Trash
                    </button>
                    {{-- MODAL --}}
                    <div class="modal fade" id="trashModal" tabindex="-1" aria-labelledby="trashModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="trashModalLabel">
                                        Move to trash Confirmation
                                    </h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <p class="mb-3">
                                        Once you move a course to trash, you cannot view or edit it's content.
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary outline-gold me-2" data-bs-dismiss="modal">
                                        Cancel
                                    </button>
                                    <a class="btn fill-gold fw-semi" @click="updateCourse('archived')">
                                        Move to trash
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="hr-dark"></div>
                <div class="py-3 px-3"v-if="is_course_ready">
                    <a class="btn btn-text color-gold fw-semi" :href="'/course/'+ course.slug + '/preview'">
                        Preview
                    </a>
                </div>
            </div>
            <div class="d-flex align-items-center mb-4">
                <h6 class="color-gold me-3 mb-0" style="white-space: nowrap">
                    VIDEO
                </h6>
                <div class="hr me-lg-4"></div>
            </div>
            <div class="form-container me-lg-4 mb-4">
                <iframe 
                    width="100%" 
                    height="198" 
                    v-if="trailer"
                    :src="'https://youtube.com/embed/' + trailer"
                >
                </iframe>
                <vue-skeleton-loader
                    :height="198"
                    v-if="!is_course_ready"
                    class="w-100 mb-2"
                    color="rgba(52, 52, 52, 1)"
                    animation="fade"
                ></vue-skeleton-loader>
                <div class="py-3 px-3">
                    <label for="CourseVideo" class="form-label">
                        URL
                    </label>
                    <input class="form-control form-dark" id="CourseVideo" v-model="req.trailer" required>
                    <div id="videoHelp" class="form-text mb-2">
                        Enter a valid video URL (Youtube Video > Share > Copy Link)
                    </div>
                </div>
            </div>
            <div class="d-flex align-items-center mb-4">
                <h6 class="color-gold me-3 mb-0" style="white-space: nowrap">
                    OPTIONS
                </h6>
                <div class="hr me-lg-4"></div>
            </div>
    
            <div class="form-container me-lg-4 mb-4">
                <div class="py-3 px-3">
                    <label for="CourseCategory" class="form-label">
                        CATEGORY
                    </label>
                    <select class="form-select form-dark" id="CourseCategory" aria-label="CourseCategory" v-model="req.category_id" required>
                        <option v-for="category in categories" :value="category.id">@{{ category.name }}</option>
                    </select>
                    <div id="categoryHelp" class="form-text mb-2">
                        Select a Category
                    </div>
                </div>
                <div class="pb-3 px-3">
                    <label for="CourseLang" class="form-label">
                        LANGUAGE
                    </label>
                    <div v-if="is_course_ready">
                        <select v-if="course.status != 'completed'" v-model="req.language" class="form-select form-dark" 
                            id="CourseLang" aria-label="CourseLang" required
                        >
                            <option value="english">English</option>
                            <option value="Indonesia">Bahasa Indonesia</option>
                            <option value="korean">Korean</option>
                        </select>
                        <select v-else v-model="req.language" class="form-select form-dark" 
                            id="CourseLang" aria-label="CourseLang" disabled
                        >
                            <option value="english">English</option>
                            <option value="Indonesia">Bahasa Indonesia</option>
                            <option value="korean">Korean</option>
                        </select>
                    </div>
                    <div id="langHelp" class="form-text mb-2">
                        Select course language
                    </div>
                </div>
                <div class="pb-3 px-3">
                    <label for="CoursePrice" class="form-label">
                        PRICE
                    </label>
                    <div class="input-group" id="CoursePrice">
                        <span class="input-group-text">
                            Rp.
                        </span> 
                        <money v-model="req.price" class="form-control form-dark price" aria-label="Amount (to the nearest rupiah)" required> </money>
                        </div>
                    <div id="priceHelp" class="form-text mb-2">
                        The recommended price is between Rp. 50.000 and Rp. 300.000
                    </div>
                    <isdiscounted v-if="is_course_ready" :course="course" v-model="req.discount"></isdiscounted>
                </div>
                {{-- <div class="pb-3 px-3">
                    <label for="CourseTag" class="form-label">TAGS</label>
                    <input tag-bg="#424242" tag-text-color="white"
                    id="CourseTag "name='tags' value='PHP,Laravel' class="form-control form-dark" placeholder='Write some tags' 
                    autofocus>
                    <div id="tagHelp" class="form-text mb-2">Select one or more tags</div>
                </div> --}}
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
    <script>
        'use strict';
        var app = new Vue({
            el: '#app',
            data() {
                return {
                    error_show: null,
                    error_show2: null,
                    error_show3: null,
                    error_discount: null,
                    trailer: null,
                    categories: null,
                    attachment:{
                        hero_background:null,
                        thumbnail:null
                    },
                    hero_background:'',
                    thumbnail:'',
                    course: null,
                    is_course_ready: false,
                    is_editor_ready: false,
                    section_title:'',
                    quizzes: null,
                    addquiz: {
                        title:'',
                        max_attempt:'',
                        desc:'',
                        quiz_id:'',
                        is_unlock: 0,
                    },
                    req: {
                        thumbnail:'',
                        hero_background:'',
                        title: '',
                        price:'',
                        desc:'',
                        level:'',
                        estimated_time:'',
                        estimated_time_HH:'',
                        estimated_time_MM:'',
                        trailer:'',
                        language:'',
                        status:'',
                        point_requirement:'',
                        discount: {
                            is_on_discount:'',
                            discount_price:'',
                        },
                        category_id:'',
                    },
                    section_quiz: null,
                    embed_id: '',
                    section_edit_title: null,
                    title: null,
                    reorder: false,
                }
            },
            watch: {
                is_course_ready: function() {
                    var self = this
                    setTimeout(() => {
                        tinymce.init({
                            // init_instance_callback: function (editor) {
                            //     editor.on('load', function (e) {
                            //         self.is_editor_ready = true;
                            //     });
                            // },
                            selector: '#wysiwyg',
                            menubar : 'edit format insert',
                            height : "480",
                            skin: 'oxide-dark',
                            content_css: 'dark',
                            plugins: 'paste lists nonbreaking',
                            paste_as_text: true,
                            toolbar: 'numlist bullist',
                            nonbreaking_force_tab: true,
                        });
                        // The DOM element you wish to replace with Tagify
                        var input = document.querySelector('input[name=tags]');
                        
                        // initialize Tagify on the above input node reference
                        new Tagify(input);
                    }, 100);
                    
                }
            },
            mounted: function() {
                axios({
                    method: 'get',
                    url: this.api_url + '/api/v1/categories',
                    headers: {
                        'Authorization' : 'Bearer ' + bearer,
                        'Accept' : 'application/json',
                    },
                })
                .then(response => {
                    this.categories = response.data
                })
                .catch(function (error) {
                    if (error.response) {
                        //
                    }
                })

                axios({
                    method: 'get',
                    url: this.api_url + '/api/v1/teachers/{{session('data')['id']}}/courses/{{$course_id}}',
                    headers: {
                        'Authorization' : 'Bearer ' + bearer,
                        'Accept' : 'application/json',
                    },
                })
                .then(response => {
                    // this.course = response.data
                    this.req = response.data
                    this.req.estimated_time_HH = Math.floor(this.req.estimated_time / 3600)
                    this.req.estimated_time_MM = Math.floor((this.req.estimated_time - (this.req.estimated_time_HH * 3600))/60)
                    //anomaly
                    var course = new Object()
                    _.forEach(this.req, (value, key)=>{
                        course[key] = value
                    })

                    this.course = course

                    this.req.discount = {
                        is_on_discount: this.req.is_on_discount,
                        discount_price: this.req.discount_price
                    }
                    this.trailer = this.req.trailer
                    if(this.req.trailer  != null){
                        this.req.trailer = 'https://youtu.be/' + this.req.trailer 
                    }
                    
                    
                    this.hero_background = this.req.hero_background
                    this.thumbnail = this.req.thumbnail
                    this.is_course_ready = true

                })
                axios({
                    method: 'get',
                    url: this.api_url + '/api/v1/quizzes',
                    headers: {
                        'Authorization': 'Bearer ' + bearer,
                        'Accept': 'application/json',
                    }
                })
                .then(response => {
                    this.quizzes = response.data
                })
            },
            computed: {
                dragOptions() {
                return {
                    animation: 200,
                    group: "section",
                    disabled: false,
                    ghostClass: "ghost"
                };
                },
                dragOptionsHead() {
                return {
                    animation: 200,
                    group: "sectionhead",
                    disabled: false,
                    ghostClass: "ghost"
                };
                }
            },     
            methods: {
                createSection(status){
                    var self = this
                    this.showLoading()
                    axios({
                        method: 'post',
                        url: this.api_url + '/api/v1/courses/'+ {{ $course_id }} + '/sections',
                        headers: {
                            'Authorization' : 'Bearer ' + bearer,
                            'Accept' : 'application/json',
                        },
                        data: {
                            title: this.section_title,
                            status: status
                        }
                    })
                    .then(response => {
                        this.hideLoading()
                        window.location.href = "{{url()->current()}}"
                    })
                    .catch(function (error) {
                        if (error.response) {
                            self.error_show3 = "The title field is required"
                            window.location.href = '#'
                            self.hideLoading()
                        }
                    })
                },
                editSection(section_id){
                    this.showLoading()
                    var self = this
                    axios({
                        method: 'put',
                        url: this.api_url + '/api/v1/sections/' + section_id,
                        headers: {
                            'Authorization' : 'Bearer ' + bearer,
                            'Accept' : 'application/json',
                        },
                        data: {
                            title: this.section_edit_title,
                        }

                    })
                    .then(response => {
                        this.hideLoading()
                        window.location.href = "{{url()->current()}}"
                    })
                    .catch(function (error) {
                        if (error.response) {
                            self.error_show3 = "The title field is required"
                            window.location.href = '#'
                            self.hideLoading()
                        }
                    })
                },
                updateCourse(status,pivot=false){
                    var formData = new FormData()
                    var self = this
                    this.showLoading()
                    //ngecek discount
                    if(this.req.discount.discount_price >= this.req.price && this.req.discount.is_on_discount){
                        this.error_discount = "Discount price cant be higher than price"
                        window.location.href = '#'
                        self.hideLoading()
                    }
                    else{
                        //kondisi section tidak kosong
                        if(this.req.course_sections.length != 0 ){
                            if(this.req.course_sections[0].section_content_orders.length == 0 && status == 'completed'){
                                this.error_show3 = "Course must have at least 1 section and 1 part"
                                window.location.href = '#'
                                self.hideLoading()
                            }
                            else{
                                if(this.attachment.hero_background !=  null){
                                    formData.append("hero_background", this.attachment.hero_background)
                                }
                                if(this.attachment.thumbnail !=  null){
                                    formData.append("thumbnail", this.attachment.thumbnail)
                                }
                                // converting time to second
                                this.req.estimated_time_HH = this.req.estimated_time_HH * 3600
                                this.req.estimated_time_MM = this.req.estimated_time_MM * 60
                                this.req.estimated_time = parseInt(this.req.estimated_time_HH) + parseInt(this.req.estimated_time_MM)
            
                                formData.append('price', this.req.price)
                                formData.append('desc', tinyMCE.activeEditor.getContent())
                                formData.append('level', this.req.level)
                                formData.append('estimated_time', this.req.estimated_time)
                                formData.append('language', this.req.language)
                                formData.append('title', this.req.title)
                                formData.append('discount_price', this.req.discount.discount_price)
                                formData.append('status', status)
                                formData.append('category_id', this.req.category_id)
                                if(this.req.trailer != null){
                                    this.embed_id = this.req.trailer.replace('https://youtu.be/', '')
                                }
                                formData.append('trailer', this.embed_id)
                                axios({
                                    method: 'post',
                                    url: this.api_url + '/api/v1/courses/'+ {{ $course_id }},
                                    headers: {
                                        'Authorization' : 'Bearer ' + bearer,
                                        'Accept' : 'application/json',
                                    },
                                    data: formData,
                                })
                                .then(response => {
                                    if(this.req.course_sections.length != 0){
                                        axios({
                                            method: 'post',
                                            url: this.api_url + '/api/v1/courses/'+ {{ $course_id }},
                                            headers: {
                                                'Authorization' : 'Bearer ' + bearer,
                                                'Accept' : 'application/json',
                                            },
                                            data: {
                                                is_on_discount: this.req.discount.is_on_discount,
                                                section_orders: this.req.course_sections
                                            }
                                        })
                                        .then(response => {
                                            this.hideLoading()
                                            if (status == 'completed' || status == 'archived'){
                                                if(pivot == true){
                                                    window.location.href = "{{url()->current()}}"
                                                }
                                                else{
                                                    window.location.href = "/teacher/course"
                                                }
                                            }
                                            else{
                                                window.location.href = "{{url()->current()}}"
                                            }
                                        })
                                        .catch(function (error) {
                                            if (error.response) {
                                                self.error_show = error.response.data.errors
                                                window.location.href = '#'
                                                self.hideLoading()
                                            }
                                        })        
                                    }
                                    else{
                                        axios({
                                            method: 'post',
                                            url: this.api_url + '/api/v1/courses/'+ {{ $course_id }},
                                            headers: {
                                                'Authorization' : 'Bearer ' + bearer,
                                                'Accept' : 'application/json',
                                            },
                                            data: {
                                                is_on_discount: this.req.discount.is_on_discount,
                                            }
                                        })
                                        .then(response => {
                                            this.hideLoading()
                                            if (status == 'completed' || status == 'archived'){
                                                if(pivot == true){
                                                    window.location.href = "{{url()->current()}}"
                                                }
                                                else{
                                                    window.location.href = "/teacher/course"
                                                }
                                            }
                                            else{
                                                window.location.href = "{{url()->current()}}"
                                                console.log(response)
                                            }
                                        })
                                        .catch(function (error) {
                                            if (error.response) {
                                                self.error_show = error.response.data.errors
                                                window.location.href = '#'
                                                self.hideLoading()
                                            }
                                        })        
                                    }
                                })
                            }
                        }
                        //kondisi section kosong
                        else{
                            if(status == 'completed'){
                                this.error_show3 = "Course must have at least 1 section and 1 part"
                                window.location.href = '#'
                                self.hideLoading()
                            }
                            else{
                                if(this.attachment.hero_background !=  null){
                                    formData.append("hero_background", this.attachment.hero_background)
                                }
                                if(this.attachment.thumbnail !=  null){
                                    formData.append("thumbnail", this.attachment.thumbnail)
                                }
                                // converting time to second
                                this.req.estimated_time_HH = this.req.estimated_time_HH * 3600
                                this.req.estimated_time_MM = this.req.estimated_time_MM * 60
                                this.req.estimated_time = parseInt(this.req.estimated_time_HH) + parseInt(this.req.estimated_time_MM)
            
                                formData.append('price', this.req.price)
                                formData.append('desc', tinyMCE.activeEditor.getContent())
                                formData.append('level', this.req.level)
                                formData.append('estimated_time', this.req.estimated_time)
                                formData.append('language', this.req.language)
                                formData.append('title', this.req.title)
                                formData.append('discount_price', this.req.discount.discount_price)
                                formData.append('status', status)
                                formData.append('category_id', this.req.category_id)
                                if(this.req.trailer != null){
                                    this.embed_id = this.req.trailer.replace('https://youtu.be/', '')
                                }
                                formData.append('trailer', this.embed_id)
                                axios({
                                    method: 'post',
                                    url: this.api_url + '/api/v1/courses/'+ {{ $course_id }},
                                    headers: {
                                        'Authorization' : 'Bearer ' + bearer,
                                        'Accept' : 'application/json',
                                    },
                                    data: formData,
                                })
                                .then(response => {
                                    if(this.req.course_sections.length != 0){
                                        axios({
                                            method: 'post',
                                            url: this.api_url + '/api/v1/courses/'+ {{ $course_id }},
                                            headers: {
                                                'Authorization' : 'Bearer ' + bearer,
                                                'Accept' : 'application/json',
                                            },
                                            data: {
                                                is_on_discount: this.req.discount.is_on_discount,
                                                section_orders: this.req.course_sections
                                            }
                                        })
                                        .then(response => {
                                            this.hideLoading()
                                            if (status == 'completed' || status == 'archived'){
                                                if(pivot == true){
                                                    window.location.href = "{{url()->current()}}"
                                                }
                                                else{
                                                    window.location.href = "/teacher/course"
                                                }
                                            }
                                            else{
                                                window.location.href = "{{url()->current()}}"
                                                console.log(response)
                                            }
                                        })
                                        .catch(function (error) {
                                            if (error.response) {
                                                self.error_show = error.response.data.errors
                                                window.location.href = '#'
                                                self.hideLoading()
                                            }
                                        })        
                                    }
                                    else{
                                        axios({
                                            method: 'post',
                                            url: this.api_url + '/api/v1/courses/'+ {{ $course_id }},
                                            headers: {
                                                'Authorization' : 'Bearer ' + bearer,
                                                'Accept' : 'application/json',
                                            },
                                            data: {
                                                is_on_discount: this.req.discount.is_on_discount,
                                            }
                                        })
                                        .then(response => {
                                            this.hideLoading()
                                            if (status == 'completed' || status == 'archived'){
                                                if(pivot == true){
                                                    window.location.href = "{{url()->current()}}"
                                                }
                                                else{
                                                    window.location.href = "/teacher/course"
                                                }
                                            }
                                            else{
                                                window.location.href = "{{url()->current()}}"
                                            }
                                        })
                                        .catch(function (error) {
                                            if (error.response) {
                                                self.error_show = error.response.data.errors
                                                window.location.href = '#'
                                                self.hideLoading()
                                            }
                                        })        
                                    }
                                })
                            }
                        } 
                    }   
                },
                addQuiz(section_id){
                    this.showLoading()
                    var self = this
                    axios({
                        method: 'post',
                        url: this.api_url + '/api/v1/sections/'+ section_id + '/quizzes',
                        headers: {
                            'Authorization': 'Bearer ' + bearer,
                            'Accept': 'application/json',
                        },
                        data: {
                                desc: this.addquiz.desc,
                                max_attempt: this.addquiz.max_attempt,
                                title: this.addquiz.title,
                                quiz_id: this.addquiz.quiz_id,
                                point_requirement: this.addquiz.point_requirement,
                                is_unlock: this.addquiz.is_unlock,
                            }
                        })
                    .then(response => {
                        this.hideLoading()
                        window.location.href = "{{url()->current()}}"
                    })
                    .catch(function (error) {
                        if (error.response) {
                            self.error_show = error.response.data.errors
                            window.location.href = '#'
                            self.hideLoading()
                        }
                    })
                },
                updateQuiz(quiz_id){
                    this.showLoading()
                    var self = this
                    axios({
                        method: 'put',
                        url: this.api_url + '/api/v1/section-quizzes/' + quiz_id,
                        headers: {
                            'Authorization': 'Bearer ' + bearer,
                            'Accept': 'application/json',
                        },
                        data: {
                                desc: this.section_quiz.desc,
                                max_attempt: this.section_quiz.max_attempt,
                                title: this.section_quiz.title,
                                point_requirement: this.section_quiz.point_requirement,
                                is_unlock: this.section_quiz.is_unlock,
                            }
                        })
                    .then(response => {
                        window.location.href = "{{url()->current()}}"
                        this.hideLoading()
                    })
                    .catch(function (error) {
                        if (error.response) {
                            self.hideLoading()
                        }
                    })
                },
                getSectionQuiz(quiz_id,section_id){
                    this.showLoading()
                    var self = this
                    axios({
                        method: 'get',
                        url: this.api_url + '/api/v1/section-quizzes/' + quiz_id,
                        headers: {
                            'Authorization': 'Bearer ' + bearer,
                            'Accept': 'application/json',
                        }
                    })
                    .then(response => {
                        this.section_quiz = response.data
                        setTimeout(() => {
                            resultModal(section_id)
                        }, 1000);
                        this.hideLoading()
                    })
                    .catch(function (error) {
                        if (error.response) {
                            self.hideLoading()
                        }
                    })
                },
                deleteSectionQuiz(quiz_id){
                    this.showLoading()
                    var self = this
                    axios({
                        method: 'delete',
                        url: this.api_url + '/api/v1/section-quizzes/' + quiz_id,
                        headers: {
                            'Authorization': 'Bearer ' + bearer,
                            'Accept': 'application/json',
                            'Content-Type' : 'multipart/form-data',
                        }
                    })
                    .then(response => {
                        this.hideLoading()
                        window.location.href = "{{url()->current()}}"
                    })
                    .catch(function (error) {
                        if (error.response) {
                            self.hideLoading()
                        }
                    })
                },
                deletePart(part_id){
                    this.showLoading()
                    var self = this
                    axios({
                        method: 'delete',
                        url: this.api_url + '/api/v1/parts/' + part_id,
                        headers: {
                            'Authorization': 'Bearer ' + bearer,
                            'Accept': 'application/json',
                        }
                    })
                    .then(response => {
                        this.hideLoading()
                        window.location.href = "{{url()->current()}}"
                    })
                    .catch(function (error) {
                        if (error.response) {
                            self.hideLoading()
                        }
                    })
                },
                deleteSection(section_id){
                    this.showLoading()
                    var self = this
                    axios({
                        method: 'delete',
                        url: this.api_url + '/api/v1/sections/' + section_id,
                        headers: {
                            'Authorization': 'Bearer ' + bearer,
                            'Accept': 'application/json',
                        }
                    })
                    .then(response => {
                        this.hideLoading()
                        window.location.href = '{{url()->current()}}'
                    })
                    .catch(function (error) {
                        if (error.response) {
                            self.hideLoading()
                        }
                    })
                },
                uploadHero(event){
                    if(event.target.files[0].size > 1048576){
                        window.location.href = "#"
                        this.error_show2 = "File is too big! Max size is 1MB"
                        event.target.files[0] = null;
                        this.attachment.hero.background = '';
                    }
                    else{
                        this.attachment.hero_background = event.target.files[0]
                    }
                
                },
                uploadThumbnail(event){
                    if(event.target.files[0].size > 1048576){
                        window.location.href = "#"
                        this.error_show2 = "File is too big! Max size is 1MB"
                        event.target.files[0] = null;
                        this.attachment.thumbnail = '';
                    }
                    else{
                        this.attachment.thumbnail = event.target.files[0]
                    }
                
                },
                assignSectionTitle(title){
                    this.section_edit_title = title
                },
                reorderSwitch(mode){
                    this.reorder = mode
                }
            },
        })
    </script>
    <script>
        function resultModal(section_id){
            var myModal = document.getElementById('quizEditModal' + section_id);
            var modal = bootstrap.Modal.getOrCreateInstance(myModal)
            modal.show()
        }
    </script>
@endsection