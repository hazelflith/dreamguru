@extends('layout.default')

@section('meta')
    @component('components.meta')
        @slot('title')
             View Quiz
        @endslot

        @slot('description')
            Dreamaxtion helps companies and people maximize potential through Human Capital management system and psychometric assessment for personal and career growth.
        @endslot
    @endcomponent
@endsection

@section('css')
    <style> 
        
    </style>
@endsection

@section('content')
<div class="main-padding-h main-padding-v" v-cloak>
    <h3 class="color-gold fw-semi">
        View Quiz
    </h3>
    <h6 class="color-muted" v-if="is_quiz_ready">
        @{{questions.title}}> VIEW
    </h6>
</div>
<div class="row g-0" v-cloak>
    <div class="col-8">
        <div class="main-padding-h">
            <div v-if="error_show" class="alert alert-danger" role="alert">
                <div v-for="value in error_show">
                    <p class="color-red">
                        @{{value[0]}}
                    </p>
                </div>                
            </div>
            <div class="d-flex align-items-center mb-3">
                <h4 class="color-gold fw-semi me-auto" v-if="is_quiz_ready">
                    @{{questions.title}}
                </h3>
                <h6 class="color-muted text-capitalize" v-if="is_quiz_ready">
                    Status: @{{questions.status}}
                </h6>
            </div>
            
            <div class="d-flex align-items-center">
                <h6 class="color-gold me-3 mb-0" style="white-space: nowrap">
                    QUESTIONS
                </h6>
                <div class="hr"></div>
            </div>
            <div class="mb-3"></div>
            <div class="question-wrap">
                <div v-if="questions.questions.length == 0">
                    <p>
                        No questions added
                    </p>
                </div>
                <div v-for="(question , index) in questions.questions" :key="question.id">
                    <div class="d-flex">
                        <h5 class="mb-1 color-gold">
                            Question @{{index + 1}}
                        </h5>
                        <h5 class="color-gold ms-auto me-3">
                            Point: @{{question.point}}
                        </h5>
                    </div>
                    <p v-if="question.question_type == 'single_answer'" class="mb-1">
                        ANSWERS (Single Answer) :
                    </p>
                    <p v-else class="mb-1">
                        ANSWERS (Multiple Answer) :
                    </p>
                    <ul>
                        <li  v-for="answer in question.answers" :key="answer.id">
                            @{{answer.answer}} 
                            <i v-if="answer.is_true" class="fa-solid fa-circle-check ms-2"></i>
                        </li>
                    </ul>
                    <div class="hr mb-3"></div>
                </div>
            </div>
            <div class="mb-3"></div>
        </div>
    </div>
    <div class="col-4">
        <div class="form-container me-5 mb-4">
            <div class="py-3 px-3 text-center">
                <a href="javascript:void(0)" class="fw-semi btn fill-gold fw-semi" @click="updateQuiz('completed')">
                    Complete Quiz
                </a>
            </div>
            <div v-if="questions.status == 'completed'" class="hr-dark"></div>
            <div v-if="questions.status == 'completed'" class="py-3 px-3">
                <a href="javascript:void(0)" class="fw-semi" @click="updateQuiz('draft')">
                    Unpublish Quiz
                </a>
            </div>
            <div class="hr-dark"></div>
            <div class="py-3 px-3">
                <a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#deleteQuizModal" class="color-red fw-semi">
                    Delete Master Quiz
                </a>
            </div>
            {{-- MODAL--}}
            <div class="modal fade" id="deleteQuizModal" tabindex="-1" aria-labelledby="deleteQuizModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="deleteQuizModalLabel">
                                Delete Master Quiz Confirmation
                            </h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <p class="mb-3">
                                Once you delete a master quiz, you cannot view or edit it's content.
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary outline-gold me-2" data-bs-dismiss="modal">
                                Cancel
                            </button>
                            <a class="btn fill-gold fw-semi" @click="updateQuiz('archived')">
                                Delete Master Quiz
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
    <script>
        'use strict';
        var app = new Vue({
            el: '#app',

            data() {
                return {
                    is_quiz_ready: false,
                    questions: [],
                    error_show: null,
                }
            },
            watch: {
                is_quiz_ready: function() {
                    
                    setTimeout(() => {
                        tinymce.init({
                            selector: '#wysiwyg',
                            menubar : 'edit format insert',
                            height : "480",
                            skin: 'oxide-dark',
                            content_css: 'dark',
                            plugins: 'paste lists nonbreaking',
                            paste_as_text: true,
                            toolbar: 'numlist bullist',
                            nonbreaking_force_tab: true
                        });
                        // The DOM element you wish to replace with Tagify
                        var input = document.querySelector('input[name=tags]');
                        
                        // initialize Tagify on the above input node reference
                        new Tagify(input)
                    }, 1000);
                }
            },
            mounted: function() {

                axios({
                    method: 'get',
                    url: this.api_url + '/api/v1/quizzes/'+ {{ $quiz_id }},
                    headers: {
                        'Authorization' : 'Bearer ' + bearer,
                        'Accept' : 'application/json',
                    },
                })
                .then(response => {
                    this.questions = response.data
                    this.is_quiz_ready = true
                })
            },
            methods: {
                updateQuiz(status){
                    this.showLoading()
                    var self = this
                    axios({
                        method: 'put',
                        url: this.api_url + '/api/v1/quizzes/'+ {{ $quiz_id }},
                        headers: {
                            'Authorization' : 'Bearer ' + bearer,
                            'Accept' : 'application/json',
                        },
                        data: {
                            status: status
                        }

                    })
                    .then(response => {
                        this.hideLoading()
                        window.location.href = "/teacher/quiz"
                    })
                    .catch(function (error) {
                        if (error.response) {
                            self.error_show = error.response.data.errors
                            window.location.href = '#'
                            self.hideLoading()
                        }
                    })
                }
            },
        })
    </script>
@endsection