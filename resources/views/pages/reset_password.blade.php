@extends('layout.defaultreset')

@section('meta')
    @component('components.meta')
        @slot('title')
             Dreamguru | Reset Password
        @endslot

        @slot('description')
            Dreamaxtion helps companies and people maximize potential through Human Capital management system and psychometric assessment for personal and career growth.
        @endslot
    @endcomponent
@endsection

@section('css')

@endsection

@section('content')
<div class="container" v-cloak>
    <div class="row main-padding-v justify-content-center">
        <div class="col-md-6 col-xl-4 col-10">
            <h3 class="color-gold fw-semi text-center">
                Reset Password
            </h3> 
            <h6 class="color-muted mb-5 text-center">
                CHANGE YOUR OLD PASSWORD
            </h6>
            <div v-if="error_show" class="alert alert-danger" role="alert">
                <div v-for="value in error_show">
                    <p class="color-red">
                        @{{ value[0] }}
                    </p>
                </div>                
            </div>
            <div v-if="success_show" class="alert alert-success" role="alert">
                <p v-html='success_show' class="color-green">
                </p>
            </div>
            <p class="control-label">Email : @{{ email }}</p>
            <div class="d-flex align-items-center pt-2 mb-3">
                <div class="hr"></div>
            </div>
            <div>
               
                <div class="form-group mb-2">
                    <label class=" control-label">
                        Enter Your New Password:
                    </label>
                    <div class="input-group">
                        <input id="password" type="password" class="form-control" v-model="new_password" minlength="8" required>
                        <span class="input-group-text-blend input-group-text" id="basic-addon1" @click="toggle1()">
                            <i class="fa-solid fa-eye color-white" id="togglePassword" style="cursor: pointer; width: 24px"></i>
                        </span>
                    </div>
                </div>
                <div class="form-group mb-2">
                    <label class=" control-label">
                        Confirm New Password:
                    </label>
                    <div class="input-group">
                        <input class="form-control" id="confirm_password" type="password" v-model="new_password_confirmation" required>
                        <span class="input-group-text-blend input-group-text" id="basic-addon2" @click="toggle2()">
                            <i class="fa-solid fa-eye color-white" id="togglePassword2" style="cursor: pointer; width: 24px"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group mb-2 mt-3">
                <div class="">
                    <a class="btn fill-gold fw-semi me-2" @click="resetPassword()">Reset Password</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    
</script>
<script>
    'use strict';
    var app = new Vue({
        el: '#app',

        data() {
            return {
                email: '{{Request::get('email')}}' ,
                new_password:'',
                new_password_confirmation:'',
                verified: false,
                error_show: null,
                success_show: null,
                ready: false,
                isToggled1: false,
            }
        },
        mounted: function() {

        },
        methods: {
            toggle1() { 
                const type = password.getAttribute("type") === "password" ? "text" : "password";
                password.setAttribute("type", type);

                // toggle the eye icon
                this.$el.querySelector('#togglePassword').classList.toggle('fa-eye-slash');
            },
            toggle2() { 
                const type = confirm_password.getAttribute("type") === "password" ? "text" : "password";
                confirm_password.setAttribute("type", type);

                // toggle the eye icon
                this.$el.querySelector('#togglePassword2').classList.toggle('fa-eye-slash');
            },
            resetPassword(){
                var self = this
                axios({
                    method: 'post',
                    url: this.api_url + '/api/v1/auth/reset-password/{{$token}}',
                    headers: {
                        'Accept' : 'application/json',
                    },
                    data: {
                        password: this.new_password,
                        password_confirmation: this.new_password_confirmation,
                        email: this.email
                    }
                })
                .then(response => {
                    self.success_show = 'Reset password success, redirecting to login page...<br>Click <a class="color-green" style="text-decoration: underline" href="/login">here</a> if your are not redirected automatically'
                    setTimeout(() => {
                        window.location.href = "/login"
                    }, 5000)
                })
                .catch(function (error) {
                    if (error.response) {
                        if(!error.response.data.errors){
                            self.error_show = [[error.response.data.message]]
                        }
                        else{
                            self.error_show = error.response.data.errors
                        }
                    }
                })
            }

        },
    })
</script>
@endsection