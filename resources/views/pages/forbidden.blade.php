@extends('layout.default')

@section('meta')
    @component('components.meta')
    
        @slot('title')
             Dreamguru
        @endslot

        @slot('description')
            Dreamaxtion helps companies and people maximize potential through Human Capital management system and psychometric assessment for personal and career growth.
        @endslot
    @endcomponent
@endsection

@section('css')
    <style> 
        
    </style>
@endsection

@section('content')
<div class="forbidden text-center main-padding-v">
    <div class="color-gold heading-forbidden">
        403 Forbidden
    </div>
    <h1 class="color-gold">
        You are not authorized to view this page
    </h1>
    <a href="javascript:history.back()" class="btn fill-gold fw-semi px-5 my-5">
        Go Back
    </a>
</div>

@endsection

@section('js')
    <script>
        'use strict';
        var app = new Vue({
            el: '#app',
        })
    </script>
@endsection