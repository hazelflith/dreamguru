@extends('layout.default')

@section('meta')
    <title>Redirecting...</title>
@endsection

@section('css')
    <style> 
        
    </style>
@endsection

@section('content')
<div class="redirect-container">
    <div class="payment-redirect text-center ">
        <img src="/img/paymentbg.png" alt="paymentbg" width="35%">
        <h1 class="color-gold fw-semi">
            Processing Payment
        </h1>
        <h4 class="color-white ">
            Redirecting to Payment Page...
        </h4>
    </div>
</div>

@endsection

@section('js')
    <script>
        'use strict';
        var app = new Vue({
            el: '#app',
        })
    </script>
    <script>
        var redirect_url = "{{Request::get('redirect')}}";
        // callback functions
        var options = {
            performAuthentication: function(redirect_url){
                // Implement how you will open iframe to display 3ds authentication redirect_url to customer
                MidtransNew3ds.redirect( redirect_url, { callbackUrl : 'http://dreamguru.me/api/card/checkout/status' });
            },
            onSuccess: function(response){
                // 3ds authentication success, implement payment success scenario
            
                popupModal.closePopup();
            },
            onFailure: function(response){
                // 3ds authentication failure, implement payment failure scenario
             
                popupModal.closePopup();
            },
            onPending: function(response){
                // transaction is pending, transaction result will be notified later via 
                // HTTP POST notification, implement as you wish here
          
                popupModal.closePopup();
            }
        };
        
        // trigger `authenticate` function
        MidtransNew3ds.authenticate(redirect_url, options);
  
    </script>
@endsection