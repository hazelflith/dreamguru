@extends('layout.default')

@section('meta')
    @component('components.meta')
        @slot('title')
             Dreamguru | Register
        @endslot

        @slot('description')
            Dreamaxtion helps companies and people maximize potential through Human Capital management system and psychometric assessment for personal and career growth.
        @endslot
    @endcomponent
@endsection

@section('css')
    <style> 
        
    </style>
@endsection

@section('content')
<div class="container">
  <div class="row main-padding-v justify-content-center">
        <div class="col-md-6 col-xl-4 col-10">
            <div class="py-3 text-center" id="logo">
                <img class="" src="/img/dreamguru.png" alt="logo" width="100%">
            </div>
            <h1 class="py-2 color-gold fw-semi text-center mb-4">
                Register
            </h1>
            @if(Session::has("errors"))
                <div class="alert alert-danger" role="alert">
                    @foreach(Session::get('errors') as $message)
                        <p class="color-red">
                            {{ucfirst($message[0])}}
                        </p>
                    @endforeach
                </div>
            @endif
            <form action="" method="post">
                @csrf
                <!-- Email input -->
                <div class="form-outline mb-4">
                    <label class="form-label" for="form2Example1">
                        Username
                    </label>
                    <input type="text" id="form2Example1" class="form-control" name="username" placeholder="Input your username" value="{{old('username')}}" required>
                </div>
                <div class="form-outline mb-4">
                    <label class="form-label" for="form2Example2">
                        Full Name
                    </label>
                    <input type="text" id="form2Example2" class="form-control" name="name" placeholder="Input your full name" value="{{old('name')}}" required>
                </div>
                <div class="form-outline mb-4">
                    <label class="form-label" for="form2Example3">
                        Phone Number
                    </label>
                    <input type="number" id="form2Example3" class="form-control" name="phone_number" placeholder="Input your phone number" value="{{old('phone_number')}}" required>
                </div>
                <div class="form-outline mb-4">
                    <label>Date of Birth</label>
                    <fieldset>
                        <div class="field moveindate">
                          <input class="datepicker form-control" id="form2Example4"  name="date_of_birth" type="text" placeholder="yyyy-mm-dd" value="{{old('date_of_birth')}}" autofocus required>
                        </div>
                    </fieldset>
                </div>
                <div class="form-outline mb-4">
                    <label class="form-label" for="form2Example5">
                        Gender
                    </label>
                    <select class="form-select" id="form2Example5" name="gender" value="{{old('gender')}}" required>
                        <option selected value="male">Male</option>
                        <option value="female">Female</option>
                    </select>
                </div>
        
                <div class="form-outline mb-4">
                    <label class="form-label" for="form2Example6">
                        Email address
                    </label>
                <input type="email" id="form2Example6" class="form-control" name="email" placeholder="Input your email" value="{{old('email')}}" required>
                </div>
            
                <!-- Password input -->
                <div class="form-outline mb-4">
                    <label class="form-label" for="form2Example7">
                        Password
                    </label>
                    <div class="input-group">
                        <input type="password" id="password" name="password" id="form2Example2" class="form-control" placeholder="At least 8 characters" minlength="8" required>
                        <span class="input-group-text-blend input-group-text" id="basic-addon1">
                            <i class="fa-solid fa-eye color-white" id="togglePassword" style="cursor: pointer; width: 24px"></i>
                        </span>
                    </div>
                </div>
        
                <div class="form-outline mb-4">
                    <label class="form-label" for="form2Example8">
                        Confirm Password
                    </label>
                <input type="password" id="form2Example8" class="form-control" name="password_confirmation" placeholder="Write your password again" minlength="8" required/>
            </div>

            <div class="d-flex justify-content-center mb-3 g-recaptcha" data-sitekey="6Ldngp8fAAAAALZ6PLH3M2osc3PxRjiDBqNwvdd7" data-theme="dark"></div>
            
                <!-- Submit button -->
                <div class="row px-3"> 
                    <button type="submit" class="btn fill-gold fw-semi btn-block mb-4 ">
                        Register
                    </button>
                </div>
                <!-- Register buttons -->
                <div class="text-center">
                    <p>
                        Already a member? 
                        <a href="/login" class="color-gold">
                            Login
                        </a>
                    </p>
                    {{-- <p>
                    or register with:
                    </p>
                    <button type="button" class="btn btn-link color-gold btn-floating mx-1">
                        <i class="fab fa-facebook-f"></i>
                    </button>
                
                    <button type="button" class="btn btn-link color-gold btn-floating mx-1">
                        <i class="fab fa-google"></i>
                    </button>
                
                    <button type="button" class="btn btn-link color-gold btn-floating mx-1">
                        <i class="fab fa-twitter"></i>
                    </button>
                
                    <button type="button" class="btn btn-link color-gold btn-floating mx-1">
                        <i class="fab fa-github"></i>
                    </button> --}}
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script>
        'use strict';
        var app = new Vue({
            el: '#app',
        })
    </script>
    <script>
        const togglePassword = document.querySelector("#togglePassword");
        const password = document.querySelector("#password");

        togglePassword.addEventListener("click", function () {
            
            // toggle the type attribute
            const type = password.getAttribute("type") === "password" ? "text" : "password";
            password.setAttribute("type", type);

            // toggle the eye icon
            this.classList.toggle('fa-eye-slash');
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.datepicker').pickadate({
                selectMonths: true,
                selectYears: true,
                formatSubmit: 'yyyy-mm-dd',
                min: [1900,1,01],
                max: -6205,
                closeOnSelect: false,
                closeOnClear: false,
            });
        });
    </script>
@endsection