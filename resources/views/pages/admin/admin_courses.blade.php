@extends('layout.default')

@section('meta')
    @component('components.meta')
        @slot('title')
             Dreamguru | Admin Courses
        @endslot

        @slot('description')
            Dreamaxtion helps companies and people maximize potential through Human Capital management system and psychometric assessment for personal and career growth.
        @endslot
    @endcomponent
@endsection

@section('css')
    <style> 
        
    </style>
@endsection

@section('content')
<div class="main-padding-h main-padding-v" v-cloak>
    <h3 class="color-gold fw-semi">
        Courses List
    </h3>
    <h6  class="color-muted">
        MANAGE COURSES
    </h6>
    <div v-if="completion_message" class="alert alert-success mt-3" role="alert">
        <div v-if="completion_message">
            <p class="color-green">
                @{{ completion_message }}
            </p>
        </div>
    </div>
    <div class="d-flex align-items-center pt-4 mb-3">
        <h6 class="color-gold me-3 mb-0" style="white-space: nowrap">
            PUBLISHED COURSES
        </h6>
        <div class="hr"></div>
    </div>
    <div class="table-responsive-md">
        <table class="table mt-2">
            <tr>
                <th class="color-white">No</th>
                <th class="color-white">Name</th>
                <th class="color-white">ID</th>
                <th class="color-white">Actions</th>
            </tr>
            <template v-for="(course,index) in courses">
                <tr>
                    <td>
                        <p class="me-4">
                            @{{ index + 1 }}.
                        </p>
                    </td>
                    <td>
                        <p class="me-4">
                            @{{ course.title }}
                        </p>
                    </td>
                    <td>
                        <p class="me-4">
                            @{{ course.id }}
                        </p>
                    </td>
                    <td>
                        <a href="javascript:void(0)" class="color-red" @click="deleteCourses(course.id)">
                            <i class="fa-solid fa-trash-can"></i>
                        </a>
                    </td>
                </tr>
            </template>
        </table>
    </div>
</div>

@endsection

@section('js')
    <script>
       'use strict';
        var app = new Vue({
            el: '#app',

        data() {
            return {
                courses: null,
                completion_message: '',
            }
        },
        mounted: function() {
            axios({
                method: 'get',
                url: this.api_url + '/api/v1/admin/courses/completed',
                headers: {
                    'Authorization': 'Bearer ' + bearer,
                    'Accept': 'application/json',
                }
            })
            .then(response => {
                this.courses = response.data
                this.is_page_ready = true
            })
            .catch(function (error) {
                if (error.response) {
                    //
                }
            })
        },
        methods: {
            refreshData(){
                axios({
                    method: 'get',
                    url: this.api_url + '/api/v1/admin/courses/completed',
                    headers: {
                        'Authorization': 'Bearer ' + bearer,
                        'Accept': 'application/json',
                    }
                })
                .then(response => {
                    this.courses = response.data
                    this.is_page_ready = true
                })
                .catch(function (error) {
                    if (error.response) {
                        //
                    }
                })
            },
            deleteCourse(course_id){
                this.showLoading()
                var self = this
                axios({
                    method: 'delete',
                    url: this.api_url + '/api/v1/admin/courses/' + course_id,
                    headers: {
                        'Authorization' : 'Bearer ' + bearer,
                        'Accept' : 'application/json',
                    },
                })
                .then(response => {
                    this.hideLoading()
                    this.refreshData()
                    this.completion_message = "Course Deleted!"
                })
                .catch(function (error) {
                    if (error.response) {
                        //
                        self.hideLoading()
                    }
                })
            },
        },
        }) 
    </script>
@endsection