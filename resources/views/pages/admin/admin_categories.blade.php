@extends('layout.default')

@section('meta')
    @component('components.meta')
        @slot('title')
             Dreamguru | Admin
        @endslot

        @slot('description')
            Dreamaxtion helps companies and people maximize potential through Human Capital management system and psychometric assessment for personal and career growth.
        @endslot
    @endcomponent
@endsection

@section('css')
    <style> 
        
    </style>
@endsection

@section('content')
<div class="main-padding-h main-padding-v" v-if="is_page_ready" v-cloak>
    <h3 class="color-gold fw-semi">
        Categories
    </h3>
    <h6  class="color-muted">
        MANAGE CATEGORY
    </h6>
    <div v-if="completion_message" class="alert alert-success mt-3" role="alert">
        <div v-if="completion_message">
            <p class="color-green">
                @{{ completion_message }}
            </p>
        </div>
    </div>
    <div class="d-flex align-items-center mt-5 mb-4">
        <h6 class="color-gold me-3 mb-0" style="white-space: nowrap">
            ANALYTICS & CATEGORY
        </h6>
        <div class="hr me-5"></div>
    </div>
    <div class="row">
        <div class="form-container col">
            <div class="d-flex align-items-center ms-3 my-3">
                <h6 class="color-gold me-3 mb-0" style="white-space: nowrap">
                    CATEGORIES
                </h6>
                <a  class="btn d-inline-block fill-gold fw-semi mx-2" role="button"
                    data-bs-toggle="modal" style="white-space: nowrap" data-bs-target="#categoryModal"
                >
                    Add Category
                </a>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="categoryModal" tabindex="-1" aria-labelledby="categoryModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="categoryModalLabel">Add Category</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <label for="CategoryName" class="form-label">Category Name</label>
                            <input type="text" v-model="add.name" class="form-control form-dark" placeholder="Insert category name here" id="categoryTitle"> 
                        </div>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-secondary outline-gold me-2" data-bs-dismiss="modal">Cancel</button>
                            <a class="btn  fill-gold fw-semi" @click="createCategory('completed')">Save</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive-md">
                <table class="table mt-2">
                    <tr>
                        <th class="color-white">No</th>
                        <th class="color-white">Name</th>
                        <th class="color-white">Actions</th>
                    </tr>
                    <template v-for="(category,index) in categories">
                        <tr>
                            <td>
                                <p class="me-4">
                                    @{{index + 1}}.
                                </p>
                            </td>
                            <td>
                                <p class="me-4">
                                    @{{ category.name }}
                                </p>
                            </td>
                            <td>
                                <a href="javascript:void(0)" role="button"
                                data-bs-toggle="modal" :data-bs-target="'#editCategoryModal' +index" class="color-gold">
                                    <i class="fa-solid fa-pen-to-square"></i>
                                </a>
                                <!-- Modal -->
                                <div class="modal fade" :id="'editCategoryModal' +index" tabindex="-1" aria-labelledby="editCategoryModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="editCategoryModalLabel">
                                                    Edit Category
                                                </h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <label for="CategoryName" class="form-label">
                                                    Category Name
                                                </label>
                                                <input type="text" v-model="category.name" class="form-control form-dark" placeholder="Insert category name here" id="categoryTitle">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary outline-gold me-2" data-bs-dismiss="modal">
                                                    Cancel
                                                </button>
                                                <a class="btn  fill-gold fw-semi" @click="updateCategory(index,category.id)">
                                                    Save
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a href="javascript:void(0)" class="color-red" @click="deleteCategory(category.id)">
                                    <i class="fa-solid fa-trash-can"></i>
                                </a>
                            </td>
                        </tr>
                    </template>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
    <script>
       'use strict';
        var app = new Vue({
            el: '#app',

        data() {
            return {

                categories: null,
                is_page_ready: false,
                add:{
                    name:'',
                },
                completion_message: '',
            }
        },
        mounted: function() {
            axios({
                method: 'get',
                url: this.api_url + '/api/v1/admin/users/student',
                headers: {
                    'Authorization': 'Bearer ' + bearer,
                    'Accept': 'application/json',
                }
            })
            .then(response => {
                this.students = response.data
            })
            .catch(function (error) {
                if (error.response) {
                    //
                }
            })
            axios({
                method: 'get',
                url: this.api_url + '/api/v1/admin/users/teacher',
                headers: {
                    'Authorization': 'Bearer ' + bearer,
                    'Accept': 'application/json',
                }
            })
            .then(response => {
                this.teachers = response.data
            })
            .catch(function (error) {
                if (error.response) {
                    //
                }
            })
            axios({
                method: 'get',
                url: this.api_url + '/api/v1/categories',
                headers: {
                    'Authorization' : 'Bearer ' + bearer,
                    'Accept' : 'application/json',
                },
            })
            .then(response => {
                this.categories = response.data
                this.is_page_ready = true
            })
            .catch(function (error) {
                if (error.response) {
                    //
                }
            })
            },
            methods: {
                refreshData(){
                    axios({
                        method: 'get',
                        url: this.api_url + '/api/v1/categories',
                        headers: {
                            'Authorization' : 'Bearer ' + bearer,
                            'Accept' : 'application/json',
                        },
                    })
                    .then(response => {
                        this.categories = response.data
                    })
                    .catch(function (error) {
                        if (error.response) {
                            //
                        }
            })
                },
                appointTeacher(role,user_id){
                    this.showLoading()
                    var self = this
                    axios({
                        method: 'put',
                        url: this.api_url + '/api/v1/admin/users/'+ user_id + '/role',
                        headers: {
                            'Authorization' : 'Bearer ' + bearer,
                            'Accept' : 'application/json',
                        },
                        data: {
                            role : role
                        }
                    })
                    .then(response => {
                        this.hideLoading()
                        this.refreshData()
                        this.completion_message = "Data Updated!"
                    })
                    .catch(function (error) {
                        if (error.response) {
                            //
                            self.hideLoading()
                        }
                    })
                },
                createCategory(){
                    this.showLoading()
                    var self = this
                    axios({
                        method: 'post',
                        url: this.api_url + '/api/v1/categories',
                        headers: {
                            'Authorization' : 'Bearer ' + bearer,
                            'Accept' : 'application/json',
                        },
                        data : {
                            name: this.add.name
                        }
                    })
                    .then(response => {
                        this.hideLoading()
                        window.location.href = "{{url()->current()}}"
                    })
                    .catch(function (error) {
                        if (error.response) {
                            //
                            self.hideLoading()
                        }
                    })
                },
                updateCategory(index,category_id){
                    this.showLoading()
                    var self = this
                    axios({
                        method: 'put',
                        url: this.api_url + '/api/v1/categories/' + category_id,
                        headers: {
                            'Authorization' : 'Bearer ' + bearer,
                            'Accept' : 'application/json',
                        },
                        data : {
                            name: this.categories[index].name
                        }
                    })
                    .then(response => {
                        this.hideLoading()
                        window.location.href = "{{url()->current()}}"
                    })
                    .catch(function (error) {
                        if (error.response) {
                            //
                            self.hideLoading()
                        }
                    })
                },
                deleteCategory(category_id){
                    this.showLoading()
                    var self = this
                    axios({
                        method: 'delete',
                        url: this.api_url + '/api/v1/categories/' + category_id,
                        headers: {
                            'Authorization' : 'Bearer ' + bearer,
                            'Accept' : 'application/json',
                        },
                    })
                    .then(response => {
                        this.hideLoading()
                        window.location.href = "{{url()->current()}}"
                    })
                    .catch(function (error) {
                        if (error.response) {
                            //
                            self.hideLoading()
                        }
                    })
                },
            },
        }) 
    </script>
@endsection