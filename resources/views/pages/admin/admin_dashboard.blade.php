@extends('layout.default')

@section('meta')
    @component('components.meta')
        @slot('title')
             Dreamguru | Admin
        @endslot

        @slot('description')
            Dreamaxtion helps companies and people maximize potential through Human Capital management system and psychometric assessment for personal and career growth.
        @endslot
    @endcomponent
@endsection

@section('css')
    <style> 
        
    </style>
@endsection

@section('content')
<div class="main-padding-h main-padding-v">
    <h3 class="color-gold fw-semi">
        Admin Dashboard
    </h3>
    <h6  class="color-muted">
        SYSTEM EVALUATION, USER MANAGEMENT, MANAGE CATEGORY
    </h6>

    <div class="d-flex align-items-center pt-4 mb-3">
        <h6 class="color-gold me-3 mb-0" style="white-space: nowrap">
            Under Development
        </h6>
        <div class="hr"></div>
    </div>
</div>

@endsection

@section('js')
    <script>
       'use strict';
        var app = new Vue({
            el: '#app',

        data() {
            return {

            }
        },
        mounted: function() {

        }
        }) 
    </script>
@endsection