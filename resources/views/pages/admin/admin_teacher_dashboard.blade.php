@extends('layout.default')

@section('meta')
    @component('components.meta')
        @slot('title')
             Dreamguru | Admin
        @endslot

        @slot('description')
            Dreamaxtion helps companies and people maximize potential through Human Capital management system and psychometric assessment for personal and career growth.
        @endslot
    @endcomponent
@endsection

@section('css')
    <style> 
        
    </style>
@endsection

@section('content')
<div class="main-padding-h main-padding-v" v-if="is_page_ready"  v-cloak>
    <h3 class="color-gold fw-semi">
        User Management
    </h3>
    <h6  class="color-muted">
        USER MANAGEMENT > GURUS
    </h6>
    <div v-if="completion_message" class="alert alert-success mt-3" role="alert">
        <div v-if="completion_message">
            <p class="color-green">
                @{{ completion_message }}
            </p>
        </div>
    </div>
    <div class="d-flex align-items-center pt-4 mb-3">
        <h6 class="color-gold me-3 mb-0" style="white-space: nowrap">
            GURUS
        </h6>
        <div class="hr"></div>
    </div>
    <div class="table-responsive-md">
        <table class="table mt-2">
            <tr>
                <th class="color-white">No</th>
                <th class="color-white">Name</th>
                <th class="color-white">ID</th>
                <th class="color-white">Actions</th>
            </tr>
            <template v-for="(teacher,index) in teachers">
                <tr>
                    <td>
                        <p class="me-4">
                            @{{ index + 1 }}.
                        </p>
                    </td>
                    <td>
                        <p class="me-4">
                            @{{ teacher.profile.name }}
                        </p>
                    </td>
                    <td>
                        <p class="me-4">
                            @{{ teacher.id }}
                        </p>
                    </td>
                    <td>
                        <a href="javascript:void(0)" class="fw-semi color-gold me-2"  @click="appointTeacher('student',teacher.id)" >
                            Revoke Access
                        </a>
                        <a href="javascript:void(0)" class="fw-semi color-gold"  @click="appointTeacher('admin',teacher.id)" >
                            Make Admin
                        </a>
                    </td>
                </tr>
            </template>
        </table>
    </div>
    <div class="form-container col-6">

    </div>
</div>

@endsection

@section('js')
    <script>
       'use strict';
        var app = new Vue({
            el: '#app',

        data() {
            return {
                teachers: null,
                is_page_ready: false,
                completion_message: '',
            }
        },
        mounted: function() {
            axios({
                method: 'get',
                url: this.api_url + '/api/v1/admin/users/teacher',
                headers: {
                    'Authorization': 'Bearer ' + bearer,
                    'Accept': 'application/json',
                }
            })
            .then(response => {
                this.teachers = response.data
            })
            .catch(function (error) {
                if (error.response) {
                    //
                }
            })
            axios({
                method: 'get',
                url: this.api_url + '/api/v1/categories',
                headers: {
                    'Authorization' : 'Bearer ' + bearer,
                    'Accept' : 'application/json',
                },
            })
            .then(response => {
                this.categories = response.data
                this.is_page_ready = true
            })
            .catch(function (error) {
                if (error.response) {
                    //
                }
            })
        },
            methods: {
                refreshData(){
                    axios({
                        method: 'get',
                        url: this.api_url + '/api/v1/admin/users/teacher',
                        headers: {
                            'Authorization': 'Bearer ' + bearer,
                            'Accept': 'application/json',
                        }
                    })
                    .then(response => {
                        this.teachers = response.data
                    })
                    .catch(function (error) {
                        if (error.response) {
                            //
                        }
            })
                },
                appointTeacher(role,user_id){
                    this.showLoading()
                    var self = this
                    axios({
                        method: 'put',
                        url: this.api_url + '/api/v1/admin/users/'+ user_id + '/role',
                        headers: {
                            'Authorization' : 'Bearer ' + bearer,
                            'Accept' : 'application/json',
                        },
                        data: {
                            role : role
                        }
                    })
                    .then(response => {
                        this.hideLoading()
                        this.refreshData()
                        this.completion_message = "Data Updated!"
                    })
                    .catch(function (error) {
                        if (error.response) {
                            self.hideLoading()
                        }
                    })
                },
            },
        }) 
    </script>
@endsection