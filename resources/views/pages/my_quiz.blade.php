@extends('layout.default')

@section('meta')
    @component('components.meta')
        @slot('title')
             Dreamguru | My Quizzes
        @endslot

        @slot('description')
            Dreamaxtion helps companies and people maximize potential through Human Capital management system and psychometric assessment for personal and career growth.
        @endslot
    @endcomponent
@endsection

@section('css')
    <style> 
        
    </style>
@endsection

@section('content')
<div class="main-padding-h main-padding-v">
    <h3 class="color-gold fw-semi">
        My Quizzes
    </h3> 
    <h6 class="color-muted mb-3">
        BROWSE & EDIT YOUR QUIZZES
    </h6>

    <div class="dropdown">
          {{-- <button class="btn btn-secondary dropdown-toggle fill-gray me-2" type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
            Sort By <i class="fa-solid fa-arrow-down-wide-short ms-1"></i>
          </button>
          <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownMenuButton2">
            <li><a class="dropdown-item active" href="#">Action</a></li>
            <li><a class="dropdown-item" href="#">Another action</a></li>
            <li><a class="dropdown-item" href="#">Something else here</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="#">Separated link</a></li>
          </ul> --}}
        <a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#addQuizMasterModal"
            role="button" class="btn outline-gold fw-semi"
        >
            Add Quiz
        </a>
        <!-- Modal -->
        <div class="modal fade" id="addQuizMasterModal" tabindex="-1" aria-labelledby="addQuizMasterModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="addQuizModalLabel">
                            Add Quiz Master
                        </h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div v-if="error_show" class="alert alert-danger" role="alert">
                            <div v-for="value in error_show">
                                <p class="color-red">
                                    @{{ value[0] }}
                                </p>
                            </div>                
                        </div>
                        <label for="QuizMasterTitle" class="form-label">
                            Quiz Master Title:
                        </label>
                        <input type="text" v-model="req.title" class="form-control form-dark mb-3"
                            placeholder="Insert Quiz Master title here" id="QuizMasterTitle"
                        >
                        <label for="QuizMasterTime" class="form-label">
                            Time Limit:
                        </label>
                        <div class="d-flex w-50 mb-2">
                            <input placeholder="Minute" type="number" class="form-control form-dark me-2" 
                                v-model="req.time_limit_MM" id="EstimatedTimeMM" required
                            >
                            <input placeholder="Second" type="number" class="form-control form-dark" 
                                v-model="req.time_limit_SS" id="EstimatedTimeSS" required
                            >
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary outline-gold me-2" data-bs-dismiss="modal">
                        Cancel
                    </button>
                        <a href="javascript:void(0)" class="btn  fill-gold fw-semi"
                            @click="addQuizMaster()"
                        >
                        Save
                    </a>
                    </div>
                </div>
            </div>
        </div> 
    </div>
    <div class="d-flex align-items-center mt-3">
        <h6 class="color-gold me-3 mb-0">
            COMPLETED
        </h6>
        <div class="hr"></div>
    </div>
    <div class="row pt-4 pb-5 g-3">
        <div class="col-lg-6"  v-if="quiz.status == 'completed'" v-for="quiz in quizzes">
            <cards-longedit :quiz="quiz"></cards-longedit>
        </div>
        <div class="col-lg-6" v-if="!is_quiz_ready">
            <cards-edit-skeleton></cards-edit-skeleton>
        </div>
        <div class="col-lg-6" v-if="!is_quiz_ready">
            <cards-edit-skeleton></cards-edit-skeleton>
        </div>
    </div>
    <div class="d-flex align-items-center mt-3">
        <h6 class="color-gold me-3 mb-0">
            DRAFT
        </h6>
        <div class="hr"></div>
    </div>
    <div class="row pt-4 pb-5 g-3">
        <div class="col-lg-6"  v-if="quiz.status == 'draft'" v-for="quiz in quizzes">
            <cards-longedit :quiz="quiz"></cards-longedit>
        </div>
        <div class="col-lg-6" v-if="!is_quiz_ready">
            <cards-edit-skeleton></cards-edit-skeleton>
        </div>
        <div class="col-lg-6" v-if="!is_quiz_ready">
            <cards-edit-skeleton></cards-edit-skeleton>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script>
        'use strict';
        var app = new Vue({
            el: '#app',

            data() {
                return {
                    error_show: null,
                    quizzes: [],
                    is_quiz_ready: false,
                    req: {
                        title:'',
                        time_limit:'',
                        time_limit_MM: 0,
                        time_limit_SS: 0,
                    },
                    session_id: "{{session('data')['id']}}" ,
                }
            },
            mounted: function() {
                axios({
                    method: 'get',
                    url: this.api_url + '/api/v1/quizzes',
                    headers: {
                        'Authorization': 'Bearer ' + bearer,
                        'Accept': 'application/json',
                    }
                })
                .then(response => {
                    var data = response.data
                    _.forEach(data, (quiz)=>{
                        quiz.time_limit_MM = Math.floor(quiz.time_limit /60)
                        quiz.time_limit_SS = Math.floor(quiz.time_limit - (quiz.time_limit_MM * 60))
                    })
                    this.quizzes = data  
                    this.is_quiz_ready = true  
                })
            },
            methods: {
                addQuizMaster(){
                    this.showLoading()
                    var self = this
                    // converting time to second
                    this.req.time_limit_MM = this.req.time_limit_MM * 60
                    this.req.time_limit = parseInt(this.req.time_limit_SS) + parseInt(this.req.time_limit_MM)
                    axios({
                        method: 'post',
                        url: this.api_url + '/api/v1/quizzes',
                        headers: {
                            'Authorization' : 'Bearer ' + bearer,
                            'Accept' : 'application/json',
                        },
                        data: {
                            title: this.req.title,
                            time_limit: this.req.time_limit,
                        }

                    })
                    .then(response => {
                        this.hideLoading()
                        window.location.href = "{{url()->current()}}"
                    })
                    .catch(function (error) {
                        if (error.response) {
                            self.error_show = error.response.data.errors
                            self.hideLoading()
                        }
                    })
                }
            },
        })
    </script>
@endsection