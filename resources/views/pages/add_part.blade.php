@extends('layout.default')

@section('meta')
    @component('components.meta')
        @slot('title')
             Add Part
        @endslot

        @slot('description')
            Dreamaxtion helps companies and people maximize potential through Human Capital management system and psychometric assessment for personal and career growth.
        @endslot
    @endcomponent
@endsection

@section('css')
    <style> 
        
    </style>
@endsection

@section('content')
<div class="main-padding-h main-padding-v" v-cloak>
    <h3 class="color-gold fw-semi">
        Add Part
    </h3>
    <h6 v-if="is_part_ready" class="color-muted">
        @{{ section.title }} > ADD PART
    </h6>
</div>
<div class="row g-0" v-if="is_part_ready" v-cloak>
    <div class="col-8">
        <div class="main-padding-h">
            <div v-if="error_show" class="alert alert-danger" role="alert">
                <div v-for="value in error_show">
                    <p class="color-red">
                        @{{ value[0] }}
                    </p>
                </div>                
            </div>
            <div v-if="error_show2" class="alert alert-danger" role="alert">
                <div>
                    <p class="color-red">
                        @{{ error_show2 }}
                    </p>
                </div>                
            </div>
            <div class="d-flex align-items-center">
                <h6 class="color-gold me-3 mb-0" style="white-space: nowrap">
                    BASIC INFORMATION
                </h6>
                <div class="hr"></div>
            </div>
            <div class="mb-3"></div>
            <label for="PartTitle" class="form-label">
                Part Title
            </label>
            <input type="text" class="form-control" v-model="req.title" id="PartTitle" v-validate="'required'">
            <div id="titleHelp" class="form-text mb-2">
                Pick a Short and Clear Title
            </div>

            <label for="hero-background" class="form-label">
                Set Featured Image
            </label>
            <input type="file" accept="image/png, image/jpeg" class="form-control mt-3 mb-3" @change="uploadHero">
            <div id="imageHelp" class="form-text mb-2">
                Images Max Size 1MB & Use 4:1 Ratio Images
            </div>

            <label for="EstimatedTime" class="form-label">
                Set Estimated Time (Minute : Second)
            </label>
            <div class="d-flex w-50 mb-2">
                <input 
                    placeholder="Minute" 
                    type="number" 
                    class="form-control me-2" 
                    v-model="req.estimated_time_MM" 
                    id="EstimatedTimeMM" 
                    v-validate="'required'"
                >
                <input 
                    placeholder="Second" 
                    type="number" 
                    class="form-control" 
                    v-model="req.estimated_time_SS" 
                    id="EstimatedTimeSS" 
                    v-validate="'required'"
                >
            </div>

            <label for="wysiwyg" class="form-label">
                Content
            </label>
            <textarea id="wysiwyg" v-model="req.text" v-validate="'required'"></textarea>
            <div id="wysiwygHelp" class="form-text mb-2">
                Shortly describe this part
            </div>
            <div class="mb-3"></div>
        </div>
    </div>
    <div class="col-4">
        <div class="form-container me-5 mb-4">
            <div class="text-center"> 
                <a class="btn  d-inline-block fill-gold fw-semi my-4"
                    role="button" @click="updatePart('completed')"
                >
                    SAVE PART
                </a>
            </div>
        </div>
        <div class="d-flex align-items-center mb-4">
            <h6 class="color-gold me-3 mb-0" style="white-space: nowrap">
                VIDEO
            </h6>
            <div class="hr me-5"></div>
        </div>
        <div class="form-container me-5 mb-4">
            <div class="py-3 px-3">
                <label for="CourseVideo" class="form-label">
                    URL
                </label>
                <input @change="setEmbedId()" class="form-control form-dark" id="CourseVideo" v-model="req.video" v-validate="'required'">
                <div id="videoHelp" class="form-text mb-2">
                    Enter a valid video URL (Youtube Video > Share > Copy Link)
                </div>
            </div>
        </div>
        <div class="d-flex align-items-center mb-4">
            <h6 class="color-gold me-3 mb-0" style="white-space: nowrap">
                OPTIONS
            </h6>
            <div class="hr me-5"></div>
        </div>

        <div class="form-container me-5 mb-4">
            <div class="py-3 px-3">
                <div>
                    <input type="checkbox" id="isUnlock" v-model="req.is_unlock">
                    <label for="isUnlock" class="form-label">
                        Is Unlocked?
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
    <script>
        'use strict';
        var app = new Vue({
            el: '#app',

            data() {
                return {
                    error_show: null,
                    error_show2: null,
                    attachment:{
                        picture: null
                    },
                    section: null,
                    is_part_ready: false,
                    req: {
                        title: '',
                        text:'',
                        estimated_time:'',
                        estimated_time_SS: 0,
                        estimated_time_MM: 0,
                        video:'',
                        is_unlock: false,
                    },
                    embed_id: null,
                }
            },
            watch: {
                is_part_ready: function() {
                    
                    setTimeout(() => {
                            tinymce.init({
                                selector: '#wysiwyg',
                                menubar : 'edit format insert',
                                height : "480",
                                skin: 'oxide-dark',
                                content_css: 'dark',
                                plugins: 'paste lists nonbreaking',
                                paste_as_text: true,
                                toolbar: 'numlist bullist',
                                nonbreaking_force_tab: true
                            });
                            // The DOM element you wish to replace with Tagify
                            var input = document.querySelector('input[name=tags]');
                            
                            // initialize Tagify on the above input node reference
                            new Tagify(input)
                    }, 1000);
                }
            },
            mounted: function() {

                axios({
                    method: 'get',
                    url: this.api_url + '/api/v1/sections/{{$section_id}}',
                    headers: {
                        'Authorization' : 'Bearer ' + bearer,
                        'Accept' : 'application/json',
                    },
                })
                .then(response => {
                    this.section = response.data
                    this.is_part_ready = true

                })
            },
            methods: {
                updatePart(status){
                    var formData = new FormData()
                    var self = this
                    this.showLoading()
                    this.req.estimated_time = parseInt(this.req.estimated_time_SS) + parseInt((this.req.estimated_time_MM * 60))
                    if(this.attachment.picture !=  null){
                        formData.append("picture", this.attachment.picture)
                    }
                    if(this.embed_id !=  null){
                        formData.append('video', this.embed_id)
                    }
                    
                    formData.append("title", this.req.title)
                    formData.append("text", tinyMCE.activeEditor.getContent())
                    
                    formData.append('estimated_time', this.req.estimated_time)
                    formData.append('is_unlock', this.req.is_unlock)
                    formData.append('status', status)
                    axios({
                        method: 'post',
                        url: this.api_url + '/api/v1/sections/{{$section_id}}/parts',
                        headers: {
                            'Authorization' : 'Bearer ' + bearer,
                            'Accept' : 'application/json',
                        },
                        data: formData,

                        })
                        .then(response => {
                            this.hideLoading()
                            if (status == 'completed'){
                                window.location.href = "{{url()->previous()}}"
                            }
                            else{
                                window.location.href = "{{url()->current()}}"

                            }

                        })
                        .catch(function (error) {
                            if (error.response) {
                                self.error_show = error.response.data.errors;
                                window.location.href = '#'
                                self.hideLoading()
                            }
                        })
                    },
                uploadHero(event){
                    if(event.target.files[0].size > 1048576){
                        window.location.href = "#"
                        this.error_show2 = "File is too big! Max size is 1MB"
                        event.target.files[0] = null;
                        this.attachment.picture = '';
                    }
                    else{
                        this.attachment.picture = event.target.files[0]
                    }
                },
                setEmbedId(){
                    this.embed_id = this.req.video.replace('https://youtu.be/', '')
                }
            },
        })
    </script>
@endsection