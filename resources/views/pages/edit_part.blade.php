@extends('layout.default')

@section('meta')
    @component('components.meta')
        @slot('title')
             Part Edit
        @endslot

        @slot('description')
            Dreamaxtion helps companies and people maximize potential through Human Capital management system and psychometric assessment for personal and career growth.
        @endslot
    @endcomponent
@endsection

@section('css')
    <style> 
        
    </style>
@endsection

@section('content')
<div class="main-padding-h main-padding-v" v-cloak>
    <h3 class="color-gold fw-semi">
        Edit Part
    </h3>
    <h6 v-if="is_part_ready" class="color-muted text-uppercase">
        @{{title}} > EDIT
    </h6>
    <vue-skeleton-loader
        :height="30"
        class="w-100"
        v-if="!is_part_ready"
        color="rgba(52, 52, 52, 1)"
        animation="fade"
    ></vue-skeleton-loader>
</div>
<div class="sticky-container" v-if="is_part_ready" v-cloak>
    <p class="pt-3 ps-3">
        Shortcuts :
    </p>
    <div class="d-flex p-3">
            <div v-if="is_part_ready" class="me-3">
                <a class="btn fill-gold fw-semi"
                    @click="updatePart('completed')"
                >
                    SAVE PART
                </a>
            </div>
            <button class="btn color-red"  @click="updatePart('archived')">
                <i class="fa-solid fa-trash-can"></i>
            </button>
            <div v-if="is_part_ready">
                <a class="btn color-gold" :href="'/course/{{$course_id}}/parts/' + part.id + '/preview'">
                    <i class="fa-solid fa-eye"></i>
                </a>
            </div>
    </div>
</div>
<div class="row g-0" v-cloak>
    <div class="col-8">
        <div class="main-padding-h">
            <div v-if="error_show2" class="alert alert-danger" role="alert">
                <div>
                    <p class="color-red">
                        @{{ error_show2 }}
                    </p>
                </div>                
            </div>
            <div class="d-flex align-items-center">
                <h6 class="color-gold me-3 mb-0" style="white-space: nowrap">
                    BASIC INFORMATION
                </h6>
                <div class="hr"></div>
            </div>
            <div class="mb-3"></div>
                <label for="PartTitle" class="form-label">
                    Part Title
                </label>
                <input type="text" class="form-control" v-model="req.title" id="PartTitle" v-if="is_part_ready" required>
                <vue-skeleton-loader
                    :height="25"
                    class="w-100"
                    v-if="!is_part_ready"
                    color="rgba(52, 52, 52, 1)"
                    animation="fade"
                ></vue-skeleton-loader>
                <div id="titleHelp" class="form-text mb-2">
                    Pick a Short and Clear Title
                </div>
                <label for="hero-background" class="form-label">
                    Set Featured Image
                </label>
                <img id="hero-background" :src="api_url + '/storage/' + hero_background" 
                     height="300" class="w-100" v-if="hero_background"
                >
                <vue-skeleton-loader
                    :height="300"
                    v-if="!is_part_ready"
                    class="w-100"
                    color="rgba(52, 52, 52, 1)"
                    animation="fade"
                ></vue-skeleton-loader>
                <input type="file" class="form-control mt-3 mb-3" accept="image/png, image/jpeg" @change="uploadHero">
                <div id="imageHelp" class="form-text mb-2">
                    Images Max Size 1MB & Use 4:1 Ratio Images
                </div>
                <label for="EstimatedTime" class="form-label">
                    Set Estimated Time (Minute : Second)
                </label>
                <div class="d-flex w-50 mb-2" v-if="is_part_ready">
                    <input placeholder="Minute" type="number" class="form-control me-2" 
                    v-model="req.estimated_time_MM" id="EstimatedTimeMM" required>
                    <input placeholder="Second" type="number" class="form-control" 
                    v-model="req.estimated_time_SS" id="EstimatedTimeSS" required>
                </div>
                <div class="d-flex"  v-if="!is_part_ready">
                    <vue-skeleton-loader
                        :height="36"
                        :width="200"
                        class="me-2"
                        color="rgba(52, 52, 52, 1)"
                        animation="fade"
                    ></vue-skeleton-loader>
                    <vue-skeleton-loader
                        :height="36"
                        :width="200"
                        class="me-2"
                        color="rgba(52, 52, 52, 1)"
                        animation="fade"
                    ></vue-skeleton-loader>
                </div>
                <label for="wysiwyg" class="form-label">
                    Content
                </label>
                <textarea id="wysiwyg" v-model="req.text" v-if="is_part_ready" required>
                    @{{part.text}}
                </textarea>
                <vue-skeleton-loader
                    :height="480"
                    v-if="!is_part_ready"
                    class="w-100"
                    color="rgba(52, 52, 52, 1)"
                    animation="fade"
                ></vue-skeleton-loader>
                <div id="wysiwygHelp" class="form-text mb-2">
                    Shortly describe this part
                </div>
                <div class="mb-3"></div>
            </div>
        </div>
    <div class="col-4">
        <div class="form-container me-5 mb-4">
            <div class="text-center"> 
                <a class="btn d-inline-block fill-gold fw-semi my-4"
                    @click="updatePart('completed')"
                >
                    SAVE PART
                </a>
            </div>
            <div class="hr-dark"></div>
            <div class="py-3 px-3">
                <a class="btn btn-text color-red fw-semi" 
                    @click="updatePart('archived')"
                >
                    Move to Trash
                </a>
            </div>
            <div class="hr-dark"></div>
            <div class="py-3 px-3" v-if="is_part_ready">
                <a class="btn btn-text color-gold fw-semi" :href="'/course/{{$course_id}}/parts/' + part.id + '/preview'">
                    Preview
                </a>
            </div>
        </div>
        <div class="d-flex align-items-center mb-4">
            <h6 class="color-gold me-3 mb-0" style="white-space: nowrap">
                VIDEO
            </h6>
            <div class="hr me-5"></div>
        </div>
        <div class="form-container me-5 mb-4">
            <iframe 
                width="100%" 
                height="198" 
                v-if="video"
                :src="'https://youtube.com/embed/' + video"
            >
            </iframe>
            <vue-skeleton-loader
                :height="198"
                v-if="!is_part_ready"
                class="w-100 mb-2"
                color="rgba(52, 52, 52, 1)"
                animation="fade"
            ></vue-skeleton-loader>
            <div class="py-3 px-3">  
                <label for="CourseVideo" class="form-label">
                    URL
                </label>
                <input class="form-control form-dark" id="CourseVideo" v-model="req.video" required>
                <div id="videoHelp" class="form-text mb-2">
                    Enter a valid video URL (Youtube Video > Share > Copy Link)
                </div>
            </div>
        </div>
        <div class="d-flex align-items-center mb-4">
            <h6 class="color-gold me-3 mb-0" style="white-space: nowrap">
                OPTIONS
            </h6>
            <div class="hr me-5"></div>
        </div>
        <div class="form-container me-5 mb-4">
            <div class="py-3 px-3">
                <div>
                    <input type="checkbox" id="isUnlock" v-model="req.is_unlock">
                    <label for="isUnlock" class="form-label">
                        Is Unlocked?
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
    <script>
        'use strict';
        var app = new Vue({
            el: '#app',

            data() {
                return {
                    video: null,
                    attachment:{
                        picture: null
                    },
                    hero_background:'',
                    part: null,
                    is_part_ready: false,
                    is_editor_ready: false,
                    req: {
                        title: '',
                        text:'',
                        estimated_time:'',
                        estimated_time_MM:'',
                        estimated_time_SS:'',
                        video:'',
                        is_unlock: '',
                        picture:''
                    },
                    embed_id: null,
                    title: null,
                    error_show2: null,
                }
            },
            watch: {
                is_part_ready: function() {
                    var self = this
                    setTimeout(() => {
                            tinymce.init({
                                selector: '#wysiwyg',
                                menubar : 'edit format insert',
                                height : "480",
                                skin: 'oxide-dark',
                                content_css: 'dark',
                                plugins: 'paste lists nonbreaking image',
                                automatic_uploads: true,
                                file_picker_types: 'image',
                                /* and here's our custom image picker*/
                                file_picker_callback: function (cb, value, meta) {
                                    var input = document.createElement('input');
                                    input.setAttribute('type', 'file');
                                    input.setAttribute('accept', 'image/*');

                                    /*
                                    Note: In modern browsers input[type="file"] is functional without
                                    even adding it to the DOM, but that might not be the case in some older
                                    or quirky browsers like IE, so you might want to add it to the DOM
                                    just in case, and visually hide it. And do not forget do remove it
                                    once you do not need it anymore.
                                    */

                                    input.onchange = function () {
                                    var file = this.files[0];

                                    var reader = new FileReader();
                                    reader.onload = function () {
                                        /*
                                        Note: Now we need to register the blob in TinyMCEs image blob
                                        registry. In the next release this part hopefully won't be
                                        necessary, as we are looking to handle it internally.
                                        */
                                        var id = 'blobid' + (new Date()).getTime();
                                        var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                                        var base64 = reader.result.split(',')[1];
                                        var blobInfo = blobCache.create(id, file, base64);
                                        blobCache.add(blobInfo);

                                        /* call the callback and populate the Title field with the file name */
                                        cb(blobInfo.blobUri(), { title: file.name });
                                    };
                                    reader.readAsDataURL(file);
                                    };

                                    input.click();
                                },
                                paste_as_text: true,
                                toolbar: 'numlist bullist',
                                nonbreaking_force_tab: true,
                                // init_instance_callback: function (editor) {
                                //     editor.on('load', function (e) {
                                //         self.is_editor_ready = true;
                                //     });
                                // }
                            });
                            // The DOM element you wish to replace with Tagify
                            var input = document.querySelector('input[name=tags]');
                            
                            // initialize Tagify on the above input node reference
                            new Tagify(input)
                    }, 1000);


                }
            },
            mounted: function() {

                axios({
                    method: 'get',
                    url: this.api_url + '/api/v1/parts/{{$part_id}}',
                    headers: {
                        'Authorization' : 'Bearer ' + bearer,
                        'Accept' : 'application/json',
                    },
                })
                .then(response => {
                    
                    this.part = response.data
                    this.title = this.part.title
                    this.req = response.data
                    this.is_part_ready = true
                    this.hero_background = this.req.picture
                    this.video = this.req.video
                    if(this.req.video != null){
                        this.req.video = 'https://youtu.be/' + this.req.video
                    }
                    this.req.estimated_time_MM = Math.floor(this.req.estimated_time /60)
                    this.req.estimated_time_SS = Math.floor(this.req.estimated_time - (this.req.estimated_time_MM * 60))

                })
            },
            methods: {
                updatePart(status){
                    this.showLoading()
                    var self = this
                    var formData = new FormData()
                    if(this.attachment.picture !=  null){
                        formData.append("picture", this.attachment.picture)
                    }
                    
                    formData.append("title", this.req.title)
                    formData.append("text", tinyMCE.activeEditor.getContent())
                    // converting time to second
                    this.req.estimated_time_MM = this.req.estimated_time_MM * 60
                    this.req.estimated_time = parseInt(this.req.estimated_time_SS) + parseInt(this.req.estimated_time_MM)

                    formData.append('estimated_time', this.req.estimated_time)
                    formData.append('status', status)
                    axios({
                        method: 'post',
                        url: this.api_url + '/api/v1/parts/' + this.part.id,
                        headers: {
                            'Authorization' : 'Bearer ' + bearer,
                            'Accept' : 'application/json',
                        },
                        data: formData,

                    })
                    if(this.req.video != null){
                        this.embed_id = this.req.video.replace('https://youtu.be/', '')
                    }
                        
                    axios({
                        method: 'post',
                        url: this.api_url + '/api/v1/parts/' + this.part.id,
                        headers: {
                            'Authorization' : 'Bearer ' + bearer,
                            'Accept' : 'application/json',
                        },
                        data:{
                            is_unlock: this.req.is_unlock,
                            video: this.embed_id
                        }

                    })
                    .then(response => {
                        this.hideLoading()
                        if(status == 'completed'){
                            window.location.href = "/teacher/course/{{Request::route('course_id')}}/edit"
                        }
                        else{
                            window.location.href = "{{url()->current()}}"
                        }

                    })
                    .catch(function (error) {
                        if (error.response) {
                            self.hideLoading()
                        }
                    })
                },
                uploadHero(event){
                    if(event.target.files[0].size > 1048576){
                        window.location.href = "#"
                        this.error_show2 = "File is too big! Max size is 1MB"
                        event.target.files[0] = null;
                        this.attachment.picture = '';
                    }
                    else{
                        this.attachment.picture = event.target.files[0]
                    }
                    this.attachment.picture = event.target.files[0]
                },
            },
        }) 
    </script>
@endsection