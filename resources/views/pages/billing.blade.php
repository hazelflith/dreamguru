@extends('layout.default')

@section('meta')
    @component('components.meta')
    
        @slot('title')
             Pembayaran Billing
        @endslot

        @slot('description')
            Dreamaxtion helps companies and people maximize potential through Human Capital management system and psychometric assessment for personal and career growth.
        @endslot
    @endcomponent
@endsection

@section('css')
    <style> 
        
    </style>
@endsection

@section('content')
<div class="main-padding-h main-padding-v">
    <h3 class="color-gold fw-semi">
        Pembayaran Billing
    </h3>
    <h6 class="color-muted mb-4 text-uppercase">
        Selesaikan pembayaran anda disini.
    </h6>
    <div v-cloak>
        <div v-if="error_show" class="alert alert-danger" role="alert">
            <div>
                <p class="color-red">
                    @{{ error_show }}
                </p>
            </div>                
        </div>
        <div v-if="!is_billing_ready">
            <vue-skeleton-loader
                :height="100"
                class="w-100 mb-3"
                color="rgba(52, 52, 52, 1)"
                animation="fade"
            ></vue-skeleton-loader>
            <vue-skeleton-loader
                :height="100"
                class="w-100 mb-3"
                color="rgba(52, 52, 52, 1)"
                animation="fade"
            ></vue-skeleton-loader>
            <vue-skeleton-loader
                :height="500"
                class="w-100 mb-3"
                color="rgba(52, 52, 52, 1)"
                animation="fade"
            ></vue-skeleton-loader>
            <vue-skeleton-loader
                :height="100"
                class="w-100 mb-3"
                color="rgba(52, 52, 52, 1)"
                animation="fade"
            ></vue-skeleton-loader>
        </div>
        <div class="row gx-5" v-if="is_billing_ready">
            <div v-if="billing.bank" class="col-7">
                <div>
                    <div class="d-flex justify-content-between">
                        <div v-if="billing.status == 'pending'">
                            <p class="fw-semi color-muted">
                                Selesaikan pembayaran dalam
                            </p>
                            <div v-if="is_billing_ready">
                                <h3 class="fw-semi color-gold" id="timer"></h3>
                            </div>
                        </div>
                        <div class="ms-auto">
                            <p class="text-end fw-semi color-muted">
                                Tanggal
                            </p>
                            <h3 class="fw-semi color-gold">
                                @{{ date }}
                            </h3>
                        </div>
                    </div>
                    <div class="hr-gold mt-3 mb-3"></div>
                    <div class="d-flex align-items-center justify-content-between">
                        <div>
                            <p class="fw-semi color-muted" v-if="billing.payment_type == 'bank_transfer'">
                                Metode Pembayaran
                            </p>
                            <h5 class="fw-semi" v-if="billing.payment_type == 'bank_transfer' && billing.bank != 'mandiri'">
                                Bank Transfer
                            </h5>
                            <h5 class="fw-semi" v-if="billing.bank == 'mandiri'">
                                e-Channel
                            </h5>
                        </div>
                        <div v-if="billing.bank == 'bni'" class="img-white-bg">
                            <img src="/img/bni.png" alt="bni" width="150">
                        </div>
                        <div v-if="billing.bank == 'bca'" class="img-white-bg">
                            <img src="/img/bca.png" alt="bca" width="150">
                        </div>
                        <div v-if="billing.bank == 'mandiri'" class="img-white-bg">
                            <img src="/img/mandiri.png" alt="mandiri" width="150">
                        </div>
                        <div v-if="billing.bank == 'permata'" class="img-white-bg">
                            <img src="/img/permata.png" alt="permata" width="150">
                        </div>
                        <div v-if="billing.bank == 'bri'" class="img-white-bg">
                            <img src="/img/briva.svg" alt="briva" width="150">
                        </div>
                    </div>
                    <div class="my-2">
                        <p class="fw-semi color-muted" v-if="billing.payment_type == 'bank_transfer'">
                            Nomor Virtual Account
                        </p>
                        <h5 class="fw-semi">
                            @{{ billing.va_number }}
                        </h5>
                    </div>
                    <div class="my-2" v-if="billing.biller_code">
                        <p class="fw-semi color-muted">
                            Kode Perusahaan
                        </p>
                        <h5 class="fw-semi">
                            @{{ billing.biller_code }}
                        </h5>
                    </div>
                    <div class="my-2">
                        <p class="fw-semi color-muted">
                            Status
                        </p>
                        <h5 class="fw-semi" v-if="billing.status == 'pending'">
                            Pending
                        </h5>
                        <h5 class="fw-semi color-gold" v-if="billing.status == 'success'">
                            Success
                        </h5>
                        <h5 class="fw-semi color-red" v-if="billing.status == 'expired'">
                            Expired
                        </h5>
                        <h5 class="fw-semi color-red" v-if="billing.status == 'challenge'">
                            Challenge
                        </h5>
                        <h5 class="fw-semi color-red" v-if="billing.status == 'failed'">
                            Failed
                        </h5>
                    </div>
                </div>
                <div v-if="billing.payment_type == 'gopay'">
                    <div class="d-flex align-items-center justify-content-between">
                        <div>
                            <p class="fw-semi color-muted">
                                Metode Pembayaran
                            </p>
                            <h5 class="fw-semi">
                                GoPay
                            </h5>
                        </div>
                        <div class="img-white-bg">
                            <img src="/img/gopay.png" alt="gopay" width="150">
                        </div>
                    </div>
                    <div class="text-center mt-3">
                        <p class="mb-2">
                            Scan barcode ini dengan app Gojek untuk membayar
                        </p>
                        <img class="rounded" :src="'https://api.sandbox.midtrans.com/v2/gopay/' + billing.details.transaction_id + '/qr-code'" alt="qr-code" width="50%">
                    </div>
                </div>
                <div v-if="billing.payment_type == 'dana'">
                    <div class="d-flex align-items-center justify-content-between">
                        <div>
                            <p class="fw-semi color-muted">
                                Metode Pembayaran
                            </p>
                            <h5 class="fw-semi">
                                Dana
                            </h5>
                        </div>
                        <div class="img-white-bg">
                            <img src="/img/dana.png" alt="dana" width="150">
                        </div>
                    </div>
                </div>
                <div v-if="billing.payment_type == 'linkaja'">
                    <div class="d-flex align-items-center justify-content-between">
                        <div>
                            <p class="fw-semi color-muted">
                                Metode Pembayaran
                            </p>
                            <h5 class="fw-semi">
                                LinkAja
                            </h5>
                        </div>
                        <div class="img-white-bg">
                            <img src="/img/linkaja.png" alt="linkaja" width="150">
                        </div>
                    </div>
                </div>
                <div v-if="billing.payment_type == 'ovo'">
                    <div class="d-flex align-items-center justify-content-between">
                        <div>
                            <p class="fw-semi color-muted">
                                Metode Pembayaran
                            </p>
                            <h5 class="fw-semi">
                                OVO
                            </h5>
                        </div>
                        <div class="img-white-bg">
                            <img src="/img/ovo.png" alt="ovo" width="150">
                        </div>
                    </div>
                </div>
                <div v-if="billing.payment_type == 'shopeePay'">
                    <div class="d-flex align-items-center justify-content-between">
                        <div>
                            <p class="fw-semi color-muted">
                                Metode Pembayaran
                            </p>
                            <h5 class="fw-semi">
                                ShopeePay
                            </h5>
                        </div>
                        <div class="img-white-bg">
                            <img src="/img/shopeepay.png" alt="shopeepay" width="150">
                        </div>
                    </div>
                    <div v-if="billing.status == 'pending'" class="text-center mt-3">
                        <p class="mb-2">
                            Scan barcode ini dengan app ShopeePay untuk membayar
                        </p>
                        <img class="rounded" src="https://api.veritrans.co.id/v2/gopay/0667d2be-b683-4fad-8b0f-6d04afeaae5f/qr-code" alt="qr-code" width="50%">
                    </div>
                </div>
                <div>
                    <div class="mt-5">
                        <div class="billing-container m-0">
                            <p class="color-gold fw-semi mb-3">
                                Detail Item : 
                            </p>
                            <div class="d-flex justify-content-between">
                                <strong><p>
                                    Judul Course
                                </p></strong>
                                <strong><p>
                                    Harga
                                </p></strong>
                            </div>
                            <div class="d-flex justify-content-between">
                                <p>
                                    @{{billing.course.title}}
                                </p>
                                <p v-if="!billing.course.is_on_discount">
                                    Rp. @{{price}}
                                </p>
                                <div v-if="billing.course.is_on_discount">
                                    <p class="color-muted strike">
                                        Rp. @{{price}}
                                    </p>
                                    <p>
                                        Rp. @{{discount_price}}
                                    </p>
                                </div>
                                
                            </div>
                            <hr style="color:#FBD68D !important">
                            <div class="d-flex justify-content-between">
                                <strong><p>
                                    Total
                                </p></strong>
                                <strong v-if="!billing.course.is_on_discount"><p>
                                    Rp. @{{price}}
                                </p></strong>
                                <strong v-else><p>
                                    Rp. @{{discount_price}}
                                </p></strong>
                            </div>
                        </div>
                    </div>
                </div>
                <a v-if="billing.status == 'pending'" href="javascript:void(0)" @click="checkPayment()" class="btn fill-gold fw-semi mt-4">
                    Selesai Membayar
                </a>
                <a v-else href="/student/dashboard/billing" class="btn fill-gold fw-semi mt-4">
                    Billing Dashboard
                </a>
            </div>
            <div v-else>
                <div>
                    <div class="d-flex justify-content-between">
                        <div v-if="billing.status == 'pending'">
                            <p class="fw-semi color-muted">
                                Selesaikan pembayaran dalam
                            </p>
                            <div v-if="is_billing_ready">
                                <h3 class="fw-semi color-gold" id="timer"></h3>
                            </div>
                        </div>
                        <div class="ms-auto">
                            <p class="text-end fw-semi color-muted">
                                Tanggal
                            </p>
                            <h3 class="fw-semi color-gold">
                                @{{ date }}
                            </h3>
                        </div>
                    </div>
                    <div class="hr-gold mt-3 mb-3"></div>
                    <div class="d-flex align-items-center justify-content-between">
                        <div>
                            <p class="fw-semi color-muted" v-if="billing.payment_type == 'bank_transfer'">
                                Metode Pembayaran
                            </p>
                            <h5 class="fw-semi" v-if="billing.payment_type == 'bank_transfer' && billing.bank != 'mandiri'">
                                Bank Transfer
                            </h5>
                            <h5 class="fw-semi" v-if="billing.bank == 'mandiri'">
                                e-Channel
                            </h5>
                        </div>
                        <div v-if="billing.bank == 'bni'" class="img-white-bg">
                            <img src="/img/bni.png" alt="bni" width="150">
                        </div>
                        <div v-if="billing.bank == 'bca'" class="img-white-bg">
                            <img src="/img/bca.png" alt="bca" width="150">
                        </div>
                        <div v-if="billing.bank == 'mandiri'" class="img-white-bg">
                            <img src="/img/mandiri.png" alt="mandiri" width="150">
                        </div>
                        <div v-if="billing.bank == 'permata'" class="img-white-bg">
                            <img src="/img/permata.png" alt="permata" width="150">
                        </div>
                        <div v-if="billing.bank == 'bri'" class="img-white-bg">
                            <img src="/img/briva.svg" alt="briva" width="150">
                        </div>
                    </div>
                    <div class="my-2">
                        <p class="fw-semi color-muted" v-if="billing.payment_type == 'bank_transfer'">
                            Nomor Virtual Account
                        </p>
                        <h5 class="fw-semi">
                            @{{ billing.va_number }}
                        </h5>
                    </div>
                    <div class="my-2" v-if="billing.biller_code">
                        <p class="fw-semi color-muted">
                            Kode Perusahaan
                        </p>
                        <h5 class="fw-semi">
                            @{{ billing.biller_code }}
                        </h5>
                    </div>
                    <div class="my-2">
                        <p class="fw-semi color-muted">
                            Status
                        </p>
                        <h5 class="fw-semi" v-if="billing.status == 'pending'">
                            Pending
                        </h5>
                        <h5 class="fw-semi color-gold" v-if="billing.status == 'success'">
                            Success
                        </h5>
                        <h5 class="fw-semi color-red" v-if="billing.status == 'expired'">
                            Expired
                        </h5>
                        <h5 class="fw-semi color-red" v-if="billing.status == 'challenge'">
                            Challenge
                        </h5>
                        <h5 class="fw-semi color-red" v-if="billing.status == 'failed'">
                            Failed
                        </h5>
                    </div>
                </div>
                <div v-if="billing.payment_type == 'gopay'">
                    <div class="d-flex align-items-center justify-content-between">
                        <div>
                            <p class="fw-semi color-muted">
                                Metode Pembayaran
                            </p>
                            <h5 class="fw-semi">
                                GoPay
                            </h5>
                        </div>
                        <div class="img-white-bg">
                            <img src="/img/gopay.png" alt="gopay" width="150">
                        </div>
                    </div>
                    <div class="text-center mt-3">
                        <p class="mb-2">
                            Scan barcode ini dengan app Gojek untuk membayar
                        </p>
                        <img class="rounded" :src="'https://api.sandbox.midtrans.com/v2/gopay/' + billing.details.transaction_id + '/qr-code'" alt="qr-code" width="50%">
                    </div>
                </div>
                <div v-if="billing.payment_type == 'dana'">
                    <div class="d-flex align-items-center justify-content-between">
                        <div>
                            <p class="fw-semi color-muted">
                                Metode Pembayaran
                            </p>
                            <h5 class="fw-semi">
                                Dana
                            </h5>
                        </div>
                        <div class="img-white-bg">
                            <img src="/img/dana.png" alt="dana" width="150">
                        </div>
                    </div>
                </div>
                <div v-if="billing.payment_type == 'linkaja'">
                    <div class="d-flex align-items-center justify-content-between">
                        <div>
                            <p class="fw-semi color-muted">
                                Metode Pembayaran
                            </p>
                            <h5 class="fw-semi">
                                Dana
                            </h5>
                        </div>
                        <div class="img-white-bg">
                            <img src="/img/linkaja.png" alt="linkaja" width="150">
                        </div>
                    </div>
                </div>
                <div v-if="billing.payment_type == 'ovo'">
                    <div class="d-flex align-items-center justify-content-between">
                        <div>
                            <p class="fw-semi color-muted">
                                Metode Pembayaran
                            </p>
                            <h5 class="fw-semi">
                                Dana
                            </h5>
                        </div>
                        <div class="img-white-bg">
                            <img src="/img/ovo.png" alt="ovo" width="150">
                        </div>
                    </div>
                </div>
                <div v-if="billing.payment_type == 'shopeePay'">
                    <div class="d-flex align-items-center justify-content-between">
                        <div>
                            <p class="fw-semi color-muted">
                                Metode Pembayaran
                            </p>
                            <h5 class="fw-semi">
                                ShopeePay
                            </h5>
                        </div>
                        <div class="img-white-bg">
                            <img src="/img/shopeepay.png" alt="shopeepay" width="150">
                        </div>
                    </div>
                    <div v-if="billing.status == 'pending'" class="text-center mt-3">
                        <p class="mb-2">
                            Scan barcode ini dengan app ShopeePay untuk membayar
                        </p>
                        <img class="rounded" src="https://api.veritrans.co.id/v2/gopay/0667d2be-b683-4fad-8b0f-6d04afeaae5f/qr-code" alt="qr-code" width="50%">
                    </div>
                </div>
                <div>
                    <div class="mt-5">
                        <div class="billing-container m-0">
                            <p class="color-gold fw-semi mb-3">
                                Detail Item : 
                            </p>
                            <div class="d-flex justify-content-between">
                                <strong><p>
                                    Judul Course
                                </p></strong>
                                <strong><p>
                                    Harga
                                </p></strong>
                            </div>
                            <div class="d-flex justify-content-between">
                                <p>
                                    @{{billing.course.title}}
                                </p>
                                <p v-if="!billing.course.is_on_discount">
                                    Rp. @{{price}}
                                </p>
                                <div v-if="billing.course.is_on_discount">
                                    <p class="color-muted strike">
                                        Rp. @{{price}}
                                    </p>
                                    <p>
                                        Rp. @{{discount_price}}
                                    </p>
                                </div>
                            </div>
                            <hr style="color:#FBD68D !important">
                            <div class="d-flex justify-content-between">
                                <strong><p>
                                    Total
                                </p></strong>
                                <strong v-if="!billing.course.is_on_discount"><p>
                                    Rp. @{{price}}
                                </p></strong>
                                <strong v-else><p>
                                    Rp. @{{discount_price}}
                                </p></strong>
                            </div>
                        </div>
                    </div>
                </div>
                <a v-if="billing.status == 'pending'" href="javascript:void(0)" class="btn fill-gold fw-semi mt-4" @click="checkPayment()">
                    Selesai Membayar
                </a>
                <a v-else href="/student/dashboard/billing" class="btn fill-gold fw-semi mt-4">
                    Billing Dashboard
                </a>
            </div>
            {{-- MANDIRI --}}
            <div class="col-5" v-if="billing.bank == 'mandiri'"> 
                <p class="color-gold mb-3">
                    Cara Pembayaran
                </p>
                <ul class="nav nav-tabs" id="mandiriTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button 
                            class="nav-link active" 
                            id="atm-mandiri" 
                            data-bs-toggle="tab" 
                            data-bs-target="#atm_mandiri" 
                            type="button" role="tab" 
                            aria-controls="atm_mandiri" 
                            aria-selected="true"
                        >
                            ATM Mandiri
                        </button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button 
                            class="nav-link" 
                            id="ib-mandiri" 
                            data-bs-toggle="tab" 
                            data-bs-target="#ib_mandiri" 
                            type="button" 
                            role="tab" 
                            aria-controls="ib_mandiri" 
                            aria-selected="false"
                        >
                            Mandiri Internet Banking
                        </button>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="atm_mandiri" role="tabpanel" aria-labelledby="atm-mandiri">
                        <div class="row mt-3">
                            <div class="col">
                                <ol>
                                    <li>Pada menu utama, pilih Bayar/Beli.</li>
                                    <li>Pilih Lainnya.</li>
                                    <li>Pilih Multi Payment.</li>
                                    <li>Masukkan 70012 (kode perusahaan Midtrans) lalu tekan Benar.</li>
                                    <li>Masukkan Kode Pembayaran Anda lalu tekan Benar.</li>
                                    <li>Pada halaman konfirmasi akan muncul detail pembayaran Anda. Jika informasi telah sesuai tekan Ya.</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="ib_mandiri" role="tabpanel" aria-labelledby="ib-mandiri">
                        <div class="row mt-3">
                            <div class="col">
                                <ol>
                                    <li>Login ke Internet Banking Mandiri (https://ibank.bankmandiri.co.id/).</li>
                                    <li>Pada menu utama, pilih Bayar, lalu pilih Multi Payment.</li>
                                    <li>Pilih akun Anda di Dari Rekening, kemudian di Penyedia Jasa pilih Midtrans.</li>
                                    <li>Masukkan Kode Pembayaran Anda dan klik Lanjutkan.</li>
                                    <li>Konfirmasi pembayaran Anda menggunakan Mandiri Token.</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- BNI --}}
            <div class="col-5" v-if="billing.bank == 'bni'"> 
                <p class="color-gold mb-3">
                    Cara Pembayaran
                </p>
                <ul class="nav nav-tabs" id="bniTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button 
                            class="nav-link active" 
                            id="atm-bni" 
                            data-bs-toggle="tab" 
                            data-bs-target="#atm_bni" 
                            type="button" role="tab" 
                            aria-controls="atm_bni" 
                            aria-selected="true"
                        >
                            ATM BNI
                        </button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button 
                            class="nav-link" 
                            id="ib-bni" 
                            data-bs-toggle="tab" 
                            data-bs-target="#ib_bni" 
                            type="button" 
                            role="tab" 
                            aria-controls="ib_bni" 
                            aria-selected="false"
                        >
                            BNI Internet Banking
                        </button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button 
                            class="nav-link" 
                            id="m-bni" 
                            data-bs-toggle="tab" 
                            data-bs-target="#m_bni" 
                            type="button" 
                            role="tab" 
                            aria-controls="m_bni" 
                            aria-selected="false"
                        >
                            BNI Mobile Banking
                        </button>
                    </li>
                </ul>
                <div class="tab-content" id="bniContent">
                    <div class="tab-pane fade show active" id="atm_bni" role="tabpanel" aria-labelledby="atm-bni">
                        <div class="row mt-3">
                            <div class="col">
                                <ol>
                                    <li>Pada menu utama, pilih Menu Lainnya.</li>
                                    <li>Pilih Transfer.</li>
                                    <li>Pilih Rekening Tabungan.</li>
                                    <li>Pilih Ke Rekening BNI.</li>
                                    <li>Masukkan nomor virtual account dan pilih Tekan Jika Benar.</li>
                                    <li>Masukkan jumlah tagihan yang akan anda bayar secara lengkap. Pembayaran dengan jumlah tidak sesuai akan otomatis ditolak.</li>
                                    <li>Jumlah yang dibayarkan, nomor rekening dan nama Merchant akan ditampilkan. Jika informasi telah sesuai, tekan Ya.</li>
                                    <li>Transaksi Anda sudah selesai.</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="ib_bni" role="tabpanel" aria-labelledby="ib-bni">
                        <div class="row mt-3">
                            <div class="col">
                                <ol>
                                    <li>Ketik alamat https://ibank.bni.co.id kemudian klik Masuk.</li>
                                    <li>Silakan masukkan User ID dan Password.</li>
                                    <li>Klik menu Transfer kemudian pilih Tambah Rekening Favorit.</li>
                                    <li>Masukkan nama, nomor rekening, dan email, lalu klik Lanjut.</li>
                                    <li>Masukkan Kode Otentikasi dari token Anda dan klik Lanjut.</li>
                                    <li>Kembali ke menu utama dan pilih Transfer lalu Transfer Antar Rekening BNI.</li>
                                    <li>Pilih rekening yang telah Anda favoritkan sebelumnya di Rekening Tujuan lalu lanjutkan pengisian, dan tekan Lanjut.</li>
                                    <li>Pastikan detail transaksi Anda benar, lalu masukkan Kode Otentikasi dan tekan Lanjut.</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="m_bni" role="tabpanel" aria-labelledby="m-bni">
                        <div class="row mt-3">
                            <div class="col">
                                <ol>
                                    <li>Buka aplikasi BNI Mobile Banking dan login</li>
                                    <li>Pilih menu Transfer</li>
                                    <li>Pilih menu Virtual Account Billing</li>
                                    <li>Pilih rekening debit yang akan digunakan</li>
                                    <li>Pilih menu Input Baru dan masukkan 16 digit nomor Virtual Account</li>
                                    <li>Informasi tagihan akan muncul pada halaman validasi</li>
                                    <li>Jika informasi telah sesuai, masukkan Password Transaksi dan klik Lanjut</li>
                                    <li>Transaksi Anda akan diproses</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- BCA --}}
            <div class="col-5" v-if="billing.bank == 'bca'">
                <p class="color-gold mb-3">
                    Cara Pembayaran
                </p>
                <ul class="nav nav-tabs" id="bcaTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button 
                            class="nav-link active" 
                            id="atm-bca" 
                            data-bs-toggle="tab" 
                            data-bs-target="#atm_bca" 
                            type="button" 
                            role="tab" 
                            aria-controls="atm_bca" 
                            aria-selected="true"
                        >
                            ATM BCA
                        </button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button 
                            class="nav-link" 
                            id="klik-bca" 
                            data-bs-toggle="tab" 
                            data-bs-target="#klik_bca" 
                            type="button" 
                            role="tab" 
                            aria-controls="klik_bca" 
                            aria-selected="false"
                        >
                            Klik BCA
                        </button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button 
                            class="nav-link" 
                            id="m-bca" 
                            data-bs-toggle="tab" 
                            data-bs-target="#m_bca" 
                            type="button" 
                            role="tab" 
                            aria-controls="m_bca" 
                            aria-selected="false"
                        >
                            m-BCA
                        </button>
                    </li>
                </ul>
                <div class="tab-content" id="bcaContent">
                    <div class="tab-pane fade show active" id="atm_bca" role="tabpanel" aria-labelledby="atm-bca">
                        <div class="row mt-3">
                            <div class="col">
                                <ol>
                                    <li>Pada menu utama, pilih Transaksi Lainnya.</li>
                                    <li>Pilih Transfer.</li>
                                    <li>Pilih Ke Rek BCA Virtual Account.</li>
                                    <li>Masukkan Nomor Rekening pembayaran (11 digit) Anda lalu tekan Benar.</li>
                                    <li>Masukkan jumlah tagihan yang akan anda bayar.</li>
                                    <li>Pada halaman konfirmasi transfer akan muncul detail pembayaran Anda. Jika informasi telah sesuai tekan Ya.</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="klik_bca" role="tabpanel" aria-labelledby="klik-bca">
                        <div class="row mt-3">
                            <div class="col">
                                <ol>
                                    <li>Pilih menu Transfer Dana.</li>
                                    <li>Pilih Transfer ke BCA Virtual Account.</li>
                                    <li>Masukkan nomor BCA Virtual Account, atau pilih Dari Daftar Transfer.</li>
                                    <li>Ambil BCA Token Anda dan masukkan KEYBCA Response APPLI 1 dan Klik Submit.</li>
                                    <li>Transaksi Anda selesai.</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="m_bca" role="tabpanel" aria-labelledby="m-bca">
                        <div class="row mt-3">
                            <div class="col">
                                <ol>
                                    <li>Lakukan log in pada aplikasi BCA Mobile.</li>
                                    <li>Pilih menu m-BCA, kemudian masukkan kode akses m-BCA.</li>
                                    <li>Pilih m-Transfer > BCA Virtual Account.</li>
                                    <li>Pilih dari Daftar Transfer, atau masukkan Nomor Virtual Account tujuan.</li>
                                    <li>Masukkan jumlah yang ingin dibayarkan.</li>
                                    <li>Masukkan pin m-BCA.</li>
                                    <li>Pembayaran selesai. Simpan notifikasi yang muncul sebagai bukti pembayaran.</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- PERMATA --}}
            <div class="col-5" v-if="billing.bank == 'permata'">
                <p class="color-gold mb-3">
                    Cara Pembayaran
                </p>
                <ul class="nav nav-tabs" id="permataTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button 
                            class="nav-link active" 
                            id="atm-permata" 
                            data-bs-toggle="tab" 
                            data-bs-target="#atm_permata" 
                            type="button" 
                            role="tab" 
                            aria-controls="atm_permata" 
                            aria-selected="true"
                        >
                            ATM Permata
                        </button>
                    </li>
                </ul>
                <div class="tab-content" id="permataContent">
                    <div class="tab-pane fade show active" id="atm_permata" role="tabpanel" aria-labelledby="atm-permata">
                        <div class="row mt-3">
                            <div class="col">
                                <ol>
                                    <li>Pada menu utama, pilih Transaksi Lainnya.</li>
                                    <li>Pilih Pembayaran.</li>
                                    <li>Pilih Pembayaran Lainnya.</li>
                                    <li>Pilih Virtual Account.</li>
                                    <li>Masukkan 16 digit No. Virtual Account yang dituju, lalu tekan Benar.</li>
                                    <li>Pilih rekening pembayaran Anda dan tekan Benar.</li>
                                    <li>Transaksi Anda sudah selesai.</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- BRI --}}
            <div class="col-5" v-if="billing.bank == 'bri'">
                <p class="color-gold mb-3">
                    Cara Pembayaran
                </p>
                <ul class="nav nav-tabs" id="briTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button 
                            class="nav-link active" 
                            id="atm-bri" 
                            data-bs-toggle="tab" 
                            data-bs-target="#atm_bri" 
                            type="button" 
                            role="tab" 
                            aria-controls="atm_bri" 
                            aria-selected="true"
                        >
                            ATM BRI
                        </button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button 
                            class="nav-link" 
                            id="ib-bri" 
                            data-bs-toggle="tab" 
                            data-bs-target="#ib_bri" 
                            type="button" 
                            role="tab" 
                            aria-controls="ib_bri" 
                            aria-selected="false"
                        >
                            IB BRI
                        </button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button 
                            class="nav-link" 
                            id="m-bri" 
                            data-bs-toggle="tab" 
                            data-bs-target="#m_bri" 
                            type="button" 
                            role="tab" 
                            aria-controls="m_bri" 
                            aria-selected="false"
                        >
                            BRImo
                        </button>
                    </li>
                </ul>
                <div class="tab-content" id="briContent">
                    <div class="tab-pane fade show active" id="atm_bri" role="tabpanel" aria-labelledby="atm-bri">
                        <div class="row mt-3">
                            <div class="col">
                                <ol>
                                    <li>Pilih menu utama, pilih Transaksi Lain.</li>
                                    <li>Pilih Pembayaran.</li>
                                    <li>Pilih Lainnya.</li>
                                    <li>Pilih BRIVA.</li>
                                    <li>Masukkan Nomor BRIVA pelanggan dan pilih Benar.</li>
                                    <li>Jumlah pembayaran, nomor BRIVA dan nama merchant akan muncul pada halaman konfirmasi pembayaran. Jika informasi yang dicantumkan benar, pilih Ya.</li>
                                    <li>Pembayaran telah selesai. Simpan bukti pembayaran Anda.</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="ib_bri" role="tabpanel" aria-labelledby="ib-bri">
                        <div class="row mt-3">
                            <div class="col">
                                <ol>
                                    <li>Masuk pada Internet Banking BRI.</li>
                                    <li>Pilih menu Pembayaran & Pembelian.</li>
                                    <li>Pilih sub menu BRIVA.</li>
                                    <li>Masukkan Nomor BRIVA.</li>
                                    <li>Jumlah pembayaran, nomor pembayaran, dan nama merchant akan muncul pada halaman konfirmasi pembayaran. Jika informasi yang dicantumkan benar, pilih Kirim.</li>
                                    <li>Masukkan password dan mToken, pilih Kirim.</li>
                                    <li>Masukkan password dan mToken, pilih Kirim.</li>
                                    <li>Pembayaran telah selesai. Untuk mencetak bukti transaksi, pilih Cetak.</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="m_bri" role="tabpanel" aria-labelledby="m-bri">
                        <div class="row mt-3">
                            <div class="col">
                                <ol>
                                    <li>Masuk ke dalam aplikasi BRI Mobile, pilih Mobile Banking BRI.</li>
                                    <li>Pilih Pembayaran, lalu pilih BRIVA.</li>
                                    <li>Masukkan nomor BRIVA.</li>
                                    <li>Jumlah pembayaran, nomor pembayaran, dan nama merchant akan muncul pada halaman konfirmasi pembayaran. Jika informasi yang dicantumkan benar, pilih Continue.</li>
                                    <li>Masukkan Mobile Banking BRI PIN, pilih Ok.</li>
                                    <li>Pembayaran telah selesai. Simpan notifikasi sebagai bukti pembayaran.</li>
                                </ol>
                            </div>
                        </div>
                    </div>
            </div>
            {{-- GOPAY --}}
            <div class="col-5" v-if="billing.payment_type == 'gopay'">
                <p class="color-gold mb-3">
                    Cara Pembayaran
                </p>
                <ol>
                    <li>Anda akan diarahkan ke Aplikasi ShopeePay</li>
                    <li>ShopeePay akan menampilkan total pembayaran</li>
                    <li>Masukkan PIN</li>
                    <li>Klik Lanjutkan</li>
                    <li>Transaksi Selesai</li>
                </ol>
            </div>
            {{-- SHOPEEPAY --}}
            <div class="col-5" v-if="billing.payment_type == 'shopeepay'">
                <p class="color-gold mb-3">
                    Cara Pembayaran
                </p>
                <ol>
                    <li>Anda akan diarahkan ke Aplikasi ShopeePay</li>
                    <li>ShopeePay akan menampilkan total pembayaran</li>
                    <li>Masukkan PIN</li>
                    <li>Klik Lanjutkan</li>
                    <li>Transaksi Selesai</li>
                </ol>
            </div>
        </div>
        </div>
    </div>
</div>

@endsection

@section('js')
    <script>
        'use strict';
        var app = new Vue({
            el: '#app',

            data() {
                return {
                    error_show: null,
                    billing : null,
                    is_billing_ready : false,
                    date: null,
                    price: '',
                    discount_price: '',
                    course: null,
                }
            },
            
            mounted() {
                
                axios({
                    method: 'get',
                    url: this.api_url + '/api/v1/users/self/transactions/{{Request::get('order')}}',
                    headers: {
                        'Authorization': 'Bearer ' + bearer,
                        'Accept': 'application/json',
                    }
                })
                .then(response => {
                    this.billing = response.data
                    this.billing.amount = Number(this.billing.amount).toLocaleString('id');
                    var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour : 'numeric', minute : 'numeric'}
                    this.date = new Date(this.billing.due_date).toLocaleString('id-ID', options)
                    // Set the date we're counting down to
                    var countDownDate = new Date(this.billing.due_date).getTime();
                    var self = this
                    this.is_billing_ready = true
                    this.price = Number(this.billing.course.price).toLocaleString('id');
                    this.discount_price = Number(this.billing.course.discount_price).toLocaleString('id');
                    
                    // Update the count down every 1 second
                    var x = setInterval(function() {
                        
                        // Get today's date and time
                        var now = new Date().getTime();
                            
                        // Find the distance between now and the count down date
                        var distance = countDownDate - now;
                        
                        // Time calculations for days, hours, minutes and seconds
                        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                        
                        // Display the result in the element with id="demo"
                        document.getElementById("timer").innerHTML = hours + "h "
                        + minutes + "m " + seconds + "s";
                        
                        
                        // If the count down is finished, write some text
                        if (distance < 0) {
                            clearInterval(x);
                            document.getElementById("timer").innerHTML = "EXPIRED"
                        }
                    }, 1000);
                })
            },
            methods: {
                checkPayment(){
                    axios({
                    method: 'get',
                    url: this.api_url + '/api/v1/users/self/transactions/{{Request::get('order')}}',
                    headers: {
                        'Authorization': 'Bearer ' + bearer,
                        'Accept': 'application/json',
                    }
                    })
                    .then(response => {
                        this.billing = response.data
                        this.billing.amount = Number(this.billing.amount).toLocaleString('id');
                        var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour : 'numeric', minute : 'numeric'}
                        this.date = new Date(this.billing.due_date).toLocaleString('id-ID', options)
                        // Set the date we're counting down to
                        var countDownDate = new Date(this.billing.due_date).getTime();
                        var self = this
                        this.price = Number(this.billing.course.price).toLocaleString('id');
                        this.discount_price = Number(this.billing.course.discount_price).toLocaleString('id');
                        if(this.billing.status == 'success'){
                            window.location.href = '/course/' + this.billing.course.slug
                        }
                        else{
                            this.error_show = "Maaf pembayaran anda belum kami terima, Mohon pastikan kembali lalu klik ulang tombol selesaikan pembayaran"
                            window.location.href ="#"
                        }
                    })
                }
            },
        })
    </script>
@endsection