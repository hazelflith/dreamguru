@extends('layout.default')

@section('meta')
    @component('components.meta')
        @slot('title')
             Learn Angular | Session 1
        @endslot

        @slot('description')
            Dreamaxtion helps companies and people maximize potential through Human Capital management system and psychometric assessment for personal and career growth.
        @endslot
    @endcomponent
@endsection

@section('css')
    <style> 
        
    </style>
@endsection

@section('content')
<div class="row g-0">
    <div class="col-lg-9">
        <div class="main-padding-h mb-3 mt-5" v-cloak>
            <div class="d-flex align-items-center">
                <h1 v-if="is_question_ready" class="color-gold fw-bold m-0">
                    Question @{{active_question}} of @{{questions.length}}
                </h1>
                <h1 class="color-gold fw-bold m-0 ms-auto"><label class="color-gold" id="timer"></label>
                </h1>
            </div>
            <div class="d-flex" v-if="!is_question_ready">
                <vue-skeleton-loader
                    :height="36"
                    :width="400"
                    color="rgba(52, 52, 52, 1)"
                    animation="fade"
                ></vue-skeleton-loader>
                <vue-skeleton-loader
                    :height="36"
                    :width="100"
                    color="rgba(52, 52, 52, 1)"
                    animation="fade"
                    class="ms-auto"
                ></vue-skeleton-loader>
            </div>
        </div>
        <div v-for="(question, index) in questions" v-if="user_quiz" class="mb-5">
            <question 
                :question="question" 
                :length="length" 
                :question_index="index" 
                :key="question.id" 
                :user_question="_.find(user_quiz.user_quiz_questions, {'question_id': question.id})"
                v-show="index + 1 == active_question"
            ></question>
        </div>
        <question-skeleton v-if="!is_question_ready"></question-skeleton>
        <result 
            v-if="result"
            :result="result" 
            :user_quiz="user_quiz" 
            :section_quiz="section_quiz" 
            :course_sections="course_sections"
            :course_id="course_id"
        ></result>
    </div>
    <div class="d-none d-lg-block col-lg-3">
        <toc :course_sections="course_sections" content_id="{{$content_id}}" :progress="progress" v-if="is_question_ready"></toc>
        <toc-skeleton v-if="!is_question_ready"></toc-skeleton>
    </div>
</div>
<div class="modal fade" id="warningModal" tabindex="-1" aria-labelledby="warningModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <h2 class="color-gold fw-bold text-center">
                    Sorry!
                </h2>
                <p class="color-gold text-center mt-4">
                    Quiz tidak dapat diolah karena jawaban kosong! Mohon pastikan jawaban anda telah terisi
                </p>
                <button type="button" class="w-100 btn d-inline-block fill-gold fw-semi mt-4" data-bs-dismiss="modal">
                    Mengerti
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="warningExpiredModal" tabindex="-1" aria-labelledby="warningModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <h2 class="color-gold fw-bold text-center">
                    Sorry!
                </h2>
                <p class="color-gold text-center mt-4">
                    Quiz tidak dapat diolah karena jawaban kosong!
                </p>
                <button type="button" class="w-100 btn d-inline-block fill-gold fw-semi mt-4" 
                    data-bs-dismiss="modal" @click="retakeQuiz()"
                >
                    Retake Quiz
                </button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
    <script>
        'use strict';
        var app = new Vue({
            el: '#app',

            data() {
                return {
                    course_sections: null,
                    course_id: '{{$course_id}}',
                    quiz: null,
                    questions: null,
                    section_quiz: null,
                    is_question_ready: false,
                    active_question: 1,
                    user_quiz: null,
                    length: null,
                    question_index: null,
                    is_result_ready : false,
                    result: null,
                    time_spent: 0,
                    myInterval: null,
                    progress: '',
                }
            },
            mounted: function(){
                axios({
                    method: 'get',
                    url: this.api_url + '/api/v1/courses/{{ $course_id }}',
                    headers: {
                        'Authorization': 'Bearer ' + bearer,
                        'Accept': 'application/json',
                    }
                })
                .then(response => {
                    this.course_sections = response.data.course_sections
                }),
                axios({
                    method: 'get',
                    url: this.api_url + "/api/v1/students/{{session('data')['id']}}/courses/{{ $course_id }}/progress",
                    headers: {
                        'Authorization': 'Bearer ' + bearer,
                        'Accept': 'application/json',
                    }
                })
                .then(response => {
                    this.progress = response.data
                }),
                axios({
                    method: 'get',
                    url: this.api_url + '/api/v1/section-quizzes/{{ $content_id }}',
                    headers: {
                        'Authorization': 'Bearer ' + bearer,
                        'Accept': 'application/json',
                    }
                })
                .then(response => {
                    this.section_quiz = response.data
                    this.quiz = this.section_quiz.quiz
                    this.questions = this.quiz.questions
                    this.is_question_ready = true
                    this.length = this.questions.length

                    //ambil data user quiz
                    axios({
                        method: 'get',
                        url: this.api_url + '/api/v1/students/{{session('data')['id']}}/section-quizzes/' + this.section_quiz.id + '/run',
                        headers: {
                            'Authorization': 'Bearer ' + bearer,
                            'Accept': 'application/json',
                        }
                    })
                    .then(response => {
                        self = this
                        this.user_quiz = response.data
                        var maxTicks = this.quiz.time_limit;
                        if(this.$cookies.isKey('tickCount'+ this.user_quiz.id)){
                            var tickCount = this.$cookies.get('tickCount' + this.user_quiz.id)
                        }
                        else{
                            var tickCount = this.user_quiz.time_spent;
                        }
                        var distance = maxTicks - tickCount

                        var minutes = Math.floor(distance / 60)
                        var seconds = Math.floor(distance - (minutes * 60))
                        var tick = function(){
                            if (tickCount >= maxTicks){
                                // Stops the interval.
                                clearInterval(self.myInterval);

                                self.expiredQuiz()
                                document.getElementById("timer").innerHTML = "EXPIRED"
                                return
                            }
                        
                            /* The particular code you want to excute on each tick */
                            if(seconds == -1){
                                minutes -= 1
                                seconds = 59
                            }
                            if(minutes < 10 && seconds < 10){
                                document.getElementById("timer").innerHTML = "0" + minutes + ":" + "0" + seconds;   
                            }
                            else if(minutes < 10){
                                document.getElementById("timer").innerHTML = "0" + minutes + ":" + seconds;   
                            }
                            else if(seconds < 10){
                                document.getElementById("timer").innerHTML = minutes + ":" + "0" + seconds;   
                            }
                            else{
                                document.getElementById("timer").innerHTML = minutes + ":" + seconds;                         
                            }
                            self.$cookies.set('tickCount' + self.user_quiz.id, tickCount);
                            tickCount++;
                            self.time_spent = tickCount
                            seconds -= 1
                            
                        };
                        
                        // Start calling tick function every 1 second.
                        this.myInterval = setInterval(tick, 1000);
                        
                    })
                    .catch(error => {
                        window.location.href = '/course/'+ this.course_id + '/section-quizzes/' + this.section_quiz.slug
                    })
                })
            },
            methods: {
                next(question_id, answers) {
                    this.showLoading()
                    axios({
                        method: 'post',
                        url: this.api_url + '/api/v1/students/{{session('data')['id']}}/quizzes/' + this.user_quiz.id + '/answers',
                        headers: {
                            'Authorization' : 'Bearer ' + bearer,
                            'Accept' : 'application/json',
                        },
                        data: {
                            question_id: question_id,
                            answers: answers,
                            time_spent: this.time_spent
                        } 
                    })
                    .then(response => {
                        this.active_question += 1
                        this.hideLoading()
                    })
                },
                prev(question_id, answers) {
                    this.showLoading()
                    axios({
                        method: 'post',
                        url: this.api_url + '/api/v1/students/{{session('data')['id']}}/quizzes/' + this.user_quiz.id + '/answers',
                        headers: {
                            'Authorization' : 'Bearer ' + bearer,
                            'Accept' : 'application/json',
                        },
                        data: {
                            question_id: question_id,
                            answers: answers,
                            time_spent: this.time_spent
                        } 

                    })
                    .then(response => {
                        this.active_question -= 1
                        this.hideLoading()
                    })
                },
                submitQuiz(question_id, answers) {
                    this.showLoading()
                    //stop the timer
                    clearInterval(this.myInterval);
                    //submit the current question
                    axios({
                        method: 'post',
                        url: this.api_url + '/api/v1/students/{{session('data')['id']}}/quizzes/' + this.user_quiz.id + '/answers',
                        headers: {
                            'Authorization' : 'Bearer ' + bearer,
                            'Accept' : 'application/json',
                        },
                        data: {
                            question_id: question_id,
                            answers: answers,
                            time_spent: this.time_spent
                        }, 
                    })
                    .then(response => {
                        if(response.status == 201){
                            axios({
                                method: 'post',
                                url: this.api_url + '/api/v1/students/{{session('data')['id']}}/quizzes/' + this.user_quiz.id + '/done',
                                headers: {
                                    'Authorization' : 'Bearer ' + bearer,
                                    'Accept' : 'application/json',
                                },
                                data: {
                                    time_spent: this.time_spent
                                }
                            })
                            .then(response => {
                                this.result = response.data
                                this.is_result_ready = true
                                setTimeout(() => {
                                    resultModal()
                                }, 1000);
                                this.hideLoading()
                            })
                            .catch(error => {
                                warningModal()
                                this.hideLoading()
                            }) 
                        }
                    })                   
                },
                expiredQuiz(){
                    this.showLoading()
                    axios({
                        method: 'post',
                        url: this.api_url + '/api/v1/students/{{session('data')['id']}}/quizzes/' + this.user_quiz.id + '/done',
                        headers: {
                            'Authorization' : 'Bearer ' + bearer,
                            'Accept' : 'application/json'
                        },
                        data: {
                            time_spent: this.time_spent
                        }
                    })
                    .then(response => {
                        this.result = response.data
                        this.is_result_ready = true
                        setTimeout(() => {
                            resultModal()
                        }, 1000);
                        this.hideLoading()
                    })
                    .catch(error => {
                        warningExpiredModal()
                        this.hideLoading()
                    })
                },
                retakeQuiz(){
                    var current_section_index = _.findIndex(this.course_sections, {'id': this.section_quiz.course_section_id})
                    var current_section = this.course_sections[current_section_index]
                    
                    window.location.href = '/course/'+ current_section.course_id + '/section-quizzes/' + this.section_quiz.slug
                }
            }
        })
</script>
<script>
    function resultModal(){
        var myModal = document.getElementById('resultModal')
        var modal = bootstrap.Modal.getOrCreateInstance(myModal)
        modal.show()
    }

    function warningModal(){
        var myModal = document.getElementById('warningModal')
        var modal = bootstrap.Modal.getOrCreateInstance(myModal)
        modal.show()
    }

    function warningExpiredModal(){
        var myModal = document.getElementById('warningExpiredModal')
        var modal = bootstrap.Modal.getOrCreateInstance(myModal)
        modal.show()
    }
</script>
@endsection