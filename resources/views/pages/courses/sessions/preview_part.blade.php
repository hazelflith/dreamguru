@extends('layout.default')

@section('meta')
    @component('components.meta')
        @slot('title')
            Dreamguru
        @endslot

        @slot('description')
            Dreamaxtion helps companies and people maximize potential through Human Capital management system and psychometric
            assessment for personal and career growth.
        @endslot
    @endcomponent
@endsection

@section('css')
<style>

</style>
@endsection

@section('content')
<div class="row g-0">
    <div class="col-lg-9">
        <div class="main-padding-h main-padding-v" v-cloak>
            <div class="d-flex" v-if="!is_course_ready">
                <vue-skeleton-loader
                    :height="36"
                    :width="400"
                    color="rgba(52, 52, 52, 1)"
                    animation="fade"
                ></vue-skeleton-loader>
                <vue-skeleton-loader
                    :height="36"
                    :width="100"
                    color="rgba(52, 52, 52, 1)"
                    animation="fade"
                    class="ms-auto"
                ></vue-skeleton-loader>
            </div>
            <div class="d-flex" v-if="part">
                <h1 class="color-gold fw-bold">
                    @{{part.title}}
                </h1>
                <div class="ms-auto">
                    <h6 class="color-gold fw-bold">
                        Estimated Time
                    </h6>
                    <h6 class="color-gold text-end fw-bold">
                        @{{getTime(part.estimated_time)}}
                    </h6>
                </div>
            </div>
            <div class="lesson-img pt-4 pb-4 text-center" v-if="!is_course_ready">
                <vue-skeleton-loader
                    :height="500"
                    color="rgba(52, 52, 52, 1)"
                    animation="fade"
                    class="w-100"
                ></vue-skeleton-loader>
            </div>
            <div class="lesson-img pt-4 pb-4 text-center" v-if="is_course_ready">
                <iframe 
                    v-if="part.video" 
                    width="100%" 
                    height="500"
                    :src="'https://www.youtube.com/embed/' + part.video"
                ></iframe>
                <img v-if="part.picture" :src="api_url + '/storage/' + part.picture" height="100%">
            </div>
            <div v-if="part" v-html="part.text"></div>
            <vue-skeleton-loader
                :height="25"
                class="w-100 mb-2"
                color="rgba(52, 52, 52, 1)"
                animation="fade"
                v-if="!is_course_ready"
            ></vue-skeleton-loader>
            <vue-skeleton-loader
                :height="25"
                class="w-75 mb-2"
                color="rgba(52, 52, 52, 1)"
                animation="fade"
                v-if="!is_course_ready"
            ></vue-skeleton-loader>
            <vue-skeleton-loader
                :height="25"
                class="w-50 mb-2"
                color="rgba(52, 52, 52, 1)"
                animation="fade"
                v-if="!is_course_ready"
            ></vue-skeleton-loader>
            <vue-skeleton-loader
                :height="36"
                :width="100"
                color="rgba(52, 52, 52, 1)"
                animation="fade"
                v-if="!is_course_ready"
            ></vue-skeleton-loader>
        </div>
    </div>
    <div class="d-none d-lg-block col-lg-3">
        <toc :course_sections="course_sections" content_id="{{$content_id}}" v-if="is_course_ready" :preview=true ></toc>
        <toc-skeleton v-if="!is_course_ready"></toc-skeleton>
    </div>
</div>
@endsection

@section('js')
<script>
    var part = null
    'use strict';
    var app = new Vue({
        el: '#app',

        data() {
            return {
                course_sections: null,
                part: null,
                is_course_ready: false
            }
        },
        mounted: function () {
            axios({
                    method: 'get',
                    url: this.api_url + '/api/v1/courses/{{ $course_id }}',
                    headers: {
                        'Authorization': 'Bearer ' + bearer,
                        'Accept': 'application/json',
                    }
                })
            .then(response => {
                this.course_sections = response.data.course_sections
            }),
            axios({
                method: 'get',
                url: this.api_url + '/api/v1/parts/{{ $content_id }}',
                headers: {
                    'Authorization': 'Bearer ' + bearer,
                    'Accept': 'application/json',
                }
            })
            .catch(error => {
                lockedModal()
            })
            .then(response => {
                this.part = response.data
                part = this.part
                this.is_course_ready = true
                document.title = this.part.title
                axios({
                    method: 'post',
                    url: this.api_url + "/api/v1/students/{{session('data')['id']}}/parts/" + this.part.id,
                    headers: {
                        'Authorization': 'Bearer ' + bearer,
                        'Accept': 'application/json',
                    },
                    data: {
                        status: 'in progress'
                    }
                })
                .then(response => {
                    //
                })
            })
        },
        methods: {
            partComplete(){
                axios({
                    method: 'put',
                    url: this.api_url + "/api/v1/students/{{session('data')['id']}}/parts/" + part.id,
                    headers: {
                        'Authorization': 'Bearer ' + bearer,
                        'Accept': 'application/json',
                    },
                    data: {
                        status: 'completed'
                    }
                })
                .then(response => {
                    //check is course can be completed
                    axios({
                        method: 'get',
                        url: this.api_url + "/api/v1/students/{{session('data')['id']}}/courses/" + this.course_sections[0].course_id + "/complete",
                        headers: {
                            'Authorization': 'Bearer ' + bearer,
                            'Accept': 'application/json',
                        },
                    })
                    .then(response => {
                       
                        if(response.status == 200){
                            var current_section_index = _.findIndex(this.course_sections, {'id': this.part.course_section_id})
                            var current_section = this.course_sections[current_section_index]
                            
                            var part_quiz_index = _.findIndex(current_section.section_content_orders, {'content_id': this.part.slug})
                            var next_part = current_section.section_content_orders[part_quiz_index + 1]
                            var first_page = this.course_sections[0]
                            var first_content = first_page.section_content_orders[0]

                            if(part_quiz_index + 1 == current_section.section_content_orders.length) {
                                if(current_section_index + 1 == this.course_sections.length) {
                                    // Redirect ke halaman pertama
                                    if(first_content.endpoint == 'section_quizzes') {
                                        window.location.href = '/course/'+ current_section.course_id + '/section-quizzes/' + first_content.content_id
                                    }else{
                                        window.location.href = '/course/'+ current_section.course_id + '/' + first_content.endpoint + '/' + first_content.content_id
                                    }
                                }else{
                                    var next_section = this.course_sections[current_section_index + 1]
                                    next_part = next_section.section_content_orders[0]
                                }
                            }

                            if(next_part.endpoint == 'section_quizzes') {
                                window.location.href = '/course/'+ current_section.course_id + '/section-quizzes/' + next_part.content_id
                            }else{
                                window.location.href = '/course/'+ current_section.course_id + '/' + next_part.endpoint + '/' + next_part.content_id
                            }
                        }
                        else{
                            //halaman complete
                            window.location.href = '/complete?course=' + this.course_sections[0].course_id
                        }
                    })
                    .catch(error => {
                        var current_section_index = _.findIndex(this.course_sections, {'id': this.part.course_section_id})
                        var current_section = this.course_sections[current_section_index]
                        
                        var part_quiz_index = _.findIndex(current_section.section_content_orders, {'content_id': this.part.slug})
                        var next_part = current_section.section_content_orders[part_quiz_index + 1]
                        var first_page = this.course_sections[0]
                        var first_content = first_page.section_content_orders[0]

                        if(part_quiz_index + 1 == current_section.section_content_orders.length) {
                            if(current_section_index + 1 == this.course_sections.length) {
                                // Redirect ke halaman pertama
                                if(first_content.endpoint == 'section_quizzes') {
                                    window.location.href = '/course/'+ current_section.course_id + '/section-quizzes/' + first_content.content_id
                                }else{
                                    window.location.href = '/course/'+ current_section.course_id + '/' + first_content.endpoint + '/' + first_content.content_id
                                }
                            }else{
                                var next_section = this.course_sections[current_section_index + 1]
                                next_part = next_section.section_content_orders[0]
                            }
                        }

                        if(next_part.endpoint == 'section_quizzes') {
                            window.location.href = '/course/'+ current_section.course_id + '/section-quizzes/' + next_part.content_id
                        }else{
                            window.location.href = '/course/'+ current_section.course_id + '/' + next_part.endpoint + '/' + next_part.content_id
                        }
                    })    
                })
            },
            getTime(data) {
                var date = new Date(data * 1000).toISOString().substr(14, 5)
                return date
            },
        },
    })
</script>
<script>
    function lockedModal(){
        var myModal = document.getElementById('lockedModal');
        var modal = bootstrap.Modal.getOrCreateInstance(myModal)
        modal.show()
    }
</script>
@endsection
