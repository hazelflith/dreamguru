@extends('layout.default')

@section('meta')
    @component('components.meta')
        @slot('title')
             Learn Angular | Session 1
        @endslot

        @slot('description')
            Dreamaxtion helps companies and people maximize potential through Human Capital management system and psychometric assessment for personal and career growth.
        @endslot
    @endcomponent
@endsection

@section('css')
    <style> 
        
    </style>
@endsection

@section('content')
<div class="row g-0">
    <div class="col-lg-9">
        <div class="main-padding-h main-padding-v" v-cloak>
            <div class="d-flex align-items-center">
                <h1 class="color-gold fw-bold m-0" v-if="is_page_ready">
                    @{{quiz.title}}
                </h1>
                <vue-skeleton-loader
                    :height="50"
                    :width="200"
                    color="rgba(52, 52, 52, 1)"
                    animation="fade"
                    v-if="!is_page_ready"
                ></vue-skeleton-loader>
                <div class="ms-auto">
                    <h3 class="color-gold fw-bold m-0" v-if="is_page_ready">
                        @{{quiz.total_question}} Questions
                    </h3>
                    <vue-skeleton-loader
                        :height="50"
                        :width="200"
                        color="rgba(52, 52, 52, 1)"
                        animation="fade"
                        v-if="!is_page_ready"
                    ></vue-skeleton-loader>
                    <div v-if="is_page_ready">
                        <h3 v-if="quizInProgress" class="color-gold fw-bold m-0 text-end">
                            @{{getTime(quiz.quiz.time_limit - user_quiz.time_spent)}}
                        </h3>
                        <h3 v-else class="color-gold fw-bold m-0 text-end">
                            @{{ getTime(quiz.quiz.time_limit) }}
                        </h3>
                    </div>
                </div>
            </div>
            <h6 class="pt-3" v-if="is_page_ready">
                <div class="d-flex" v-if="quiz.max_attempt != 0">
                    <span class="color-gold fw-bold me-2" >
                        @{{ quiz.max_attempt - user_quiz_attempts }}
                    </span> 
                    <p>
                        Attempts Left!
                    </p>
                </div>
                <span class="color-gold fw-bold" v-else>
                    Unlimited Attempt!
                </span>
            </h6>
            <h6 class="pt-5" v-if="is_page_ready">
                @{{ quiz.desc }}
            </h6>
            <h6 class="pt-5" v-if="!is_page_ready">
                <vue-skeleton-loader
                    :height="25"
                    :width="300"
                    color="rgba(52, 52, 52, 1)"
                    animation="fade"
                ></vue-skeleton-loader>
                <vue-skeleton-loader
                    :height="25"
                    :width="200"
                    color="rgba(52, 52, 52, 1)"
                    animation="fade"
                ></vue-skeleton-loader>
                <vue-skeleton-loader
                    :height="25"
                    :width="150"
                    color="rgba(52, 52, 52, 1)"
                    animation="fade"
                ></vue-skeleton-loader>
            </h6>
            <div v-if="is_page_ready">
                <div v-if="user_quiz">
                    <a  v-if="user_quiz.status == 'in_progress'" href="javascript:void(0)"
                        class="btn d-inline-block fill-gold fw-semi mt-4" role="button" @click="continueQuiz()" 
                    >
                        Continue Quiz
                    </a>
                    <a  v-else href="javascript:void(0)"
                        class="btn d-inline-block fill-gold fw-semi mt-4" role="button" @click="startQuiz()"
                    >
                        Start Quiz
                    </a>
                </div>
                <div v-if="user_quiz == null">
                    <a  href="javascript:void(0)"
                        class="btn d-inline-block fill-gold fw-semi mt-4" role="button" @click="startQuiz()"
                    >
                        Start Quiz
                    </a>
                </div>
            </div>
            <vue-skeleton-loader
                :height="36"
                :width="100"
                color="rgba(52, 52, 52, 1)"
                animation="fade"
                class="mt-4"
                v-if="!is_page_ready"
            ></vue-skeleton-loader>
        </div>
    </div>
    <div class="d-none d-lg-block col-lg-3">
        <toc :course_sections="course_sections" content_id="{{$content_id}}" :progress="progress" v-if="is_page_ready"></toc>
        <toc-skeleton v-if="!is_page_ready"></toc-skeleton>
    </div>
</div>
{{-- MODAL --}}
<div class="modal fade" id="restrictionModal" tabindex="-1" aria-labelledby="restrictionModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <h2 class="color-gold fw-bold text-center">
                    Sorry!
                </h2>
                <p class="color-gold text-center mt-4">
                    Kesempatan untuk mengerjakan quiz sudah habis
                </p>
                <button type="button" class="w-100 btn d-inline-block fill-gold fw-semi mt-4" data-bs-dismiss="modal">
                    Mengerti
                </button>
            </div>
        </div>
    </div>
</div>
{{-- MODAL --}}
<div class="modal fade" id="lockedModal" tabindex="-1" aria-labelledby="lockedModalLabel" aria-hidden="true" data-bs-backdrop="static">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="lockedModalLabel">
                    Part/Quiz Locked
                </h5>
            </div>
            <div class="modal-body">
                <p>
                    You need to enroll in this course to unlock this part/quiz
                </p>
            </div>
            <div class="modal-footer">
                <a href="/course/{{$course_id}}" class="btn fill-gold fw-semi">
                    Go To Course Page
                </a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    var quiz = null
    'use strict';
    var app = new Vue({
        el: '#app',
        data() {
            return {
                course_sections: null,
                quiz: null,
                user_quiz: null,
                user_quiz_attempts: 0,
                is_page_ready: false,
                progress: '',
            }
        },
        mounted: function(){
            axios({
                method: 'get',
                url: this.api_url + '/api/v1/courses/{{ $course_id }}',
                headers: {
                    'Authorization': 'Bearer ' + bearer,
                    'Accept': 'application/json',
                }
            })
            .then(response => {
                this.course_sections = response.data.course_sections
            }),
            axios({
                method: 'get',
                url: this.api_url + "/api/v1/students/{{session('data')['id']}}/courses/{{ $course_id }}/progress",
                headers: {
                    'Authorization': 'Bearer ' + bearer,
                    'Accept': 'application/json',
                }
            })
            .then(response => {
                this.progress = response.data
            }),
            axios({
                method: 'get',
                url: this.api_url + '/api/v1/section-quizzes/{{ $content_id }}',
                headers: {
                    'Authorization': 'Bearer ' + bearer,
                    'Accept': 'application/json',
                }
            })
            .then(response => {
                
                this.quiz = response.data
                quiz = this.quiz

                //get latest user quizzes info
                axios({
                    method: 'get',
                    url: this.api_url + '/api/v1/students/{{session('data')['id']}}/section-quizzes/' + quiz.id + '/run',
                    headers: {
                        'Authorization': 'Bearer ' + bearer,
                        'Accept': 'application/json',
                    }
                })
                .then(response => {
                    this.user_quiz = response.data
                    this.user_quiz_attempts = this.user_quiz.attempts
                    this.is_page_ready = true
                })
                .catch(error => {
                    axios({
                        method: 'get',
                        url: this.api_url + '/api/v1/students/{{session('data')['id']}}/section-quizzes/' + quiz.id,
                        headers: {
                        'Authorization': 'Bearer ' + bearer,
                        'Accept': 'application/json',
                        }
                    })
                    .then(response => {
                        this.user_quiz = response.data
                        this.user_quiz_attempts = this.user_quiz.attempts
                        this.is_page_ready = true
                    })
                    .catch(error => {
                        this.user_quiz_attempts = 0
                        this.is_page_ready = true
                    })
                })
            })
            .catch(error => {
                lockedModal()
            })
        },
        computed: {
            quizInProgress(){
                if(this.user_quiz){
                    if(this.user_quiz.status == 'in_progress'){
                        return true
                    }
                    else{
                        return false
                    }
                }
            }
        },
        methods: {
            startQuiz() {
                axios({
                    method: 'post',
                    url: this.api_url + '/api/v1/section-quizzes/' + this.quiz.id + '/start',
                    headers: {
                        'Authorization': 'Bearer ' + bearer,
                        'Accept': 'application/json',
                    }
                })
                .then(response => {
                    window.location.href = '/course/{{ $course_id }}/section-quizzes/{{ $content_id }}/run'
                })
                .catch(error => {
                    restrictionModal()
                })
            },

            continueQuiz() {
                window.location.href = '/course/{{ $course_id }}/section-quizzes/{{ $content_id }}/run'
            },

            getTime(data) {
                var date = new Date(data * 1000).toISOString().substr(14, 5)
                return date
            },
        }
    })
</script>
<script>
    function restrictionModal(){
        var myModal = document.getElementById('restrictionModal');
        var modal = bootstrap.Modal.getOrCreateInstance(myModal)
        modal.show()
    }
</script>
<script>
    function lockedModal(){
        var myModal = document.getElementById('lockedModal');
        var modal = bootstrap.Modal.getOrCreateInstance(myModal)
        modal.show()
    }
</script>
@endsection