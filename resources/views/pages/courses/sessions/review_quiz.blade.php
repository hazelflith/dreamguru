@extends('layout.default')

@section('meta')
    @component('components.meta')
        @slot('title')
             Review Quiz
        @endslot

        @slot('description')
            Dreamaxtion helps companies and people maximize potential through Human Capital management system and psychometric assessment for personal and career growth.
        @endslot
    @endcomponent
@endsection

@section('css')
    <style> 
        
    </style>
@endsection

@section('content')
<div class="main-padding-h main-padding-v" v-cloak>
    <h3 class="color-gold fw-semi">
        Quiz Review
    </h3>
    <h6 class="color-muted" v-if="is_quiz_ready">
        @{{quiz.section_quiz.title}}> REVIEW
    </h6>
</div>
<div class="row g-0" v-cloak>
    <div class="col">
        <div class="main-padding-h">
            <div v-if="error_show" class="alert alert-danger" role="alert">
                <div v-for="value in error_show">
                    <p class="color-red">
                        @{{value[0]}}
                    </p>
                </div>                
            </div>
            <div class="d-flex align-items-center mb-3" v-cloak>
                <h4 class="color-gold fw-semi me-auto" v-if="is_quiz_ready">
                    @{{quiz.section_quiz.title}}
                </h3>
            </div>
            <h6 v-if="quiz.is_success" class="text-capitalize mb-2" v-if="is_quiz_ready">
                <span class="color-green">SUCCESS</span>
            </h6>
            <h6 v-if="!quiz.is_success" class="text-capitalize color-red mb-2" v-if="is_quiz_ready">
                <span class="color-red">FAILED</span>
            </h6>
            <div class="mb-3 d-flex">
                <div class="card h-100 card-container card-medium p-3 me-2" :class="{ wrong: !quiz.is_success, correct: quiz.is_success }">
                    <h4 class="mb-3">
                        Score 
                    </h4>
                    <div class="d-flex align-items-center">
                        <h2 v-if = "quiz.is_success">
                            @{{quiz.user_score}}
                        </h2>
                        <h2 v-else>
                            @{{quiz.user_score}} / @{{quiz.section_quiz.point_requirement}} 
                        </h2>
                        <i v-if="quiz.is_success" class="fa-solid fa-star ms-auto color-gold" style="font-size: 24px"></i>
                    </div>
                </div>
                <div class="card h-100 card-container card-medium p-3 me-2">
                    <h4 class="mb-3">
                        Correct: 
                    </h4>
                    <div class="d-flex align-items-center">
                        <h2>
                             @{{quiz.total_true}}
                        </h2>
                        <i class="fa-solid fa-circle-check ms-auto color-green" style="font-size: 24px"></i>
                    </div>
                </div>
                <div class="card h-100 card-container card-medium p-3">
                    <h4 class="mb-3">
                        Wrong:
                    </h4>
                    <div class="d-flex align-items-center">
                        <h2>
                            @{{quiz.total_false}}
                        </h2>
                        <i class="fa-solid fa-circle-xmark color-red ms-auto" style="font-size: 24px"></i>
                    </div>
                </div>
            </div>
            
            <div class="d-flex align-items-center">
                <h6 class="color-gold me-3 mb-0" style="white-space: nowrap">
                    QUESTIONS
                </h6>
                <div class="hr"></div>
            </div>
            <div class="mb-3"></div>
            <div class="p-2">
                <div v-for="(question , index) in quiz.user_quiz_questions" :key="question.id">
                    <div class="mb-3 review-container" :class="{ wrong: !question.is_true, correct: question.is_true }">
                        <div class="d-flex flex-wrap">
                            <h5 class="mb-1">
                                <span class="color-gold">@{{index + 1}}.</span> @{{question.question.question}}
                            </h5>
                            <h5 class="color-muted ms-md-auto me-3">
                                Point Earned: @{{question.point}}
                            </h5>
                        </div>
                        <p v-if="question.question.question_type == 'single_answer'" class="mb-1">
                            ANSWERS (Single Answer) :
                        </p>
                        <p v-else class="mb-1">
                            ANSWERS (Multiple Answer) :
                        </p>
                        <ul>
                            <li  v-for="answer in question.question.answers" :key="answer.id">
                                @{{answer.answer}} 
                                <i v-if="answer.is_true" class="fa-solid fa-circle-check ms-2"></i>
                            </li>
                        </ul>
                        <div class="d-flex">
                            <div v-if="question.is_true" class="me-2">
                                <i class="fa-solid fa-square-check color-green" style="font-size: 24px"></i>
                            </div>
                            <div v-else class="me-2">
                                <i class="fa-solid fa-rectangle-xmark color-red" style="font-size: 24px"></i>
                            </div>
                            <p class="me-2">
                                Your Answer : 
                            </p>
                            <p class="me-2" v-for="answer in question.user_quiz_question_answers">
                                @{{answer.answer}}
                            </p>
                            <p v-if="question.user_quiz_question_answers.length == 0">
                                No Answer
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mb-3"></div>
        </div>
    </div>
</div>

@endsection

@section('js')
    <script>
        'use strict';
        var app = new Vue({
            el: '#app',

            data() {
                return {
                    is_quiz_ready: false,
                    quiz: [],
                    error_show: null,
                }
            },
            mounted: function() {

                axios({
                    method: 'get',
                    url: this.api_url + "/api/v1/students/{{session('data')['id']}}/quizzes/{{ $user_quiz_id }}",
                    headers: {
                        'Authorization' : 'Bearer ' + bearer,
                        'Accept' : 'application/json',
                    },
                })
                .then(response => {
                    this.quiz = response.data
                    this.is_quiz_ready = true
                })
            },
            methods: {
                
            },
        })
    </script>
@endsection