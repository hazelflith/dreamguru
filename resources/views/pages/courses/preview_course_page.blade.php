@extends('layout.default')

@section('meta')
    @component('components.meta')
        @slot('title')
             Dreamguru | Course
        @endslot

        @slot('description')
            Dreamaxtion helps companies and people maximize potential through Human Capital management system and psychometric assessment for personal and career growth.
        @endslot
    @endcomponent
@endsection

@section('css')
    <style> 
        
    </style>
@endsection

@section('content')
<div v-if="is_course_ready">
    <course-highlight :course="course" :show="show" :session_id="session_id" :preview="true" v-if="is_course_ready"></course-highlight>
    <course-tab :course="course" :session_id="session_id" v-if="is_course_ready" :preview="true"></course-tab>
</div>
<highlight-skeleton v-if="!is_course_ready"></highlight-skeleton>
<div class="main-padding-v main-padding-h">
    
</div>
@endsection

@section('js')
    <script>
       'use strict';
        var app = new Vue({
            el: '#app',

            data() {
                return {
                    course: null,
                    is_course_ready: false,
                    session_id: "{{session('data')['id']}}" ,
                    show:{
                        price:'',
                        discount_price:'',
                    }
                }
            },
            mounted: function() {
                axios({
                    method: 'get',
                    url:  this.api_url + '/api/v1/courses/{{ $course_id }}',
                    headers: {
                    'Authorization' : 'Bearer ' + bearer,
                    'Accept' : 'application/json',
                    },
                })
                .then(response => {
                    this.course = response.data
                    this.show.price = Number(this.course.price).toLocaleString('id');
                    this.show.discount_price = Number(this.course.discount_price).toLocaleString('id');
                    this.is_course_ready = true
                })
            },
        }) 
    </script>
@endsection