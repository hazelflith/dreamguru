@extends('layout.default')

@section('meta')
    @component('components.meta')
    
        @slot('title')
             Dreamguru
        @endslot

        @slot('description')
            Dreamaxtion helps companies and people maximize potential through Human Capital management system and psychometric assessment for personal and career growth.
        @endslot
    @endcomponent
@endsection

@section('css')
    <style> 
        
    </style>
@endsection

@section('content')
<div class="comingsoon text-center">
    <div class="heading">
        Guru.
    </div>
    <h1>
        Coming Soon!
    </h1>
    <h2>
        Dreamguru is still on development
    </h2>
</div>

@endsection

@section('js')
    <script>
        'use strict';
        var app = new Vue({
            el: '#app',
        })
    </script>
@endsection