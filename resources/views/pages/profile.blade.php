@extends('layout.default')

@section('meta')
    @component('components.meta')
        @slot('title')
             Dreamguru | Profile
        @endslot

        @slot('description')
            Dreamaxtion helps companies and people maximize potential through Human Capital management system and psychometric assessment for personal and career growth.
        @endslot
    @endcomponent
@endsection

@section('css')

@endsection

@section('content')
<div class="main-padding-h main-padding-v" v-cloak>
    <h3 class="color-gold fw-semi">
        User Profile
    </h3> 
    <h6 class="color-muted mb-5">
        CUSTOMIZE YOUR PROFILE
    </h6>
    <div v-if="error_show2" class="alert alert-danger" role="alert">
        <div>
            <p class="color-red">
                @{{error_show2}}
            </p>
        </div>                
    </div>
        <div class="d-flex flex-wrap justify-content-center">
            <div class="">
                <div class="">
                    <div v-if="ready">
                        <img 
                            v-if="show_avatar" 
                            :src="api_url + '/storage/' + show_avatar" 
                            class="avatar img-round mb-3 me-5" 
                            alt="avatar" 
                            style="height:160px; width:160px;"
                        >
                        <img 
                            v-else 
                            src="/img/placeholder.png" 
                            class="avatar img-round mb-3 me-5" 
                            alt="avatar" 
                            style="height:160px; width:160px;"
                        >
                    </div>
                    <vue-skeleton-loader
                        type="circle"
                        :height="160"
                        :width="160"
                        v-if="!ready"
                        color="rgba(52, 52, 52, 1)"
                        animation="fade"
                    ></vue-skeleton-loader>
                </div>
            </div>
            <div class="">
                <h1 class="color-gold mb-2">
                    @{{ info.name }}
                    <vue-skeleton-loader
                        :height="50"
                        :width="300"
                        v-if="!ready"
                        color="rgba(52, 52, 52, 1)"
                        animation="fade"
                    ></vue-skeleton-loader>
                </h1>
                <p class="d-flex">
                    <span v-if="ready" class="fw-semi me-1">Username : </span> @{{ info.username }}
                    <vue-skeleton-loader
                        :height="20"
                        :width="150"
                        v-if="!ready"
                        color="rgba(52, 52, 52, 1)"
                        animation="fade"
                        class="mb-2"
                    ></vue-skeleton-loader>
                </p>
                <p class="text-capitalize d-flex">
                    <span v-if="ready" class="fw-semi me-1">Gender : </span>@{{ info.gender }}
                    <vue-skeleton-loader
                        :height="20"
                        :width="150"
                        v-if="!ready"
                        color="rgba(52, 52, 52, 1)"
                        animation="fade"
                        class="mb-2"
                    ></vue-skeleton-loader>
                </p>
                @csrf
                <p class="d-flex">
                    <span v-if="ready" class="fw-semi me-1">Phone Number : </span>@{{ info.phone_number }}
                    <vue-skeleton-loader
                        :height="20"
                        :width="150"
                        v-if="!ready"
                        color="rgba(52, 52, 52, 1)"
                        animation="fade"
                        class="mb-2"
                    ></vue-skeleton-loader>
                </p>
                <p class="d-flex">
                    <span v-if="ready" class="fw-semi me-1">Date of Birth : </span>@{{ info.date_of_birth }}
                    <vue-skeleton-loader
                        :height="20"
                        :width="150"
                        v-if="!ready"
                        color="rgba(52, 52, 52, 1)"
                        animation="fade"
                        class="mb-2"
                    ></vue-skeleton-loader>
                </p>
                <div class="form-group mb-2">
                    <label v-if="ready" class=" control-label">
                        Address:
                    </label>
                        <p>
                            @{{ info.address }}
                        </p>
                        <vue-skeleton-loader
                            :height="50"
                            v-if="!ready"
                            class="w-100"
                            color="rgba(52, 52, 52, 1)"
                            animation="fade"
                            class="mb-2"
                        ></vue-skeleton-loader>
                </div>
            </div>
            <div class="ms-auto">
                <a href="javascript:void(0)" class="btn fill-gold fw-semi" data-bs-toggle="modal" data-bs-target="#editProfileModal">Edit Profile</a>
            </div>
        </div>

        <div class="personal-info" v-if="ready">
        </div>
    {{-- MODAL --}}
    <div class="modal fade" id="editProfileModal" tabindex="-1" aria-labelledby="editProfileModalLabel"
        aria-hidden="true"
        >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editProfileModalLabel">
                        Edit Profile
                    </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                        aria-label="Close"
                    ></button>
                </div>
                <div class="modal-body">
                    <div v-if="error_show" class="alert alert-danger" role="alert">
                        <div v-for="value in error_show">
                            <p class="color-red">
                                @{{value[0]}}
                            </p>
                        </div>                
                    </div>
                    <div class="personal-info">
                        <div class="text-center">
                            <img 
                                v-if="show_avatar" 
                                :src="api_url + '/storage/' + show_avatar" 
                                class="avatar img-round mb-3" 
                                alt="avatar" 
                                style="height:160px; width:160px;"
                            >
                            <img 
                                v-else 
                                src="/img/placeholder.png" 
                                class="avatar img-round mb-3" 
                                alt="avatar" 
                                style="height:160px; width:160px;"
                            >
                            <h6 class="mb-3">
                                Upload your photo here
                            </h6>
                        </div>
                        <input type="file" @change="uploadAvatar" accept="image/png, image/jpeg" class="form-control form-dark mt-2">
                        <div id="imageHelp" class="form-text mb-2">
                            Images Max Size 1MB & Use 1:1 Ratio Images
                        </div>
                        <form class="form-horizontal" v-if="user">
                            @csrf
                            <div class="form-group mb-2">
                                <label class="control-label">
                                    Full name:
                                </label>
                                <div class="">
                                    <input class="form-control form-dark" type="text" v-model="user.profile.name">
                                </div>
                            </div>
                            <div class="form-group mb-2">
                                <label class=" control-label">
                                    Phone Number:
                                </label>
                                <div class="">
                                    <input placeholder="Input your phone number here" class="form-control form-dark" type="number" v-model="user.profile.phone_number">
                                </div>
                            </div>
                            <div class="form-group mb-2" v-if="user">
                                <label class="control-label">
                                    Date of birth:
                                </label>
                                <div>
                                    <input type="date" v-model="user.profile.date_of_birth" class="form-control form-dark">
                                </div>
                            </div>
                            <div class="form-group mb-2" v-if="user">
                                <label class="control-label">
                                    Gender:
                                </label>
                                <div class="">
                                    <select 
                                        class="form-control form-dark" 
                                        name="gender" 
                                        v-model="user.profile.gender"
                                    >
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group mb-2">
                                <label class=" control-label">
                                    Address:
                                </label>
                                <div class="">
                                    <textarea placeholder="Input your address here" class="form-control form-dark"  v-model="user.profile.address"> </textarea>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary outline-gold me-2"
                        data-bs-dismiss="modal"
                    >
                        Cancel
                    </button>
                    <a href="javascript:void(0)" class="btn fill-gold fw-semi" @click="updateDetail()">
                        Save
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        @if(Session::has("message"))
        <div class="alert alert-success" role="alert">
            {{Session::get('message')}}
        </div>
        @endif
        @if(Session::has("error"))
            <div class="alert alert-danger" role="alert">
                @foreach(Session::get('error') as $message)
                    <p class="color-red">
                        {{ucfirst($message[0])}}
                    </p>
                @endforeach
            </div>
        @endif
        <div class="">
            <div class="d-flex align-items-center pt-4 mb-3">
                <h6 class="color-gold me-3 mb-0" style="white-space: nowrap">
                    CHANGE EMAIL
                </h6>
                <div class="hr"></div>
            </div>
            <form class="mt-3" action="/student/email" method="post">
                @csrf
                <div>
                    <div>
                        <div class="form-group mb-2">
                            <label class=" control-label">
                                Email:
                            </label>
                            <div v-if="user" class="mb-2">
                                <input class="form-control" type="email" name="email" v-model="user.email" disabled>
                            </div>
                            <a href="javascript:void(0)" class="btn fill-gold fw-semi" data-bs-toggle="modal" data-bs-target="#changeEmailModal">Change Email</a>
                            <vue-skeleton-loader
                                :height="36"
                                v-if="!user"
                                class="w-100"
                                color="rgba(52, 52, 52, 1)"
                                animation="fade"
                            ></vue-skeleton-loader>
                            {{-- MODAL --}}
                            <div class="modal fade" id="changeEmailModal" tabindex="-1" aria-labelledby="changeEmailModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="changeEmailModalLabel">
                                                Change Email
                                            </h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"
                                            ></button>
                                        </div>
                                        <div class="modal-body">
                                            <div v-if="error_show" class="alert alert-danger" role="alert">
                                                <div v-for="value in error_show">
                                                    <p class="color-red">
                                                        @{{value[0]}}
                                                    </p>
                                                </div>                
                                            </div>
                                            <label class=" control-label">
                                                Email:
                                            </label>
                                            <div v-if="user" class="mb-2">
                                                <input class="form-control form-dark" type="email" name="email" v-model="user.email" required>
                                            </div>
                                            <div>
                                                <div class="form-group mb-2">
                                                    <label class=" control-label">
                                                        Enter Your Current Password:
                                                    </label>
                                                    <div class="">
                                                      <input class="form-control form-dark" type="password" name="current_password" value="" required>
                                                    </div>
                                                  </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary outline-gold me-2"
                                                data-bs-dismiss="modal"
                                            >
                                                Cancel
                                            </button>
                                            <input type="submit" class="btn fill-gold fw-semi" value="Change Email"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
    
        </div>
        <div class="">
            <div class="d-flex align-items-center pt-4 mb-3">
                <h6 class="color-gold me-3 mb-0" style="white-space: nowrap">
                    CHANGE PASSWORD
                </h6>
                <div class="hr"></div>
            </div>
            <form class="mt-3" action="/student/password" method="post">
                @csrf
                <div>
                    <div >
                        <div class="form-group mb-2">
                            <a href="javascript:void(0)" class="btn fill-gold fw-semi" data-bs-toggle="modal" data-bs-target="#changePasswordModal">Change Password</a>
                            {{-- MODAL --}}
                            <div class="modal fade" id="changePasswordModal" tabindex="-1" aria-labelledby="changePasswordModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="changePasswordModalLabel">
                                                Change Password
                                            </h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"
                                            ></button>
                                        </div>
                                        <div class="modal-body">
                                            <div v-if="error_show" class="alert alert-danger" role="alert">
                                                <div v-for="value in error_show">
                                                    <p class="color-red">
                                                        @{{value[0]}}
                                                    </p>
                                                </div>                
                                            </div>
                                            <label class=" control-label">
                                                Enter Your Current Password:
                                            </label>
                                            <div class="mb-2">
                                                <input class="form-control form-dark" type="password" name="current_password" minlength="8" value="" required>
                                            </div>
                                            <div>
                                                <div class="form-group mb-2">
                                                    <label class=" control-label">
                                                        New Password:
                                                    </label>
                                                    <div>
                                                        <input class="form-control form-dark" type="password" name="new_password" value="" minlength="8" required>
                                                        <div id="passwordHelp" class="form-text">
                                                            Password must be at least having 8 characters. But having a mixture of uppercase, lowercase, 
                                                            number and even symbols will make your password more secure
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="form-group mb-2">
                                                    <label class=" control-label">
                                                        Confirm New Password:
                                                    </label>
                                                    <div class="">
                                                        <input class="form-control form-dark" type="password" name="new_password_confirmation" value="" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary outline-gold me-2"
                                                data-bs-dismiss="modal"
                                            >
                                                Cancel
                                            </button>
                                            <input type="submit" class="btn fill-gold fw-semi me-2" value="Change Password">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script>
        'use strict';
        var app = new Vue({
            el: '#app',

            data() {
                return {
                    info: {
                        name:'',
                        username:'',
                        gender:'',
                        date_of_birth:'',
                        address:'',
                        role:'',
                        phone_number:''
                    },
                    user: null,
                    show_avatar:'',
                    avatar: null,
                    error_show: null,
                    error_show2: null,
                    ready: false,
                }
            },
            mounted: function(){
                axios({
                    method: 'get',
                    url: this.api_url + '/api/v1/users/self',
                    headers: {
                        'Authorization': 'Bearer ' + bearer,
                        'Accept': 'application/json',
                    }
                })
                .then(response => {
                    this.user = response.data.data
                    

                    this.info.name = this.user.profile.name
                    this.info.username = this.user.username
                    this.info.gender = this.user.profile.gender
                    var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'}
                    this.info.date_of_birth = new Date(this.user.profile.date_of_birth).toLocaleString('id-ID', options)
                    this.info.address = this.user.profile.address
                    this.info.phone_number = this.user.profile.phone_number
                    this.info.role = this.user.role
                    
                    this.show_avatar = this.user.profile.avatar
                    this.ready = true
                })
                .catch(function (error) {
                    if (error.response) {
                    }
                })

            },
            methods: {
                updateDetail(){
                    this.showLoading()
                    var self = this
                    var formData = new FormData()
                    formData.append('name', this.user.profile.name)
                    formData.append('date_of_birth', this.user.profile.date_of_birth)
                    formData.append('gender', this.user.profile.gender)
                    formData.append('phone_number', this.user.profile.phone_number)
                    if(this.avatar !=  null){
                        formData.append("avatar", this.avatar)
                    }
                    if(this.user.profile.address !=  null){
                        formData.append("address", this.user.profile.address)
                    }
                    axios({
                        method: 'post',
                        url: this.api_url + '/api/v1/users/self/profile',
                        headers: {
                            'Authorization': 'Bearer ' + bearer,
                            'Accept': 'application/json',
                        },
                        data: formData,
                    })
                    .then(response => {
                        this.hideLoading()
                        window.location.href = "{{url()->current()}}"
                    })
                    .catch(function (error) {
                        if (error.response) {
                            console.log(error.response.data.errors)
                            self.error_show = error.response.data.errors
                            window.location.href = '#'
                            self.hideLoading()
                        }
                    })
                },
                uploadAvatar(event){
                    if(event.target.files[0].size > 1048576){
                        window.location.href = "#"
                        this.error_show2 = "File is too big! Max size is 1MB"
                        event.target.files[0] = null;
                        this.avatar = '';
                    }
                    if(event.target.files[0].type == 'image/jpeg' || event.target.files[0].type == 'image/png'){
                        this.avatar = event.target.files[0]
                    }
                    else{
                        window.location.href = "#"
                        this.error_show2 = "Wrong file extension!"
                        event.target.files[0] = null;
                        this.avatar = '';
                        
                    }
              
                },
            },
        })
    </script>
    {{-- <script type="text/javascript">
        $(document).ready(function() {
            $('.datepicker').pickadate({
                selectMonths: true,
                selectYears: true,
                formatSubmit: 'yyyy-mm-dd',
                min: [1900,1,01],
                max: -6205,
                closeOnSelect: false,
                closeOnClear: false,
            });
        });
    </script> --}}
@endsection