@extends('layout.default')
@section('meta')
    @component('components.meta')
        @slot('title')
             Dreamguru | Course Completed!
        @endslot

        @slot('description')
            Dreamaxtion helps companies and people maximize potential through Human Capital management system and psychometric assessment for personal and career growth.
        @endslot
    @endcomponent
@endsection

@section('css')
    <style> 
        
    </style>
@endsection

@section('content')
<div class="redirect-container">
    <div class="payment-redirect text-center ">
        <img src="/img/complete.png" alt="complete-image" width="35%">
        <h1 class="color-gold fw-semi">
            Course Completed!
        </h1>
        <h4 class="color-white ">
            Congratulations on your completion
        </h4>
        <div class="d-flex justify-content-center align-items-center my-1">
            <rating :course_id="course_id"></rating>
        </div>
        <div class="d-flex justify-content-center my-4">
            <a href="/" class="btn outline-gold">
                Home
            </a>
            <a href="/student/dashboard/courses" class="btn fill-gold ms-3">
                Student Dashboard
            </a>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script>
    var app = new Vue({
    el: '#app',
        data(){
            return{
                course_id: {{Request::get('course')}}
            }
        }
    });
    </script>
@endsection