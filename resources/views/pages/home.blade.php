@extends('layout.default')
@section('meta')
    @component('components.meta')
        @slot('title')
             Dreamguru | Home
        @endslot

        @slot('description')
            Dreamaxtion helps companies and people maximize potential through Human Capital management system and psychometric assessment for personal and career growth.
        @endslot
    @endcomponent
@endsection

@section('css')
    <style> 
        
    </style>
@endsection

@section('content')

<dashboard-highlight :course_highlight="course_highlight" :hero_background="hero_background" v-if="is_course_ready"></dashboard-highlight>
<highlight-skeleton v-if="!is_course_ready"></highlight-skeleton>
<div class="main-padding-h main-padding-v">
    <div v-if="in_progress_courses.length > 0">
        <h5 class="color-gold fw-semi mt-3">
            Continue Your Journey
        </h5> 
        <p class="color-muted mb-3">
            Here is what’s been leftoff
        </p>
        <div v-if="in_progress_courses.length > 0">
            <div class="d-flex align-items-center">
                <h6 class="color-gold me-3 mb-0">
                    COURSES
                </h6>
                <div class="hr"></div>
            </div>
            <div class="row pt-4 pb-5 g-3"
                v-if="in_progress_courses_ready"
            >
                <div class="col-lg-6 col-xl-3 col-xxl-3" v-for="course in in_progress_courses">
                    <cards-cont :course="course.course"></cards-cont>
                </div>
            </div>
        </div>
    </div>
    <h3 class="color-gold fw-semi">
        Recommended For You
    </h3>
    <h6 class="color-muted">
        Based on your goals
    </h6>
    <div class="row pt-4 pb-5 g-3">
        <div class="col-lg-6 col-xl-3 col-xxl-3" v-if="course.status == 'completed'" v-for="course in courses">
            <cards :course="course" :session_id="session_id"></cards>
        </div>
    </div>
    <div class="row g-3" v-if="!is_course_ready">
        <div class="col-lg-6 col-xl-3 col-xxl-3">
            <cards-skeleton></cards-skeleton>
        </div>
        <div class="col-lg-6 col-xl-3 col-xxl-3">
            <cards-skeleton></cards-skeleton>
        </div>
        <div class="col-lg-6 col-xl-3 col-xxl-3">
            <cards-skeleton></cards-skeleton>
        </div>
        <div class="col-lg-6 col-xl-3 col-xxl-3">
            <cards-skeleton></cards-skeleton>
        </div>
    </div>
    {{-- <div class="d-flex align-items-center">
        <h6 class="color-gold me-3 mb-0">CODING</h6>
        <a class="btn  btn-rounded btn-sm outline-gold me-3" style="white-space: nowrap" role="button">View More</a>
        <div class="hr"></div>
    </div>
    <div class="row pt-4 pb-5">
        <div class="col-12 col-lg-6 col-xl-3" v-for="course in courses">
            <cards class="mt-3" :course="course"></cards>
        </div>
        
    </div> --}}
    
    
</div>


@endsection

@section('js')
    <script>
    var app = new Vue({
    el: '#app',
    data() {
        return {
            course_highlight: null,
            courses: null,
            is_course_ready: false,
            hero_background:'',
            thumbnail:'',
            session_id: "{{session('data')['id']}}" ,
            in_progress_courses: '',
            in_progress_courses_ready: false,
        }
    },
    mounted: function() {
        axios.get(this.api_url + '/api/v1/courses')
        .then(response => {
            this.course_highlight = response.data[0]
            this.courses = response.data
            this.hero_background = this.course_highlight.hero_background
            this.is_course_ready = true
        }),
        axios.get(this.api_url + '/api/v1/courses')
        .then(response => {
            this.courses = response.data
        }),
        axios({
            method: 'get',
            url: this.api_url + '/api/v1/students/{{session('data')['id']}}/courses',
            headers: {
                'Authorization' : 'Bearer ' + bearer,
                'Accept' : 'application/json',
            }
        })
        .then(response => {
            this.in_progress_courses = response.data
            this.in_progress_courses_ready = true

        })
        .catch(function (error) {
            if (error.response) {
            }
        })
    }
    });
    
    </script>
@endsection