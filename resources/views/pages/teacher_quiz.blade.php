@extends('layout.default')

@section('meta')
    @component('components.meta')
        @slot('title')
             Dreamguru | Quiz
        @endslot

        @slot('description')
            Dreamaxtion helps companies and people maximize potential through Human Capital management system and psychometric assessment for personal and career growth.
        @endslot
    @endcomponent
@endsection

@section('css')
    <style> 
        
    </style>
@endsection

@section('content')
<div class="main-padding-h main-padding-v" v-if="is_course_ready">
    <h3 class="color-gold fw-semi">
        Course Dashboard
    </h3>
    <h6  class="color-muted">
        @{{course.title}} > Quiz Name > DASHBOARD
    </h6>

    <div class="d-flex align-items-center pt-4 mb-3">
        <h6 class="color-gold me-3 mb-0" style="white-space: nowrap">
            STUDENT DETAIL
        </h6>
        <div class="hr"></div>
    </div>
    <div class="d-flex flex-wrap">
        <h6 class="me-3">
            Average Score: @{{course.avg_score}}
        </h6>
        <h6 class="me-3">
            Failed: @{{course.total_students_fail}}
        </h6>
        <h6 class="color-gold">
            Passed : @{{course.total_students_pass}}
        </h6>
    </div>
    <div class="table-responsive-md">
        <table class="table mt-2">
            <tr>
                <th class="color-white">No</th>
                <th class="color-white">Name</th>
                <th class="color-white">Attempts</th>
                <th class="color-white">Score</th>
                <th class="color-white">Status</th>
            </tr>
            <tr v-for="(student,index) in course.students">
                <td>
                    <p class="me-4">
                        @{{index + 1}}.
                    </p>
                </td>
                <td>
                    <div class="d-flex align-items-center">
                        <img 
                            v-if="student.profile.avatar" 
                            class="me-2 avatar" 
                            :src="api_url + '/storage/' + student.profile.avatar" 
                            alt="avatar" 
                            height="30">
                        <img v-else class="me-2 avatar" src="/img/placeholder.png" alt="avatar">
                        <p class="me-4">
                            @{{student.profile.name}}
                        </p>
                    </div>
                </td>
                <td>
                    <div v-if="student.user_quizzes[0]">
                        <p class="me-4">
                            @{{student.user_quizzes[0].attempts}}
                        </p>
                    </div>
                    <div v-else>
                        <p class="me-4">
                            No Attempt
                        </p>
                    </div>
                </td>
                <td>
                    <p class="me-4">
                        @{{student.max_score}}
                    </p>
                </td>
                <td>
                    <p v-if="student.is_success == 1" class="me-4 color-gold">
                        Passed!
                    </p>
                    <p v-else class="me-4">
                        Failed
                    </p>
                </td>
            </tr>
        </table>
    </div>
</div>

@endsection

@section('js')
    <script>
       'use strict';
        var app = new Vue({
            el: '#app',

        data() {
            return {
                course: null,
                is_course_ready: false,
                course_quizzes: []
            }
        },
        mounted: function() {
            axios({
                method: 'get',
                url: this.api_url + '/api/v1/teachers/{{session('data')['id']}}/courses/{{$course_id}}/quizzes/{{Request::route('content_id')}}',
                headers: {
                    'Authorization': 'Bearer ' + bearer,
                    'Accept': 'application/json',
                }
            })
            .then(response => {
                this.course = response.data
                this.is_course_ready = true
                var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour : 'numeric', minute : 'numeric'}
                for(var i = 0; i < this.course.students.length; i++){
                    this.course.students[i].pivot.created_at = new Date(this.course.students[i].pivot.created_at).toLocaleString('id-ID', options);  
                    this.course.students[i].pivot.updated_at = new Date(this.course.students[i].pivot.updated_at).toLocaleString('id-ID', options);  
                }
                for(var i = 0; i < this.course.course_sections.length; i++){
                    this.course_quizzes[i] = this.course.course_sections[i]
                    for(var j = 0; j < this.course.course_sections[i].section_content_orders.length; j++){
                        this.course_quizzes[i].section_content_orders = _.filter(this.course.course_sections[i].section_content_orders, ['endpoint', 'section_quizzes']);
                    }
                }
            })
        },
        }) 
    </script>
@endsection