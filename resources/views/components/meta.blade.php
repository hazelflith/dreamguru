<title> {{ $title }} </title>

<!-- General SEO -->
<meta name="description" content="{{ $description }}">

<!-- Schema.org -->
<meta itemprop="name" content="{{ $title }}">
<meta itemprop="description" content="{{ $description }}">
{{-- <meta itemprop="image" content="{{ isset($image) ? $image : asset('img/dreamaxtion-banner-1200.png') }}"> --}}

<!-- Twitter card data -->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@dreamaxtion">
<meta name="twitter:title" content="{{ $title }}">
<meta name="twitter:description" content="{{ $description }}">
<meta name="twitter:creator" content="@dreamaxtion">
{{-- <meta name="twitter:image" content="{{ isset($image) ? $image : asset('img/dreamaxtion-banner-1200.png') }}"> --}}

<!-- Open graph data -->
<meta property="og:title" content="{{ $title }}">
<meta property="og:type" content="website">
<meta property="og:url" content="{{ request()->url() }}">
{{-- <meta property="og:image" content="{{ isset($image) ? $image : asset('img/dreamaxtion-banner-1200.png') }}"> --}}
<meta property="og:description" content="{{ $description }}">
<meta property="og:site_name" content="Dreamaxtion">


