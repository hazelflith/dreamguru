<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        @yield('meta')

        <!-- Fonts -->
        {{-- <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'> --}}
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
        <link href="https://unpkg.com/@yaireo/tagify/dist/tagify.css" rel="stylesheet" type="text/css" />
        
        <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="/css/app.css">
        <script id="midtrans-script" type="text/javascript" src="https://api.midtrans.com/v2/assets/js/midtrans-new-3ds.min.js" data-environment="{{env('MIDTRANS_ENVIRONMENT')}}" data-client-key="{{env('MIDTRANS_CLIENT_KEY')}}"></script>
        <!-- Styles -->
        @yield('style')
    </head>
    <body>
        {{-- Preloader --}}
        {{-- <div id="preloaders" class="preloader text-center">
            <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
        </div> --}}
        {{-- Loading --}}
        <div  class="loading">
            <div class="loading-bg"></div>
            <div class="loading-gif"></div>
        </div>
        <div id="app">
            <div class="container-fluid g-0">
                @if(Request::is('admin*'))
                    @include('partials.admin_sidebar') 
                @else   
                    @include('partials.sidebar2')   
                @endif
            </div>
        </div>
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
        {{-- <script>
            // If reCAPTCHA is still loading, grecaptcha will be undefined.
            grecaptcha.ready(function(){
                grecaptcha.render("container", {
                    sitekey: "6Ldngp8fAAAAALZ6PLH3M2osc3PxRjiDBqNwvdd7"
                });
            });
        </script> --}}
        <script src="https://unpkg.com/@yaireo/tagify"></script>
        <script src="https://unpkg.com/@yaireo/tagify@3.1.0/dist/tagify.polyfills.min.js"></script>
        <script src="https://cdn.tiny.cloud/1/iwbxnnd9285rbf4007eh6bfdch450be4qboxu6jy8itdavq7/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
        <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
        <script type="text/javascript" src="/js/navbar.js"></script>
        <script type="text/javascript" src="/js/app.js"></script>
        <script type="text/javascript" src="/js/picker.js"></script>
        <script type="text/javascript" src="/js/picker.date.js"></script>
        <script>
            if(!this.$cookies.isKey('dreamguru_role')){
                this.$cookies.set('dreamguru_role', "teacher")
            }
        </script>
        @yield('js')
    </body>
</html>

