/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import "bootstrap";
window.bootstrap = require("bootstrap")
window.Vue = require('vue').default;

import money from 'v-money'
import Vue from "vue";
// register directive v-money and component <money>
Vue.use(money, {precision: 0, thousands:"."})

//cookies
import VueCookies from 'vue-cookies'
Vue.use(require('vue-cookies'))
Vue.use(VueCookies, { expire: '365d'})
//skeleton
import VueSkeletonLoader from 'skeleton-loader-vue';
Vue.component('vue-skeleton-loader', VueSkeletonLoader);

import VueSlickCarousel from 'vue-slick-carousel';
Vue.component('vue-slick-carousel', VueSlickCarousel);

//login token expired
axios.interceptors.response.use((response) => response, (error) => {
    if(error.response) {
        if(error.response.status == 401 && error.response.data.message !== 'Your credential does not match.') {
            //kelogin
            window.location.href = "/sessionexpired"
        }
    }
    throw error;
});

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('dashboard-highlight', require('./components/DashboardHighlight.vue').default);
Vue.component('highlight-skeleton', require('./components/HighlightSkeleton.vue').default);
Vue.component('course-highlight', require('./components/CourseHighlight.vue').default);
Vue.component('cards', require('./components/Cards/Cards.vue').default);
Vue.component('cards-skeleton', require('./components/Cards/CardsSkeleton.vue').default);
Vue.component('cards-edit-skeleton', require('./components/Cards/CardsEditSkeleton.vue').default);
Vue.component('cards-edit', require('./components/Cards/CardsEdit.vue').default);
Vue.component('cards-cont', require('./components/Cards/CardsContinue.vue').default);
Vue.component('cards-long', require('./components/Cards/CardsLong.vue').default);
Vue.component('cards-longedit', require('./components/Cards/CardsLongEdit.vue').default);
Vue.component('course-tab', require('./components/CourseTab.vue').default);
Vue.component('toc', require('./components/TableOfContent.vue').default);
Vue.component('toc-skeleton', require('./components/TableOfContentSkeleton.vue').default);
Vue.component('progress-ring', require('./components/ProgressRing.vue').default);
Vue.component('idp-cont', require('./components/IDPcontinue.vue').default);
Vue.component('idp-comp', require('./components/IDPcompleted.vue').default);
Vue.component('isdiscounted', require('./components/IsDiscount.vue').default);
Vue.component('question', require('./components/Question.vue').default);
Vue.component('question-skeleton', require('./components/QuestionSkeleton.vue').default);
Vue.component('result', require('./components/Result.vue').default);
Vue.component('rating', require('./components/Rating.vue').default);
Vue.component('test', require('./components/test.vue').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

 var api_url = process.env.MIX_API_URL
//Preloader

// window.onload = function(){
//     var elems = document.getElementsByClassName('skeleton-loader');
//     for (var i=0;i<elems.length;i+=1){
//         elems[i].style.display = 'none';
//     }
// }

//mixin

import draggable from 'vuedraggable';
import VeeValidate from 'vee-validate';
import Pagination from 'vue-pagination-2';
const Mixin = {
    data() {
        return {
            api_url,
            role: 'teacher',
        }
    },
    components: {
        draggable, Pagination
    },
    methods: {
        //
        switchRole(target){
            this.$cookies.set('dreamguru_role', target)
            location.reload()
        },
        showLoading() {
            $('.loading').show()
        },
        hideLoading() {
            $('.loading').hide()
        }
    }
}

Vue.mixin(Mixin)
Vue.use(VeeValidate);