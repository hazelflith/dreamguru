<h1>DREAMGURU</h1>
<b> HOW TO SETUP </b>
<br>

1. Pull the files from this git
2. composer install
3. npm install
4. php artisan key:generate
5. cp .env.example .env
6. Change api url on .env
7. php artisan config:clear
8. npm run watch
