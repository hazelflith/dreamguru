<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Http\Controllers\FrontController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\LogoutController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FrontController::class, 'home'])->name('/');
Route::get('/course', [FrontController::class, 'course'])->name('/course');

Auth::routes();

Route::get('/welcome', [FrontController::class, 'welcome'])->name('/welcome');
Route::get('/login', [LoginController::class, 'index'])->name('login');
Route::get('/register', [RegisterController::class, 'index'])->name('register');
Route::post('/logout', [LogoutController::class, 'logout'])->name('logout');
Route::post('/login', [LoginController::class, 'login']);
Route::get('/forgot-password', [FrontController::class, 'forgotPassword']);
Route::get('/reset-password/{token}', [FrontController::class, 'resetPassword']);
Route::post('/register', [RegisterController::class, 'register']);
Route::get('/course/example/session-1', [FrontController::class, 'session'])->name('/course/example/session-1');
Route::get('/course/example/quiz-1', [FrontController::class, 'quiz'])->name('/course/example/quiz-1');
Route::get('/course/example/quiz-1/question-1', [FrontController::class, 'question1'])->name('/course/example/quiz-1/question-1');
Route::get('/course/example/quiz-1/question-2', [FrontController::class, 'question2'])->name('/course/example/quiz-1/question-2');
Route::get('/course/example/quiz-1/result', [FrontController::class, 'result'])->name('/course/example/quiz-1/result');
Route::get('/teacher/course/example/edit', [FrontController::class, 'editCourse'])->name('/teacher/course/example/edit');
Route::get('/teacher/course/example/part/edit', [FrontController::class, 'editPart']);
Route::get('/teacher/course/add', [FrontController::class, 'addCourse']);
Route::get('/teacher/quiz/add', [FrontController::class, 'addQuiz']);
Route::get('/complete', [FrontController::class, 'courseComplete']);
Route::get('/teacher/quiz/quiz-1/edit', [FrontController::class, 'editQuiz'])->name('/teacher/quiz/quiz-1/edit');
Route::get('/student/dashboard/courses', [FrontController::class, 'studentDashboardCourses'])->name('/student/dashboard/courses');
Route::get('/student/dashboard/quizzes', [FrontController::class, 'studentDashboardQuizzes'])->name('/student/dashboard/quizzes');
Route::get('/student/dashboard/billing', [FrontController::class, 'studentDashboardBilling'])->name('/student/dashboard/billing');
Route::get('/profile', [FrontController::class, 'profile'])->name('/profile');
Route::get('/teacher/course', [FrontController::class, 'myCourse'])->name('/teacher/course');
Route::get('/teacher/quiz', [FrontController::class, 'myQuiz'])->name('/teacher/quiz');
Route::get('/admin', [FrontController::class, 'admin'])->name('/admin');
Route::get('/admin/user/teachers', [FrontController::class, 'adminTeachers'])->name('/admin/user/teachers');
Route::get('/admin/user/students', [FrontController::class, 'adminStudents'])->name('/admin/user/students');
Route::get('/admin/user/admins', [FrontController::class, 'adminAdmins'])->name('/admin/user/admins');
Route::get('/admin/categories', [FrontController::class, 'adminCategories'])->name('/admin/categories');
Route::get('/admin/courses', [FrontController::class, 'adminCourses'])->name('/admin/courses');
Route::get('/sessionexpired', function (){
    return redirect('/login')->with('errors', [
        ['Your Session is Expired, Please Re Login']
    ]);
});

Route::get('/course/{course_id}', [CourseController::class, 'show']);
Route::get('/course/{course_id}/preview', [CourseController::class, 'showPreview']);
Route::get('/course/{course_id}/complete', [CourseController::class, 'complete']);
Route::get('/teacher/course/{course_id}', [CourseController::class, 'teacherShow']);
Route::get('/teacher/course/{course_id}/{content_id}/view', [CourseController::class, 'teacherShowQuiz']);
Route::get('/teacher/course/{course_id}/edit', [CourseController::class, 'edit']);
Route::get('/teacher/quiz/{quiz_id}/view', [CourseController::class, 'viewQuiz']);
Route::get('/course/{course_id}/parts/{content_id}', [CourseController::class, 'showPart']);
Route::get('/course/{course_id}/parts/{content_id}/preview', [CourseController::class, 'showPartPreview']);
Route::get('/course/{course_id}/parts/{part_id}/edit', [CourseController::class, 'editPart']);
Route::get('/teacher/quiz/{quiz_id}/edit', [CourseController::class, 'editQuiz']);
Route::get('/teacher/quiz/{quiz_id}/edit/redirect', [CourseController::class, 'redirectToEditQuiz']);
Route::get('/course/parts/{section_id}/add', [CourseController::class, 'addPart']);
Route::get('/course/{course_id}/section-quizzes/{content_id}', [CourseController::class, 'showQuiz']);
Route::get('/course/{course_id}/section-quizzes/{content_id}/review/{user_quiz_id}', [CourseController::class, 'showQuizReview']);
Route::get('/course/{course_id}/section-quizzes/{content_id}/run', [CourseController::class, 'startQuiz']);
Route::get('/course/{course_id}/section-quizzes/{content_id}/preview', [CourseController::class, 'showQuizPreview']);
Route::get('/course/{course_id}/section-quizzes/{content_id}/run/preview', [CourseController::class, 'startQuizPreview']);

Route::post('/profile',[UserController::class, 'update']);
Route::post('/student/email',[UserController::class, 'updateEmail'] );
Route::post('/student/password',[UserController::class, 'updatePassword'] );

Route::get('/payment', [PaymentController::class, 'paymentPage'])->name('payment');
Route::get('/payment/billing', [FrontController::class, 'billing']);
Route::get('/student/billing/detail', [PaymentController::class, 'show']);
Route::get('/payment/redirect', [PaymentController::class, 'redirect']);
Route::get('/redirect-to-billing', function (Request $request){
    return redirect('/student/dashboard/billing')->with('errors', [
        'Mohon maaf, pembayaran anda tidak bisa diproses, selesaikan pembayaran aktif untuk course "'. $request->course .'"'
    ]);
});
