<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function paymentPage(){
        if (session('token')){
            return view('pages.payment');
        }
        else{
            return redirect('/login');
        }
    }
    public function redirect(){
        if (session('token')){
            return view('pages.payment_redirect');
        }
        else{
            return redirect('/login');
        }
    }
    public function show(){
        if (session('token')){
            return view('pages.billing');
        }
        else{
            return redirect('/login');
        }
    }
    public function cardPaymentStatus(Request $request){
        $response = json_decode($request['response'], true);
        if($response['transaction_status'] == 'capture'){
            if($response['fraud_status'] == 'challenge'){
                echo 'pembayaran anda sedang dalam pertimbangan, mohon pantau dashboard untuk mendapatkan informasi lebih lanjut..';
                
                echo "<script>setTimeout(function(){ window.location.href = '/'; }, 4000);</script>"; 
            }
            else{
                echo 'Pembayaran berhasil, mengalihkan...';
                echo "<script>setTimeout(function(){ window.location.href = '/'; }, 4000);</script>";
            }
        }
        if($response['transaction_status'] == 'deny'){
            echo 'Transaksi gagal';
            echo "<script>setTimeout(function(){ window.location.href = '/'; }, 4000);</script>";
        }
    }
}
