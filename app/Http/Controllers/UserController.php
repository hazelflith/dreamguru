<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class UserController extends Controller
{
    public function update(Request $request){
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '. session('token'),
            'Accept' => 'application/json'
        ])->post(env('MIX_API_URL').'/api/v1/users/self/profile', [
            'address' => $request->address,
            'phone_number' => $request->phone_number,
        ]);
        if($response->status() == 200){
            return redirect('/profile')->with(['message' => 'Changes successfully saved']);
        }

        else if($response->status() == 422){
            return redirect('/profile')->with(['errors' => $response['errors']]);
        }

        else if($response->status() == 500){
            return redirect('/profile')->with(['errors' => 'Server in maintenance']);
        }
        else{
            return redirect('/profile')->with(['errors' => $response['errors']]);
        }
    }
    public function updateEmail(Request $request){
        $response =Http::withHeaders([
            'Authorization' => 'Bearer '. session('token'),
            'Accept' => 'application/json'
        ])
        ->put(env('MIX_API_URL').'/api/v1/users/self/security', [
            'email' => $request->email,
            'current_password'=> $request->current_password
        ]);

        if($response->status() == 200){
            return redirect('/profile')->with(['message' => 'Changes successfully saved']);
        }

        else if($response->status() == 422){
            return redirect('/profile')->with(['error' => $response['errors']]);
        }

        else if($response->status() == 500){
            return redirect('/profile')->with(['message' => 'Server in maintenance']);
        }
    }
    public function updatePassword(Request $request){
        $response =Http::withHeaders([
            'Authorization' => 'Bearer '. session('token'),
            'Accept' => 'application/json'
        ])
        ->put(env('MIX_API_URL').'/api/v1/users/self/security', [
            'new_password' => $request->new_password,
            'new_password_confirmation' => $request->new_password_confirmation,
            'current_password'=> $request->current_password,
        ]);  
        if($response->status() == 200){
            return redirect('/profile')->with(['message' => 'Changes successfully saved']);
        }

        else if($response->status() == 422){
            return redirect('/profile')->with(['error' => $response['errors']]);
        }

        else if($response->status() == 500){
            return redirect('/profile')->with(['message' => 'Server in maintenance']);
        }
    }
}
