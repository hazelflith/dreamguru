<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

class FrontController extends Controller
{
    public function home()
    {
        if (session('token')){
            return view('pages.home');
        }
        else{
            return view('pages.coming_soon');
        }
    }
    public function billing()
    {
        if (session('token')){
            return view('pages.billing');
        }
        else{
            return redirect('/login');
        }
        
    }
    public function courseComplete()
    {
        if (session('token')){
            return view('pages.course_completed');
        }
        else{
            return redirect('/login');
        }
    }
    public function register()
    {
        return view('pages.register');
    }
    public function forgotPassword()
    {
        return view('pages.forgot_password');
    }
    public function resetPassword($token)
    {
        return view('pages.reset_password', ['token' => $token]);
    }
    public function welcome()
    {
        return view('pages.welcome');
    }
    public function course()
    {
        if (session('token')){
            return view('pages.course');
        }
        else{
            return redirect('/login');
        }
    }
    public function coursePage()
    {
        if (session('token')){
            return view('pages.courses.course_page');
        }
        else{
            return redirect('/login');
        }
    }
    public function session()
    {
        if (session('token')){
            return view('pages.courses.sessions.part');
        }
        else{
            return redirect('/login');
        }
    }
    public function quiz()
    {
        if (session('token')){
            return view('pages.courses.sessions.quiz');
        }
        else{
            return redirect('/login');
        }
    }
    public function question1()
    {
        return view('pages.courses.sessions.questions.qs1');
    }
    public function question2()
    {
        return view('pages.courses.sessions.questions.qs2');
    }
    public function result()
    {
        if (session('token')){
            return view('pages.courses.sessions.result');
        }
        else{
            return redirect('/login');
        }
    }
    public function payment()
    {
        if (session('token')){
            return view('pages.payment');
        }
        else{
            return redirect('/login');
        }
    }
    public function redirect()
    {
        if (session('token')){
            return view('pages.payment_redirect');
        }
        else{
            return redirect('/login');
        }
    }
    public function editCourse()
    {
        return view('pages.edit_course');
    }
    public function editQuiz()
    {
        return view('pages.edit_quiz' ,);
    }
    public function editPart()
    {
        return view('pages.edit_part');
    }
    public function addCourse()
    {
        return view('pages.add_course');
    }
    public function addQuiz()
    {
        return view('pages.add_quiz');
    }
    public function studentDashboardCourses()
    {
        if (session('token')){
            if (session('data')['role'] == 'student' || session('data')['role'] == 'teacher'){
                return view('pages.student_dashboard_courses');
            }
            else{
                return view('pages.forbidden');
            }
        }
        else{
            return redirect('/login');
        }
    }
    public function studentDashboardBilling()
    {
        if (session('token')){
            if (session('data')['role'] == 'student' || session('data')['role'] == 'teacher'){
                return view('pages.student_dashboard_billing');
            }
            else{
                return view('pages.forbidden');
            }
        }
        else{
            return redirect('/login');
        }
    }
    public function studentDashboardQuizzes()
    {
        if (session('token')){
            if (session('data')['role'] == 'student' || session('data')['role'] == 'teacher'){
                return view('pages.student_dashboard_quizzes');
            }
            else{
                return view('pages.forbidden');
            }
        }
        else{
            return redirect('/login');
        }
    }
    public function profile()
    {
        if (session('token')){
            return view('pages.profile');
        }
        else{
            return redirect('/login');
        }
    }
    public function myCourse()
    {
        if (session('token')){
            if (session('data')['role'] == 'teacher'){
                return view('pages.my_course');
            }
            else{
                return view('pages.forbidden');
            } 
        }
        else{
            return redirect('/login');
        }
    }
    public function myQuiz()
    { 
        if (session('token')){
            if (session('data')['role'] == 'teacher'){
                return view('pages.my_quiz');
            }
            else{
                return view('pages.forbidden');
            }
        }
        else{
            return redirect('/login');
        }
    }
    public function admin()
    {
        if (session('token')){
            if (session('data')['role'] == 'admin'){
                return view('pages.admin.admin_dashboard');
            }
            else{
                return view('pages.forbidden');
            }
        }
        else{
            return redirect('/login');
        }
    }
    public function adminTeachers()
    {
        if (session('token')){
            if (session('data')['role'] == 'admin'){
                return view('pages.admin.admin_teacher_dashboard');
            }
            else{
                return view('pages.forbidden');
            }
        }
        else{
            return redirect('/login');
        }
    }
    public function adminStudents()
    {
        if (session('token')){
            if (session('data')['role'] == 'admin'){
                return view('pages.admin.admin_student_dashboard');
            }
            else{
                return view('pages.forbidden');
            }
        }
        else{
            return redirect('/login');
        }
    }
    public function adminAdmins()
    {
        if (session('token')){
            if (session('data')['role'] == 'admin'){
                return view('pages.admin.admin_admin_dashboard');
            }
            else{
                return view('pages.forbidden');
            }
        }
        else{
            return redirect('/login');
        }
    }
    public function adminCategories()
    {
        if (session('token')){
            if (session('data')['role'] == 'admin'){
                return view('pages.admin.admin_categories');
            }
            else{
                return view('pages.forbidden');
            }
        }
        else{
            return redirect('/login');
        }
    }
    public function adminCourses()
    {
        if (session('token')){
            if (session('data')['role'] == 'admin'){
                return view('pages.admin.admin_courses');
            }
            else{
                return view('pages.forbidden');
            }
        }
        else{
            return redirect('/login');
        }
    }
}
