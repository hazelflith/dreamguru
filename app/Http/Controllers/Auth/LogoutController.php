<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;

class LogoutController extends Controller
{
    public function logout(){
        session()->flush();

        return redirect('/login');

    }
}
