<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    
    public function index(){
        return view('pages.register');
    }
    public function register(Request $request){
        $response = Http::acceptJson()->post(env('MIX_API_URL').'/api/v1/auth/register', [
            'username' => $request->username,
            'email' => $request->email,
            'password' => $request->password,
            'password_confirmation' => $request->password_confirmation,
            'phone_number' => $request->phone_number,
            'name' => $request->name,
            'date_of_birth' => $request->date_of_birth_submit,
            'gender' => $request->gender,
        ]);
        
        if($response->status() == 422){
            return redirect('/register')->with([
                'errors' => $response['errors']
            ])->withInput($request->all());
        }

        if($response->status() == 500){
            return redirect('/register');
        }
        
        $credentials = json_decode($response, true);
        session($credentials);

        return redirect('/');

    }
}
