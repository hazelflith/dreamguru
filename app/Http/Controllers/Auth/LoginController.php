<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function index(){
        if (session('token')){
            if (session('data')['role'] == 'admin'){
                return redirect('/admin');
            }
            else{
                return redirect('/');
            }
        }
        else{
            return view('pages.login');
        }
        
    }

    public function login(Request $request){
        $response = Http::acceptJson()->post(env('MIX_API_URL'). '/api/v1/auth/login', [
            'username' => $request->username,
            'password' => $request->password,
        ]);
        $credentials = json_decode($response, true);
        session($credentials);
        if($response->status() == 401){
            return redirect('/login')->with(['message' => $response['message']]);
        }

        else if($response->status() == 422){
            return redirect('/login')->with(['errors' => $response['errors']]);
        }

        else if (session('data')['role'] == 'admin'){
            return redirect('/admin');
        }
        else{
            return redirect('/');
        }

    }
}
