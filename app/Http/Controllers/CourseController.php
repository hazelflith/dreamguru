<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function show($course_id){
        if (session('token')){
            return view("pages.courses.course_page", ["course_id" => $course_id]);
        }
        else{
            return redirect('/login');
        }
    }
    public function showPreview($course_id){
        if (session('token')){
            return view("pages.courses.preview_course_page", ["course_id" => $course_id]);
        }
        else{
            return redirect('/login');
        }
    }
    public function complete($course_id){
        if (session('token')){
            return view("pages.course_completed", ["course_id" => $course_id]);
        }
        else{
            return redirect('/login');
        }
    }
    public function teacherShow($course_id){
        if (session('token')){
            if (session('data')['role'] == 'teacher'){
                return view("pages.teacher_course", ["course_id" => $course_id]);
            }
            else{
                return view('pages.forbidden');
            }
        }
        else{
            return redirect('/login');
        }
    }
    public function teacherShowQuiz($course_id, $content_id){
        if (session('token')){
            if (session('data')['role'] == 'teacher'){
                return view("pages.teacher_quiz", ["course_id" => $course_id], ["content_id" => $content_id]);
            }
            else{
                return view('pages.forbidden');
            }
        }
        else{
            return redirect('/login');
        }
    }
    public function edit($course_id){
        if (session('token')){
            if (session('data')['role'] == 'teacher'){
                return view("pages.edit_course", ["course_id" => $course_id],);
            }
            else{
                return view('pages.forbidden');
            }
        }
        else{
            return redirect('/login');
        }
        
    }
    public function editPart($course_id, $part_id){
        if (session('token')){
            if (session('data')['role'] == 'teacher'){
                return view("pages.edit_part", ["part_id" => $part_id], ["course_id" => $course_id]);
            }
            else{
                return view('pages.forbidden');
            }
        }
        else{
            return redirect('/login');
        }
    }
    public function addPart($section_id){
        if (session('token')){
            if (session('data')['role'] == 'teacher'){
                return view("pages.add_part", ["section_id" => $section_id]);
            }
            else{
                return view('pages.forbidden');
            }
        }
        else{
            return redirect('/login');
        }
    }
    public function viewQuiz($quiz_id){
        if (session('token')){
            if (session('data')['role'] == 'teacher'){
                return view("pages.view_quiz", ["quiz_id" => $quiz_id],);
            }
            else{
                return view('pages.forbidden');
            }
        }
        else{
            return redirect('/login');
        }
    }
    public function editQuiz($quiz_id){    
        if (session('token')){
            if (session('data')['role'] == 'teacher'){
                return view("pages.edit_quiz", ["quiz_id" => $quiz_id],);
            }
            else{
                return view('pages.forbidden');
            }
        }
        else{
            return redirect('/login');
        }
    }
    public function redirectToEditQuiz($quiz_id){   
        if (session('token')){
            if (session('data')['role'] == 'teacher'){
                return view("pages.redirect_only", ["quiz_id" => $quiz_id],);
            }
            else{
                return view('pages.forbidden');
            }
        }
        else{
            return redirect('/login');
        }
    }
    public function showPart($course_id,$content_id){
        if (session('token')){
            return view("pages.courses.sessions.part", ["course_id" => $course_id], ["content_id" => $content_id]);
        }
        else{
            return redirect('/login');
        }
    }
    public function showPartPreview($course_id,$content_id){
        if (session('token')){
            return view("pages.courses.sessions.preview_part", ["course_id" => $course_id], ["content_id" => $content_id]);
        }
        else{
            return redirect('/login');
        }
    }
    public function showQuiz($course_id,$content_id){
        if (session('token')){
            return view("pages.courses.sessions.quiz", ["course_id" => $course_id], ["content_id" => $content_id]);
        }
        else{
            return redirect('/login');
        }
    }
    public function showQuizReview($course_id,$content_id,$user_quiz_id){
        if (session('token')){
            return view("pages.courses.sessions.review_quiz", [
            "course_id" => $course_id,
             "content_id" => $content_id, 
             "user_quiz_id" => $user_quiz_id
            ]);
        }
        else{
            return redirect('/login');
        }
    }
    public function startQuiz($course_id, $content_id){
        //find recent created quiz for the user    
        if (session('token')){
            return view("pages.courses.sessions.question", ["course_id" => $course_id], ["content_id" => $content_id]);
        }
        else{
            return redirect('/login');
        }
    } 
    public function showQuizPreview($course_id,$content_id){
        if (session('token')){
            return view("pages.courses.sessions.preview_quiz", ["course_id" => $course_id], ["content_id" => $content_id]);
        }
        else{
            return redirect('/login');
        }
    }
    public function startQuizPreview($course_id, $content_id){
        //find recent created quiz for the user    
        if (session('token')){
            return view("pages.courses.sessions.preview_question", ["course_id" => $course_id], ["content_id" => $content_id]);
        }
        else{
            return redirect('/login');
        }
    } 
}
